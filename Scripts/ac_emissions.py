# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 15:05:17 2021

@author: TY van der Duim
"""

'''
REQUIRED FILES:
    
    - ac_emiss_1min_jun19.so6 (not included, >100 MB)
    - ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6 (not included, >100 MB)
    - Flights_06-2019.csv (not included, >100 MB)
    - Flight_Points_Actual_06-2019.csv (not included, >100 MB)
    - vert_levels.npy (included)
    - wrfchemi_00z_d01.nc, wrfchemi_00z_d02.nc, wrfchemi_00z_d03.nc, wrfchemi_00z_d04.nc (included)
    - wrfchemi_12z_d01.nc, wrfchemi_12z_d02.nc, wrfchemi_12z_d03.nc, wrfchemi_12z_d04.nc (included)
'''

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Import packages-----------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''
    
import pandas as pd
import datetime as dt
import numpy as np
import math as m
from scipy import stats
import matplotlib.pyplot as plt
import os
os.environ["PROJ_LIB"] = "C:\\Users\\fafri\\anaconda3\\pkgs\\basemap-1.2.2-py38hcd0dc2c_2\\Library\\share\\"
from mpl_toolkits.basemap import Basemap
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import sys
import matplotlib.animation as animation
import netCDF4 as nc
from collections import Counter
import warnings
warnings.filterwarnings('ignore')

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Plot settings-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

plt.style.use('seaborn-darkgrid')
plt.rc('text', usetex=False)
plt.rc('font', family='century')
plt.rc('xtick', labelsize=18) 
plt.rc('ytick', labelsize=18) 
plt.rc('font', size=20) 

'''
-------------------------------------------------------------------------------------------------------------
---------------------------------------------------Variables-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

# time window
start_date = '01-06-2019 00:00:00'
end_date = '02-06-2019 00:00:00'

# directories
home_dir = # home directory
path = home_dir + 'Files\\'

# molar masses
moll_mass = {
        'CO': 28.0101,
        'NO': 46.0055,  # Expressed in mass NO2
        'NO2': 46.0055,
        'SO2': 64.0638,
        'NH3': 17.03052,
        'CH3OH': 32.04,
        'C2H5OH': 46.07,
        'ETH': 30.07,
        'HC3': 44.1,  # Based on Propane
        'HC5': 72.15,  # Based on Pentane
        'HC8': 114.23,  # Based on Octane
        'OL2': 28.05,
        'OLT': 42.08,  # Based on Propene
        'OLI': 54.09,  # Based on 1.3Butadiene
        'ISO': 68.12,
        'TOL': 92.14,  # Based on Toluene, but could also include other less reactive aromatics
        'XYL': 106.16,  # Based on Xylene, but could also include other reactive aromatics
        'ALD': 44.05,  # Based on Acetaldehyde, but could also include other higher aldehydes
        'HCHO': 30.031,
        'KET': 72.11,  # Based on Methyl Ethyl Ketone, but could also include other ketones
        'ORA2': 60.052  # Based on Acetic Acid, but could also inclue other higher acids
        }

# scaling factors used to map aircraft emissions to WRF-Chem anthro. emission input files
factors = {'no_ac':        {'E_ALD':    [0, 0, 0],
                            'E_C2H5OH': [0],
                            'E_CH3OH':  [0],
                            'E_CO':     [0],
                            'E_ECJ':    [0],
                            'E_ETH':    [0],
                            'E_HC3':    [0],
                            'E_HC5':    [0],
                            'E_HC8':    [0],
                            'E_HCHO':   [0],
                            'E_ISO':    [0],
                            'E_KET':    [0],
                            'E_NH3':    [0],
                            'E_NO':     [0],
                            'E_NO2':    [0],
                            'E_OL2':    [0],
                            'E_OLI':    [0],
                            'E_OLT':    [0, 0, 0],
                            'E_ORA2':   [0],
                            'E_PM25I':  [0],
                            'E_PM25J':  [0],
                            'E_SO2':    [0],
                            'E_TOL':    [0, 0],
                            'E_XYL':    [0]},
                   
           'ac_carb':      {'E_ALD':    [1, 1, 0.5],
                            'E_C2H5OH': [0],
                            'E_CH3OH':  [0],
                            'E_CO':     [1],
                            'E_ECJ':    [1],
                            'E_ETH':    [0],
                            'E_HC3':    [1],
                            'E_HC5':    [0],
                            'E_HC8':    [0],
                            'E_HCHO':   [1],
                            'E_ISO':    [0],
                            'E_KET':    [0],
                            'E_NH3':    [0],
                            'E_NO':     [0.9],
                            'E_NO2':    [0.1],
                            'E_OL2':    [0],
                            'E_OLI':    [0.5],
                            'E_OLT':    [0.5, 0.5, 1],
                            'E_ORA2':   [0],
                            'E_PM25I':  [0.2],
                            'E_PM25J':  [0.8],
                            'E_SO2':    [1],
                            'E_TOL':    [1, 1],
                            'E_XYL':    [1]},
                   
           'ac_hydrocomb': {'E_ALD':          [0, 0, 0],
                            'E_C2H5OH':       [0],
                            'E_CH3OH':        [0],
                            'E_CO':           [0],
                            'E_ECJ':          [0],
                            'E_ETH':          [0],
                            'E_HC3':          [0],
                            'E_HC5':          [0],
                            'E_HC8':          [0],
                            'E_HCHO':         [0],
                            'E_ISO':          [0],
                            'E_KET':          [0],
                            'E_NH3':          [0],
                            'E_NO':           [0.9 * 0.35],
                            'E_NO2':          [0.1 * 0.35],
                            'E_OL2':          [0],
                            'E_OLI':          [0],
                            'E_OLT':          [0, 0, 0],
                            'E_ORA2':         [0],
                            'E_PM25I':        [0],
                            'E_PM25J':        [0],
                            'E_SO2':          [0],
                            'E_TOL':          [0, 0],
                            'E_XYL':          [0]}
                    }

# scenarios
scenario1 = {'short': 'ac_carb',
            'medium': 'ac_carb',
            'long': 'ac_carb'}

scenario2 = {'short': 'no_ac',
            'medium': 'ac_carb',
            'long': 'ac_carb'}

scenario3 = {'short': 'no_ac',
            'medium': 'ac_hydrocomb',
            'long': 'ac_hydrocomb'}

scenario4 = {'short': 'no_ac',
            'medium': 'no_ac',
            'long': 'no_ac'}

scenario = scenario1

# Global attributes per domain
TRUELAT1 = [1.0e20, 1.0e20, 1.0e20, 1.0e20]
TRUELAT2 = [1.0e20, 1.0e20, 1.0e20, 1.0e20]
MOAD_CEN_LAT = [50.0, 50.0, 50.0, 50.0] 
STAND_LON = [1.0, 1.0, 1.0, 1.0] # I think this means the rotation of the Earth (because in combination with lat-lon map projection)
POLE_LAT = [90.0, 90.0, 90.0, 90.0]
POLE_LON = [0.0, 0.0, 0.0, 0.0]
GMT = [0.0, 0.0, 0.0, 0.0]
JULYR = [2013, 2013, 2013, 2013]
JULDAY = [1, 1, 1, 1]
MAP_PROJ = [6, 6, 6, 6]
MMINLU = ["MODIFIED_IGBP_MODIS_NOAH", "MODIFIED_IGBP_MODIS_NOAH", "MODIFIED_IGBP_MODIS_NOAH", "MODIFIED_IGBP_MODIS_NOAH"]
NUM_LAND_CAT = [20, 20, 20, 20]
ISWATER = [17, 17, 17, 17]
ISLAKE = [-1, -1, -1, -1]
ISICE = [15, 15, 15, 15]
ISURBAN = [13, 13, 13, 13]
ISOILWATER = [14, 14, 14, 14] 

'''
-------------------------------------------------------------------------------------------------------------
----------------------------------------------------MAIN-----------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

class example_files():
    
    '''
    Load example files on flight data 
    
    Methods
    ----------
    example_input   : used AEM input file generated with "flight_analysis" class in this script
    example_output  : AEM output file
    '''
    
    def __init__(self):
        self.example_input()
        self.example_output()
    
    def example_input(self):
        
        self.emissions_input = pd.read_csv(path + 'ac_emiss_1min_jun19.so6', delimiter = ' ', header = None, dtype = str)
        
        cols_input = ['FLEP_ID', 'ADEP', 'ADES', 'ICAO_AIRCRAFT_ID', 'START_TIME', 'END_TIME',
                      'START_FL', 'END_FL', 'ATTITUDE', 'CALL_SIGN', 'START_DATE', 'END_DATE',
                      'START_POS_LAT', 'START_POS_LON', 'END_POS_LAT', 'END_POS_LON', 'FLIGHT_ID',
                      'SEQUENCE NUMBER', 'DISTANCE', 'PARITY COLOR']
        
        self.emissions_input.columns = cols_input
        
    def example_output(self):
        self.emissions_output = pd.read_csv(path + 'ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6', 
                                       delimiter = ' ', dtype = str)


class auxiliary():
    
    '''
    Collection of uxiliary functions used throughout this script.
    
    Methods
    ----------
    attitude      : Classify flight segments into flight phases based on altitude and climb/descent rates
    dist          : Euclidean distance between arrays
    fl_to_nm      : Convert flight level to nautical miles
    lon_deg_to_nm : Convert degrees to nautical miles in longitudinal direction
    lat_deg_to_nm : Convert degrees to nautical miles in latitudinal direction
    bin_2d        : Bin 2D field onto a defined grid given a certain statistic
    '''
    
    def attitude(df, row_nr):
        
        try:
            if abs(df.iloc[row_nr]['Flight Level'] - df.iloc[row_nr + 1]['Flight Level']) < 20 and df.iloc[row_nr]['Flight Level'] > 200:
                att = 2
            elif df.iloc[row_nr + 1]['Flight Level'] < df.iloc[row_nr]['Flight Level'] and df.iloc[row_nr + 1]['Flight Level'] > 30:
                att = 1
            elif df.iloc[row_nr + 1]['Flight Level'] < df.iloc[row_nr]['Flight Level'] and df.iloc[row_nr + 1]['Flight Level'] < 30:
                att = 7
            elif df.iloc[row_nr + 1]['Flight Level'] > df.iloc[row_nr]['Flight Level'] and df.iloc[row_nr + 1]['Flight Level'] > 30:
                att = 0
            elif df.iloc[row_nr + 1]['Flight Level'] > df.iloc[row_nr]['Flight Level'] and df.iloc[row_nr + 1]['Flight Level'] < 30:
                att = 6
            elif df.iloc[row_nr]['Flight Level'] == 0 and df.iloc[row_nr + 1]['Flight Level'] == 0:
                att = 4 # assume taxi-in FF = taxi-out FF
            else:
                att = 5
        except:
            att = 9
        return att
    
    def dist(arr1, arr2):
        return abs(arr1 - arr2)
        
    def fl_to_nm(fl):
        ft = fl * 100
        return ft * 3.048e-4 * 0.5399568035
    
    def lon_deg_to_nm(lat, deg):
        circ = 6.378e3 * np.sin(np.radians(90 - lat.astype(float)))
        conv = 2 * m.pi * circ / 360 * 0.5399568035
        return deg * conv
    
    def lat_deg_to_nm(deg):
        conv = 2 * m.pi * 6.378e3 / 360 * 0.5399568035
        return deg * conv
    
    def bin_2d(x_list, y_list, z_list, min_x, max_x, min_y, max_y, res_x, res_y, stat):
        grid = stats.binned_statistic_2d(np.array(x_list).flatten(),
                                                 np.array(y_list).flatten(),
                                                 np.array(z_list).flatten(),
                                                 statistic = stat,
                                                 bins = [round((max_x - min_x) / res_x), round((max_y - min_y) / res_y)],
                                                 range = [[min_x, max_x], [min_y, max_y]])
        
        return grid.statistic


class flight_analysis:
    
    '''
    Process EUROCONTROL data and convert into AEM-readable input files.
    
    Methods
    ----------
    individual_flights  : Import 'Flights' data set - one line per flight containing general info about the flight
    flighttracks        : Import 'Flight Points Actual' data set - flight trajectories
    merge_datasets      : Merge 'Flights' and 'Flight Points Actual' data sets
    interpolate         : Interpolate merged data set in time (1 min in this research)
    distance_flown      : Auxiliary method, calculates distance between two flight points in 3-D space
    gen_adj_df          : Format merged data set into AEM-readable file
    gen_output_file     : Generates input file for AEM (invoke interpolate method before)
    example_trajec (v)  : Example flight between Stansted and Belfast Airport, Figure 4.7 of report
    wkday_flights (v)   : Shows number of flights versus day of the week
    '''
                       
    def __init__(self, month, start_date, end_date):
        '''
        month input format: mm_yyyy, e.g. '06-2019'
        start_date input format: e.g. '01-06-2019 00:00:00'
        end_date input format: e.g. '01-07-2019 00:00:00'
        '''
        self.month = month
        self.start = start_date
        self.end = end_date
    
        self.individual_flights()
        self.flighttracks()
        self.merge_datasets()
        #self.interpolate()
        #self.gen_output_file(save = False)
        
    def individual_flights(self):

        # import flights and format
        try:
            self.flights = pd.read_csv(path + 'Flights_' + self.month + '.csv', sep=';')
            print(self.flights.columns)
            # rename columns
            self.flights.columns = ['ECTRL ID', 'ICAO_dep', 'lat_dep', 'lon_dep', 'ICAO_dest', 'lat_dest', 'lon_dest',
                                'planned_dep_time', 'planned_arr_time', 'dep_time', 'arr_time', 'AC_type',
                                'AC_operator', 'AC_regis', 'flight_type', 'market', 'req_FL', 'dist']
        except:
            self.flights = pd.read_csv(path + 'Flights_' + self.month + '.csv', sep=',')
            print(self.flights.columns)
            # rename columns
            self.flights.columns = ['ECTRL ID', 'ICAO_dep', 'lat_dep', 'lon_dep', 'ICAO_dest', 'lat_dest', 'lon_dest',
                                'planned_dep_time', 'planned_arr_time', 'dep_time', 'arr_time', 'AC_type',
                                'AC_operator', 'AC_regis', 'flight_type', 'market', 'req_FL', 'dist']
        
        print('Original dataset consists of {0} flights.'.format(len(self.flights)))
        
        # invalid values
        crucial_cols = ['ECTRL ID', 'ICAO_dep', 'ICAO_dest', 'dep_time', 'arr_time', 'AC_type'] # columns that should not contain NaN
        
        nan_flights = self.flights[self.flights[crucial_cols].isna().any(axis=1)]
        
        print('Number of flights with important missing data is {0}.'.format(len(nan_flights)))

        nan_flights = self.flights[self.flights[crucial_cols].isna().any(axis=1)]
        
        print('Number of flights with important missing data after recovering known airport locations is {0}.'.format(len(nan_flights)))
        
        self.flights = self.flights[~self.flights[crucial_cols].isna().any(axis=1)]
        
        print('Number of flights after filtering those rows is {0}.'.format(len(self.flights)))


        return self.flights
    
    def flighttracks(self):
        
        self.flighttrack = pd.read_csv(path + 'Flight_Points_Actual_' + self.month + '.csv', sep=',')

        print('Original dataset consists of {0} flightpoints.'.format(len(self.flighttrack)))
        
        # all NaN values are in cols Latitude & Longitude -> drop
        self.flighttrack = self.flighttrack.dropna(how = 'any')
        
        print('Number of flight points after dropping NaN locations is {0}.'.format(len(self.flighttrack)))

        self.flighttrack['Time Over'] = pd.to_datetime(self.flighttrack['Time Over'], format='%d-%m-%Y %H:%M:%S')
        
        start_date = dt.datetime.strptime(self.start, '%d-%m-%Y %H:%M:%S')
        end_date = dt.datetime.strptime(self.end, '%d-%m-%Y %H:%M:%S')
        
        self.flighttrack = self.flighttrack[self.flighttrack['Time Over'].between(start_date, end_date)]
    
        return self.flighttrack

    def merge_datasets(self):
        
        self.flights_merged = self.flighttrack.merge(self.flights, how = "inner", on = ["ECTRL ID"])
        return self.flights_merged
    
    def interpolate(self):
        
        self.resample_interval = input('Interpolate on resample interval: ')
        
        keep_cols = ['Time Over', 'Flight Level', 'Latitude', 'Longitude']
        
        self.flights_IP = self.flights_merged
        
        self.flights_IP['Time Over'] = pd.to_datetime(self.flights_IP['Time Over'], format='%d-%m-%Y %H:%M:%S')
        self.flights_IP['Longitude'] = pd.to_numeric(self.flights_IP['Longitude'], downcast='float')
        self.flights_IP['Latitude'] = pd.to_numeric(self.flights_IP['Latitude'], downcast='float')
        self.flights_IP['Flight Level'] = pd.to_numeric(self.flights_IP['Flight Level'], downcast='float')
                
        dict_list = []
        len_of = len(self.flights_IP['ECTRL ID'].unique())
        
        for idx, ID in enumerate(self.flights_IP['ECTRL ID'].unique()):

            print('{:.2f}%'.format(idx / len_of * 100))
            flight = self.flights_IP[self.flights_IP['ECTRL ID'] == ID]
            ICAO_dep = flight['ICAO_dep'].iloc[0]
            ICAO_dest = flight['ICAO_dest'].iloc[0]
            AC_type = flight['AC_type'].iloc[0]
            AC_regis = flight['AC_regis'].iloc[0]
            flight = flight[keep_cols]
            interpol = flight.set_index('Time Over').resample(self.resample_interval).mean().interpolate(method = 'linear')
            interpol = interpol.reset_index()
            interpol['ECTRL ID'] = [ID] * len(interpol)
            interpol['Sequence Number'] = np.arange(0,len(interpol))
            interpol['ICAO_dep'] = [ICAO_dep] * len(interpol)
            interpol['ICAO_dest'] = [ICAO_dest] * len(interpol)
            interpol['AC_type'] = [AC_type] * len(interpol)
            interpol['AC_regis'] = [AC_regis] * len(interpol)
            
            dict_data = interpol.to_dict('list')
            dict_list.append(dict_data)
        
        self.flights_interpolated = pd.DataFrame.from_dict(dict_list)
        self.flights_interpolated = self.flights_interpolated.set_index(self.flights_interpolated.index).apply(pd.Series.explode).reset_index()
        self.flights_interpolated.drop('index', axis = 1, inplace=True)
    
    def distance_flown(self, array):
        
        delta_lon = list(map(auxiliary.dist, array['Longitude'], array['Longitude'].shift()))
        delta_lon = np.where(np.isnan(delta_lon), 0, delta_lon)
        delta_lat = list(map(auxiliary.dist, array['Latitude'], array['Latitude'].shift()))
        delta_lat = np.where(np.isnan(delta_lat), 0, delta_lat)
        delta_h = list(map(auxiliary.dist, array['Flight Level'], array['Flight Level'].shift()))
        delta_h = np.where(np.isnan(delta_h), 0, delta_h)
        d = np.sqrt(auxiliary.fl_to_nm(delta_h)**2 + auxiliary.lon_deg_to_nm(array['Latitude'], delta_lon)**2 + auxiliary.lat_deg_to_nm(delta_lat)**2)
        return np.concatenate([d[1:], [np.nan]])
    
    def gen_adj_df(self, df):
    
        seq_nr = df['Sequence Number'].tolist()
        seq_nr_right = seq_nr[1:] + seq_nr[:1]
        dates = df['Time Over'].dt.strftime("%y%m%d").tolist()
        times = df['Time Over'].dt.strftime("%H%M%S").tolist()
        lats = (df['Latitude'] * 60).tolist()
        lons = (df['Longitude'] * 60).tolist()
        
        df_new = pd.DataFrame({'FLEP_ID':       ['{}-{}'.format(left, right) for left, right in zip(seq_nr, seq_nr_right)],
                               'ADEP':              df['ICAO_dep'],
                               'ADES':              df['ICAO_dest'],
                               'ICAO_AIRCRAFT_ID':  df['AC_type'],
                               'START_TIME':        times,
                               'END_TIME':          times[1:] + times[:1],
                               'START_FL':          df['Flight Level'].tolist(),
                               'END_FL':            df['Flight Level'].shift(-1).tolist(),
                               'ATTITUDE':          [int(auxiliary.attitude(df, row_nr)) for row_nr in range(len(df))],
                               'CALL_SIGN':         df['AC_regis'],
                               'START_DATE':        dates,
                               'END_DATE':          dates[1:] + dates[:1],
                               'START_POS_LAT':     lats,
                               'START_POS_LON':     lons,
                               'END_POS_LAT':       lats[1:] + lats[:1],
                               'END_POS_LON':       lons[1:] + lons[:1],
                               'FLIGHT_ID':         df['ECTRL ID'],
                               'SEQUENCE NUMBER':   list(np.arange(1, len(df) + 1)),
                               'DISTANCE':          list(self.distance_flown(df)),
                               'PARITY COLOR':      [0] * len(df)
                               }
                              )
        
        return df_new

    def gen_output_file(self, save):
        
        dict_list = []
        lenfl = len(self.flights_interpolated['ECTRL ID'].unique())
        
        for idx, flight_nr in enumerate(self.flights_interpolated['ECTRL ID'].unique()):
            print(str(idx + 1) + ' out of ' + str(lenfl))
            df = self.flights_interpolated[self.flights_interpolated['ECTRL ID'] == flight_nr]
            df_new = self.gen_adj_df(df)
            df_new.drop(df_new.tail(1).index,inplace=True)
            if idx == 0:
                print(df_new)
            dict_data = df_new.to_dict('list')
            dict_list.append(dict_data)

        self.real_emis = pd.DataFrame.from_dict(dict_list)
        self.real_emis = self.real_emis.set_index(self.real_emis.index).apply(pd.Series.explode).reset_index()
        self.real_emis.dropna(inplace = True)
        self.real_emis.reset_index(drop = True, inplace = True)
        self.real_emis.drop(labels=['index'], axis=1, inplace=True)

        if save == True:
            self.real_emis.to_csv(r'ac_emiss_1min_jun19.so6', header=None, index=None, sep=' ', mode='a')

    def example_trajec(self):

        flighttrackhead = self.flighttrack[self.flighttrack['ECTRL ID'] == 230520350]
        x = flighttrackhead['Longitude'].ravel()
        y = flighttrackhead['Latitude'].ravel()
        z = flighttrackhead['Flight Level'].ravel() * 100 * 0.0003048
        
        def hor_dist(x,y):
            
            def lon_deg_to_km(lat, deg):
                circ = 6.378e3 * np.sin(np.radians(90 - lat.astype(float)))
                conv = 2 * m.pi * circ / 360
                return deg * conv
                    
            def lat_deg_to_km(deg):
                conv = 2 * m.pi * 6.378e3 / 360
                return deg * conv
            
            
            delta_lon = abs(x - np.roll(x,1))
            delta_lon[0] = 0
            delta_lat = abs(y - np.roll(y,1))
            delta_lat[0] = 0
            
            d = np.sqrt(lon_deg_to_km(y, delta_lon)**2 + lat_deg_to_km(delta_lat)**2)
            
            return np.cumsum(d)
        
        d = hor_dist(x,y)
        
        flight_interp = flighttrackhead.set_index('Time Over').resample('60S').mean().interpolate(method = 'linear')
        
        x_in = flight_interp['Longitude'].ravel()
        y_in = flight_interp['Latitude'].ravel()
        z_in = flight_interp['Flight Level'].ravel() * 100 * 0.0003048
        d_in = hor_dist(x_in, y_in)
        
        fig, ax = plt.subplots(nrows=2, ncols=1,figsize=(12,10), squeeze=True, gridspec_kw={
                                    'height_ratios': [1, 4], 'width_ratios': [1]})
        
        ax[0].plot(d, z, color = 'black')
        ax[0].scatter(d_in, z_in, color = 'blue', marker = 'x', label = 'interpolated data points')
        ax[0].scatter(d, z, color = 'red', label = 'original data points')
        ax[0].set_yticks(np.arange(0,12,2))
        ax[0].set_xlabel("horizontal distance traveled (km)")
        ax[0].set_ylabel("height (km)")
        ax[0].grid(b=True, which='major', linestyle='--')
        legend = ax[0].legend(frameon=True)
        frame = legend.get_frame()
        frame.set_color('white')
        
        map = Basemap(projection='cyl',llcrnrlat = 47, urcrnrlat = 58,\
                    llcrnrlon = -10, urcrnrlon = 7, resolution='h')
        
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        map.drawparallels(np.arange( -90., 120.,5.),labels=[1,0,0,0],fontsize=20)
        map.drawmeridians(np.arange(-180.,180.,5.),labels=[0,0,0,1],fontsize=20)
        
        #convert latitude/longitude values to plot x/y values
        lon, lat = map(0.235, 51.885)
        plt.plot([lon], [lat], '*', markersize=20, color = 'k')
        plt.text(lon + 0.5, lat, 'Stansted', fontsize=18, color = 'white',
                  bbox=dict(facecolor='black', edgecolor='none', alpha = 0.8))
        lon, lat = map(-6.21583, 54.6575)
        plt.plot([lon], [lat], '*', markersize=20, color = 'k')
        plt.text(lon - 0.5, lat, 'Belfast', fontsize=18, color = 'white', ha = 'right', va = 'center',
                  bbox=dict(facecolor='black', edgecolor='none', alpha = 0.8))
        lon, lat = map(x, y)
        plt.plot(lon, lat, color = 'black')
        lon, lat = map(x[1:-1], y[1:-1])
        plt.plot(lon, lat, 'ok', markersize=5, color = 'red')
        
        for idx, stamp in enumerate(flighttrackhead['Time Over']):
            if idx < 1:
                label = stamp.time()
                lon, lat = map(x[idx], y[idx])
                ax[0].text(d[idx] - 10, z[idx], '  {}'.format(label), fontsize=12, rotation = 90,
                            color = 'red')
            elif idx > 4:
                label = stamp.time()
                lon, lat = map(x[idx], y[idx])
                ax[0].text(d[idx], z[idx], '  {}'.format(label), fontsize=12, rotation = 60,
                            color = 'red')
            else:
                pass
            
    def wkday_flights(self):
        
        all_flights = self.flights
        all_flights['dep_time'] = pd.to_datetime(all_flights['dep_time'], format='%d-%m-%Y %H:%M:%S')
        start_date = dt.datetime.strptime('01-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        end_date = dt.datetime.strptime('15-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        weekflights = all_flights[all_flights['dep_time'].between(start_date, end_date)]
        
        weekflights['dep_day'] = weekflights['dep_time'].apply(lambda x: x.date())
        
        dayfreq = weekflights.groupby(['dep_day'])['dep_day'].count()
        
        plt.figure()
        dayfreq.plot.bar()

class import_files: # e.g. d01 = import_files(domain = 'd01')

    '''
    Used to import simulation domain properties
    '''
    
    def __init__(self, domain):
        
        self.dom = domain
        print('finding domain properties for {}'.format(self.dom))
        self.domain_props()
        
    def domain_props(self):
        
        grid = 'grid ' + self.dom[-1]
        
        grid_wrf = pd.read_excel(path + 'Grid_WRF.xlsx', 'input_res')
        ncols = np.shape(grid_wrf)[1]
            
        names_init = ['name', 'unit']
        nr_grids = int(ncols - 2)
        names = ['grid {0}'.format(gridnr + 1) for gridnr in range(nr_grids)]
        names = names_init + names
        
        grid_wrf.columns = names
        grid_wrf = grid_wrf.dropna(subset = ['name']).reset_index(drop = True)
        grid_wrf = grid_wrf.drop(grid_wrf[grid_wrf.name == 
                               'Grid dimensions (the area that is covered by the grid)'].index).reset_index(drop = True)
        grid_wrf.columns = grid_wrf.columns.str.rstrip()
        
        self.min_lat = grid_wrf[grid_wrf.name == 'latitude bottom (center cell)'].iloc[0][grid]
        self.max_lat = grid_wrf[grid_wrf.name == 'latitude top (center cell)'].iloc[0][grid]
        self.min_lon = grid_wrf[grid_wrf.name == 'longitude left (center cell)'].iloc[0][grid]
        self.max_lon = grid_wrf[grid_wrf.name == 'longitude right (center cell)'].iloc[0][grid]
        self.res_x = grid_wrf[(grid_wrf.name == 'dx =') & (grid_wrf.unit == 'degrees')].iloc[0][grid]
        self.res_y = grid_wrf[(grid_wrf.name == 'dy =') & (grid_wrf.unit == 'degrees')].iloc[0][grid]
        
        self.lons, self.lats = np.meshgrid(np.arange(self.min_lon, self.max_lon + self.res_x, self.res_x),
                                           np.arange(self.min_lat, self.max_lat + self.res_y, self.res_y))
        
        
class output_processing:
    
    '''
    Process AEM output file on air traffic emissions and convert into WRF-Chem-readable input files.
    
    Methods
    ----------
    import_grid                       : Used to import simulation domain properties
    reformat                          : Reformats imported data from AEM output
    interpolate                       : Converts trajectory-based data set to point-based data set
    bin_data                          : Grids AEM output onto 3D grid defined by horizontal domains (grid_WRF.py) and vert_levels.npy
    map_acemis_to_CBM                 : Function maps variables from AEM output to the CBM-Z speciation
    flight_ranges                     : Splits flights into short, medium and long-haul flights
    gen_emission_files                : Generates gridded emission files for WRF-Chem (sums old ground emission files with contribution air traffic)
    barplot_emiss_shares_sectors (v)  : Plots share of short, medium and long-haul flights in various vars, Figure 4.10 of report
    emissions_map (v)                 : Makes contourplots of given variable on various altitudes
    animation (v)                     : Makes animation of given variable. Manually adjust colorbar range
    '''
    
    def __init__(self):

        self.output_file = pd.read_csv(path + 'ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6', 
                                       delimiter = ' ', dtype = str)
        keep_cols = ['SegId', 'ADEP', 'ADES', 'AircraftIcaoId', 'TimeOrig_HHMMSS', 'TimeDest_HHMMSS',
                      'FlOrig', 'FlDest', 'Att', 'DateOrig_YYMMDD', 'DateDest_YYMMDD', 'LatOrig', 'LongOrig',
                      'LatDest', 'LongDest', 'FlightId', 'FlightSeq', 'Length_NM', 'BurntFuel_kg', 'NOX_kg',
                      'SOX_kg', 'CO_kg', 'HC_kg', 'Acetaldehyde_kg', 'Formaldehyde_kg', 'Propianaldehyde_kg',
                      'Acrolein_kg', 'Styrene_kg', 'Ethylbenzene_kg', 'Butadiene_1_3_kg', 'Benzene_kg',
                      'Toluene_kg', 'Xylene_kg', 'Pm25_kg', 'Pm01_kg']
        self.families = {'Aldehydes': ['Acetaldehyde_kg', 'Formaldehyde_kg', 'Propianaldehyde_kg', 'Acrolein_kg'],
                         'NOx': ['NO', 'NO2'],
                         'Ethenes': ['Styrene_kg', 'Ethylbenzene_kg'],
                         'Alkenes': ['Butadiene_1_3_kg']}
        
        self.start_date = dt.datetime.strptime(start_date, '%d-%m-%Y %H:%M:%S')
        self.end_date = dt.datetime.strptime(end_date, '%d-%m-%Y %H:%M:%S')
        self.output_file = self.output_file[keep_cols]
        self.reformat()
        self.interpolate()
        
    def import_grid(self):
        
        self.grid = 'grid ' + str(self.dom + 1)
        print(self.grid)
        self.grid_wrf = pd.read_excel(path + 'Grid_WRF.xlsx', 'input_res')
        self.ncols = np.shape(self.grid_wrf)[1]
            
        self.names_init = ['name', 'unit']
        self.nr_grids = int(self.ncols - 2)
        self.names = ['grid {0}'.format(gridnr + 1) for gridnr in range(self.nr_grids)]
        self.names = self.names_init + self.names
        
        self.grid_wrf.columns = self.names
        self.grid_wrf = self.grid_wrf.dropna(subset = ['name']).reset_index(drop = True)
        self.grid_wrf = self.grid_wrf.drop(self.grid_wrf[self.grid_wrf.name == 
                               'Grid dimensions (the area that is covered by the grid)'].index).reset_index(drop = True)
        self.grid_wrf.columns = self.grid_wrf.columns.str.rstrip()
        
        self.min_lat = self.grid_wrf[self.grid_wrf.name == 'latitude bottom (bottom cell)'].iloc[0][self.grid]
        self.max_lat = self.grid_wrf[self.grid_wrf.name == 'latitude top (top cell)'].iloc[0][self.grid]
        self.min_lon = self.grid_wrf[self.grid_wrf.name == 'longitude left (left cell)'].iloc[0][self.grid]
        self.max_lon = self.grid_wrf[self.grid_wrf.name == 'longitude right (right cell) '].iloc[0][self.grid]
        self.e_sn    = self.grid_wrf[self.grid_wrf.name == 'e_sn ='].iloc[0][self.grid]
        self.e_we    = self.grid_wrf[self.grid_wrf.name == 'e_we ='].iloc[0][self.grid]
        self.res_x   = self.grid_wrf[(self.grid_wrf.name == 'dx =') & (self.grid_wrf.unit == 'degrees')].iloc[0][self.grid]
        self.res_y   = self.grid_wrf[(self.grid_wrf.name == 'dy =') & (self.grid_wrf.unit == 'degrees')].iloc[0][self.grid]
        
    def reformat(self):
        
        # reformat dates and times
        self.output_file['Time_Orig'] = self.output_file['DateOrig_YYMMDD'] + ' ' + self.output_file['TimeOrig_HHMMSS']
        self.output_file['Time_Orig'] = pd.to_datetime(self.output_file['Time_Orig'], format='%y%m%d %H%M%S')
        
        self.output_file['Time_Dest'] = self.output_file['DateDest_YYMMDD'] + ' ' + self.output_file['TimeDest_HHMMSS']
        self.output_file['Time_Dest'] = pd.to_datetime(self.output_file['Time_Dest'], format='%y%m%d %H%M%S')
        self.output_file.drop(labels = ['TimeOrig_HHMMSS', 'TimeDest_HHMMSS', 'DateOrig_YYMMDD', 'DateDest_YYMMDD'],
                                   axis = 1, inplace = True)
        
        # reformat locations from minutes to degrees
        self.output_file['LatOrig']  = pd.to_numeric(self.output_file['LatOrig'], downcast='float') / 60
        self.output_file['LongOrig'] = pd.to_numeric(self.output_file['LongOrig'], downcast='float') / 60
        self.output_file['LatDest']  = pd.to_numeric(self.output_file['LatDest'], downcast='float') / 60
        self.output_file['LongDest'] = pd.to_numeric(self.output_file['LongDest'], downcast='float') / 60
        
        # reformat flight levels from flight level (ft/100) to km
        self.output_file['FlOrig']  = pd.to_numeric(self.output_file['FlOrig'], downcast='float')
        self.output_file['FlOrig'] = auxiliary.fl_to_nm((self.output_file['FlOrig']) / 0.5399568035)
        
        self.output_file['FlDest']  = pd.to_numeric(self.output_file['FlDest'], downcast='float')
        self.output_file['FlDest'] = auxiliary.fl_to_nm((self.output_file['FlDest']) / 0.5399568035)
        
        # reformat all other vars
        self.agg_cols = ['Length_NM', 'BurntFuel_kg', 'NOX_kg', 'SOX_kg', 'CO_kg', 'HC_kg', 'Acetaldehyde_kg',
                    'Formaldehyde_kg', 'Propianaldehyde_kg', 'Acrolein_kg', 'Styrene_kg', 'Ethylbenzene_kg',
                    'Butadiene_1_3_kg', 'Benzene_kg', 'Toluene_kg', 'Xylene_kg', 'Pm25_kg', 'Pm01_kg']
        
        for col in self.agg_cols:
            print(col)
            self.output_file[col] = pd.to_numeric(self.output_file[col], downcast='float') 
        
    def interpolate(self):
        
        self.output_file['Lat'] = self.output_file[['LatOrig', 'LatDest']].mean(axis = 1)
        self.output_file['Long'] = self.output_file[['LongOrig', 'LongDest']].mean(axis = 1)
        self.output_file['Fl'] = self.output_file[['FlOrig', 'FlDest']].mean(axis = 1)
        self.output_file['Time'] = self.output_file[['Time_Orig', 'Time_Dest']].astype(np.int64).mean(axis = 1)
        self.output_file['Time'] = pd.to_datetime(self.output_file['Time'], unit='ns')
    
    def bin_data(self):
                    
        self.import_grid()
                
        self.vert_lay = list(np.load('vert_levels.npy'))
        
        self.lons, self.lats = np.meshgrid(np.arange(self.min_lon + self.res_x, self.max_lon + self.res_x, self.res_x),
                                      np.arange(self.min_lat + self.res_y, self.max_lat + self.res_y, self.res_y))
        
        self.ground_area = np.zeros((np.shape(self.lats)[0], np.shape(self.lats)[1]))
        
        for i, row in enumerate(self.lats):
            for j, col in enumerate(row):
                self.ground_area[i, j] = (auxiliary.lat_deg_to_nm(self.res_y) / 0.5399568035) * (auxiliary.lon_deg_to_nm(col, self.res_x) / 0.5399568035)
        
        # create time variable (cut total time window)
        
        self.freq = '1H' #input('Desired time frequency: ')

        self.daterange = pd.date_range(self.start_date, self.end_date, freq = self.freq)
        
        self.data['time_window'] = pd.cut(self.data['Time'], self.daterange)
        
        self.output_timeseg = list(dict(list(self.data.groupby('time_window'))).values())

        # binning practise
        self.binned_emis_dict = {}
        self.list_vars = ['NOX_kg', 'SOX_kg', 'CO_kg', 'HC_kg', 'Acetaldehyde_kg', 'Formaldehyde_kg', 'Propianaldehyde_kg',
                      'Acrolein_kg', 'Styrene_kg', 'Ethylbenzene_kg', 'Butadiene_1_3_kg', 'Benzene_kg',
                      'Toluene_kg', 'Xylene_kg', 'Pm25_kg', 'Pm01_kg']
        
        self.total_emis = auxiliary.bin_2d(self.data['Long'].tolist(), self.data['Lat'].tolist(),
                         self.data['NOX_kg'].tolist(), self.min_lon, self.max_lon, self.min_lat, self.max_lat, 
                         self.res_x, self.res_y, 'sum')
        
        #for var in self.list_vars:
        for var in self.list_vars:
            print('Gridding variable {}...'.format(var))
            
            self.binned_emis_times = []
            
            for df in self.output_timeseg: # loop through time windows
                
                df['cut'] = pd.cut(x = df['Fl'], 
                            bins = self.vert_lay, include_lowest = True)

                self.output_segmented = list(dict(list(df.groupby('cut'))).values())
                
                self.binned_emis_segs = []
                
                for df_layer in self.output_segmented: # loop through vertical layers
                    binned_emis = auxiliary.bin_2d(df_layer['Long'].tolist(), df_layer['Lat'].tolist(),
                         df_layer[var].tolist(), self.min_lon, self.max_lon, self.min_lat, self.max_lat, 
                         self.res_x, self.res_y, 'sum')
                    try:
                        self.binned_emis_segs.append(binned_emis.T / self.ground_area)
                    except:
                        self.binned_emis_segs.append(binned_emis.T / self.ground_area[:, :-1])
                        
                self.binned_emis_times.append(self.binned_emis_segs)
        
            self.binned_emis_dict[var] = np.stack(self.binned_emis_times)

    def map_acemis_to_CBM(self, dict_ac, mapper):
        
        
        self.mapper =  {'E_ALD':    factors[mapper]['E_ALD'][0] * dict_ac['Acetaldehyde_kg']
                                    + factors[mapper]['E_ALD'][1] * dict_ac['Propianaldehyde_kg']
                                    + factors[mapper]['E_ALD'][2] * dict_ac['Acrolein_kg'],
                        'E_C2H5OH': factors[mapper]['E_C2H5OH'][0] * dict_ac['NOX_kg'],
                        'E_CH3OH':  factors[mapper]['E_CH3OH'][0] * dict_ac['NOX_kg'],
                        'E_CO':     factors[mapper]['E_CO'][0] * dict_ac['CO_kg'],
                        'E_ECJ':    factors[mapper]['E_ECJ'][0] * dict_ac['Pm01_kg'],
                        'E_ETH':    factors[mapper]['E_ETH'][0] * dict_ac['NOX_kg'],
                        'E_HC3':    factors[mapper]['E_HC3'][0] * dict_ac['HC_kg'],
                        'E_HC5':    factors[mapper]['E_HC5'][0] * dict_ac['HC_kg'],
                        'E_HC8':    factors[mapper]['E_HC8'][0] * dict_ac['HC_kg'],
                        'E_HCHO':   factors[mapper]['E_HCHO'][0] * dict_ac['Formaldehyde_kg'],
                        'E_ISO':    factors[mapper]['E_ISO'][0] * dict_ac['NOX_kg'],
                        'E_KET':    factors[mapper]['E_KET'][0] * dict_ac['NOX_kg'],
                        'E_NH3':    factors[mapper]['E_NH3'][0] * dict_ac['NOX_kg'],
                        'E_NO':     factors[mapper]['E_NO'][0] * dict_ac['NOX_kg'],
                        'E_NO2':    factors[mapper]['E_NO2'][0] * dict_ac['NOX_kg'],
                        'E_OL2':    factors[mapper]['E_OL2'][0] * dict_ac['NOX_kg'],
                        'E_OLI':    factors[mapper]['E_OLI'][0] * dict_ac['Butadiene_1_3_kg'],
                        'E_OLT':    factors[mapper]['E_OLT'][0] * dict_ac['Acrolein_kg']
                                    + factors[mapper]['E_OLT'][1] * dict_ac['Styrene_kg']
                                    + factors[mapper]['E_OLT'][2] * dict_ac['Ethylbenzene_kg'],
                        'E_ORA2':   factors[mapper]['E_ORA2'][0] * dict_ac['NOX_kg'],
                        'E_PM25I':  factors[mapper]['E_PM25I'][0] * dict_ac['Pm25_kg'],
                        'E_PM25J':  factors[mapper]['E_PM25J'][0] * dict_ac['Pm25_kg'],
                        'E_SO2':    factors[mapper]['E_SO2'][0] * dict_ac['SOX_kg'],
                        'E_TOL':    factors[mapper]['E_TOL'][0] * dict_ac['Benzene_kg']
                                    + factors[mapper]['E_TOL'][1] * dict_ac['Toluene_kg'],
                        'E_XYL':    factors[mapper]['E_XYL'][0] * dict_ac['Xylene_kg']}
        
        return self.mapper
    
    def flight_ranges(self):
        
        fl_length = self.output_file.groupby(['FlightId'])['Length_NM'].sum()
    
        fl_length = fl_length.multiply(1.852).to_dict() # convert NM to km by multiplying and convert to dict
    
        self.output_file['total_dist'] = self.output_file['FlightId'].map(fl_length)
        
        self.output_file['range'] = pd.cut(self.output_file['total_dist'], bins=[0,1500,4000,20000], labels=['short', 'medium', 'long'], right=False)
        
    def gen_emission_files(self, scenario, nr_domains):
    
        input_times = ['00', '12']  # Chemi input for WRF/chem is split in two 12 hour 
                                    # files, containing hourly data  
                                    
        # Create skeleton NetCDF files for all domains
        chemi_times = np.arange(0, 24)
        
        self.flight_ranges()
        
        for dom in range(0, nr_domains):
            
            self.dom = dom
            self.maps = []
            
            for idx, haul in enumerate(self.output_file['range'].unique()):
                self.data = self.output_file[self.output_file['range'] == haul]
                        
                self.bin_data()

                self.map_acemis_to_CBM(self.binned_emis_dict, scenario[haul])
                self.maps.append(self.mapper)
                
            self.mapping = {}
            
            for key in self.maps[0].keys():
                self.mapping[key] = sum(d[key] for d in self.maps)
                
            for t in input_times:
                start_id = 0 if t == '00' else 12
                end_id = 12 if t == '00' else 24
                name = 'wrfchemi_'+t+'z_d0'+str(self.dom + 1)+'.nc'
                print('-----------\n'+name+'\n-----------')
                domain = nc.Dataset(path + 'wrfchemi_new\\' + 'scenario_1\\' + name, 'w', format='NETCDF3_CLASSIC')
                Time = domain.createDimension('Time', None)
                bottom_top = domain.createDimension('bottom_top', len(self.vert_lay) - 1)
                south_north = domain.createDimension('south_north', self.e_sn - 1)
                west_east = domain.createDimension('west_east', self.e_we - 1)
                DateStrLen = domain.createDimension('DateStrLen', 19)
                
                try:
                    emisfilechemi = nc.Dataset(path + 'wrfchemi_new\\' + 'scenario_1\\' + name)
                except:
                    emisfilechemi = nc.Dataset(path + 'wrfchemi_new\\' + 'scenario_1\\' + (name + '.nc'))
                           
                for var in emisfilechemi.variables:
                    # seek for emission variables
                    if var.startswith('E_'):
                        print(var)
                        var_nc = domain.createVariable(var, np.float32, ('Time', 'bottom_top', 'south_north', 'west_east'))
                        # retrieve variable
                        compound_emiss = emisfilechemi[var][:]
                        # generate 4D array (3 spatial dims on vert. lvls, 1 time dimension)
                        emiss_tot = np.zeros((compound_emiss.shape[0], len(self.vert_lay) - 1, compound_emiss.shape[2], compound_emiss.shape[3]))
                        # fill first layer with ground emissions
                        emiss_tot[:,0,:,:] = compound_emiss[:,0,:,:]
                            
                        ac_emiss = self.mapping[var][start_id:end_id]
                        # PM to ug/m2/s
                        if var == 'E_PM25I' or var == 'E_PM25J' or var == 'E_ECJ':
                            ac_emiss *= 1e9 / 3600 / 1e6
                        else:
                            ac_emiss /= (moll_mass[var.replace('E_', '' ,1)] / 1e3) # CHANGE, MAKE COMPOUND DEPENDENT
                        sum_emis = emiss_tot.sum(axis = 1).mean()
                        sum_ac_emis = ac_emiss.sum(axis = 1).mean()
                        
                        print('Total column {} ground emissions = {:.3f} mol km$^{{-2}}$ hr$^{{-1}}$'.format(var, sum_emis))
                        print('Total column {} aircraft emissions = {:.3f} mol km$^{{-2}}$ hr$^{{-1}}$ ({:.3f}%)'.format(var, sum_ac_emis, sum_ac_emis / (sum_ac_emis + sum_emis) * 100))
                        
                        emiss_tot += ac_emiss
                            
                        print(np.mean(emiss_tot, axis = (0,1,2,3)))
                        # add values and metadata to variable in netCDF file
                        print(emisfilechemi[var].MemoryOrder)
                        var_nc[:] = emiss_tot
                        var_nc.units = emisfilechemi[var].units
                        var_nc.stagger = emisfilechemi[var].stagger
                        var_nc.coordinates = emisfilechemi[var].coordinates
                        var_nc.MemoryOrder = "XYZ"
                        var_nc.FieldType = emisfilechemi[var].FieldType
                        var_nc.description = emisfilechemi[var].description
                        
                bottom_top = domain.createVariable('bottom_top', np.float32, ('bottom_top'))
                bottom_top.units = 'meters' 
                south_north = domain.createVariable('south_north', np.float32, ('south_north'))
                south_north.units = 'degrees_north'
                west_east = domain.createVariable('west_east', np.float32, ('west_east'))
                west_east.units = 'degrees_east'
                
                lat = np.linspace(self.min_lat + 0.5 * self.res_y, self.max_lat - 0.5 * self.res_y, self.e_sn - 1)
                lon = np.linspace(self.min_lon + 0.5 * self.res_x, self.max_lon - 0.5 * self.res_x, self.e_we - 1)
            
                domain.variables['south_north'][:] = lat
                domain.variables['west_east'][:] = lon
                
                Times = domain.createVariable('Times', 'S1', ('Time', 'DateStrLen'))
                
                if t == '00':
                    daytimes = chemi_times[:12]
                elif t == '12':
                    daytimes = chemi_times[12:]
                
                for t in daytimes:
                    date = dt.datetime(2013, 1, 1, chemi_times[t], 0)    
                    # update the time
                    domain.variables['Times'][t%12] = list(date.isoformat().replace('T','_'))
                
                # Set global variables based on wrfinput file
                domain.CEN_LAT = (self.min_lat + self.max_lat) / 2
                domain.CEN_LON = (self.min_lon + self.max_lon) / 2
                domain.TRUELAT1 = TRUELAT1[self.dom]
                domain.TRUELAT2 = TRUELAT2[self.dom]
                domain.MOAD_CEN_LAT = MOAD_CEN_LAT[self.dom]
                domain.STAND_LON = STAND_LON[self.dom]
                domain.POLE_LAT = POLE_LAT[self.dom]
                domain.POLE_LON = POLE_LON[self.dom]
                domain.GMT = GMT[self.dom]
                domain.JULYR = JULYR[self.dom]
                domain.JULDAY = JULDAY[self.dom]
                domain.MAP_PROJ = MAP_PROJ[self.dom]
                domain.MAP_PROJ_CHAR = "Cylindrical Equidistant"
                domain.MMINLU = MMINLU[self.dom]
                domain.ISWATER = ISWATER[self.dom]
                domain.ISLAKE = ISLAKE[self.dom]
                domain.ISICE = ISICE[self.dom]
                domain.ISURBAN = ISURBAN[self.dom]
                domain.ISOILWATER = ISOILWATER[self.dom]
                
                domain.close()
                
    def barplot_emiss_shares_sectors(self):
        
        output_file = self.output_file
        fl_length = output_file.groupby(['FlightId'])['Length_NM'].sum()
        
        fl_length = fl_length.multiply(1.852).to_dict() # convert NM to km by multiplying and convert to dict
        
        output_file['total_dist'] = self.output_file['FlightId'].map(fl_length)
        
        output_file['range'] = pd.cut(output_file['total_dist'], bins=[0,1500,4000,20000], labels=['short', 'medium', 'long'], right=False)
        
        d01 = import_files(domain = 'd01')
        
        flights_dom = output_file[(output_file['LatOrig'] >= d01.min_lat) & (output_file['LatOrig'] <= d01.max_lat) &
                                  (output_file['LongOrig'] >= d01.min_lon) & (output_file['LongOrig'] <= d01.max_lon)]
            
        sums_range = flights_dom.groupby(['range'])[self.agg_cols].sum()
        sums_range.loc['sum'] = sums_range.sum(axis=0)
                        
        category_names = ['Short-haul (0-1,500 km)', 'Medium-haul (1,500-4,000 km)',
                          'Long-haul (4,000+ km)']
        
        def gen_input_dict(var):
            results = {
                'domain 1': [sums_range[var].iloc[0] / sums_range[var].iloc[3] * 100,
                              sums_range[var].iloc[1] / sums_range[var].iloc[3] * 100,
                              sums_range[var].iloc[2] / sums_range[var].iloc[3] * 100]
            }
        
            return results
        
        df1 = pd.DataFrame(gen_input_dict('Length_NM'), index = category_names)
        df2 = pd.DataFrame(gen_input_dict('BurntFuel_kg'), index = category_names)
        df3 = pd.DataFrame(gen_input_dict('Pm25_kg'), index = category_names)
        df4 = pd.DataFrame(gen_input_dict('NOX_kg'), index = category_names)
        df5 = pd.DataFrame(gen_input_dict('SOX_kg'), index = category_names)
        df6 = pd.DataFrame(gen_input_dict('CO_kg'), index = category_names)
        df7 = pd.DataFrame(gen_input_dict('HC_kg'), index = category_names)
        
        def plot_clustered_stacked(dfall):
        
            n_df = len(dfall)
            n_col = len(dfall[0].columns) 
            n_ind = len(dfall[0].index)
            plt.figure(figsize=(14, 10))
            axe = plt.subplot(111)
        
            for df in dfall : # for each data frame
                #df = 
                axe = df.plot(kind="barh",
                              linewidth=5,
                              stacked=True,
                              ax=axe,
                              legend=False,
                              grid=False,
                              color=['peachpuff', 'lavender', 'silver'])
        
            h,l = axe.get_legend_handles_labels() # get the handles we want to modify
            for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
                for j, pa in enumerate(h[i:i+n_col]):
                    for rect in pa.patches: # for each index
                        rect.set_y(rect.get_y() + 1 / float(n_df + 1) * i / float(n_col) - 0.175)
                        rect.set_height(1 / float(n_df + 3))
                        axe.text(rect.get_x() + rect.get_width() / 2,
                                rect.get_height() / 1.75 + rect.get_y(),
                                '{:.1f}%'.format(rect.get_width(), 1),
                                ha='center',
                                va='center',
                                color='k',
                                weight='bold',
                                size=14
                                )
                        
            centers = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.
            downs = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2. - 0.07
            offset = 0.125
            ticks = []
            for i in range(len(centers)):
                ticks.append([downs[i] - 3 * offset, downs[i] - 2 * offset, downs[i] - 1 * offset, 
                              downs[i], downs[i] + 1 * offset, downs[i] + 2 * offset, downs[i] + 3 * offset])
        
            ticklabels = ['flown\ndistance', 'burned\nfuel', 'PM$_{2.5}$', 'NO$_x$', 'SO$_x$', 'CO', 'HCs']
            ticks = np.reshape(np.array(ticks), -1)
            axe.set_yticks(ticks)
            axe.set_yticklabels(ticklabels, rotation = 0, fontsize = 20)
            axe.set_xticks([0,100])
            axe.set_xticklabels(['0%', '100%'])
            axe.invert_yaxis()
            axe.legend(h[:n_col], l[:n_col], ncol=len(category_names), bbox_to_anchor=(0, 1),
                       loc='lower left')
        
            return axe
        
        plot_clustered_stacked([df1.T, df2.T, df3.T, df4.T, df5.T, df6.T, df7.T])

    def emissions_map(self, var, option):
        
        if option == 'small':
            fig, (ax, ax1, cax) = plt.subplots(nrows=1, ncols=3, gridspec_kw={"width_ratios":[1, 1, 0.05]})
            
            ax.set_title('0 km')
            plt.rcParams.update({'font.size': 16})
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            lons, lats = bmap(self.lons, self.lats)
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 0, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 0, :, :], np.arange(0, 10, 1),
                                   extend='max', cmap='Greens', ax = ax)
            
            ax1.set_title('3 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax1)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 10, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax1)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 10, :, :], np.arange(0, 10, 1),
                                   extend='max', cmap='Greens', ax = ax1)
        
        else:
            
            fig, ((ax, ax1, ax5, ax2), (ax3, ax4, ax6, cax)) = plt.subplots(nrows=2, ncols=4, gridspec_kw={"width_ratios":[1, 1, 1, 0.05]})

            ax.set_title('0 km')
            plt.rcParams.update({'font.size': 16})
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            lons, lats = bmap(self.lons, self.lats)
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 0, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 0, :, :], np.arange(0, 10, 1),
                                   extend='max', cmap='Greens', ax = ax)
            
            ax1.set_title('3 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax1)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 3, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax1)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 3, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax1)
                
            ax3.set_title('9 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax3)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 9, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax3)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 9, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax3)
                
            ax4.set_title('12 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax4)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 12, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax4)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 12, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax4)
                
            ax5.set_title('6 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax5)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 6, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax5)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 6, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax5)
                
            ax6.set_title('14 km')
            bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                        llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax6)
            bmap.drawcoastlines()
            bmap.drawcountries()
            bmap.bluemarble()
            
            try:
                im = bmap.contourf(lons, lats, self.mapping[var][0, 14, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax6)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.mapping[var][0, 14, :, :], np.arange(0, 10, 1), 
                                   extend='max', cmap='Greens', ax = ax6)
            
            ip = InsetPosition(ax6, [1.05,0,0.05,1]) 
            cax.set_axes_locator(ip)
            fig.colorbar(im, cax=cax, ax=[ax,ax1])
            
    def animation(self, var, save):
        
        level = 0
        begin_color = 0
        end_color = 6.2
        step_color = 0.2
        cmap = 'ocean_r'
        
        if save == True:
            # Set up formatting for the movie files
            Writer = animation.writers['ffmpeg']
            writer = Writer(fps=2, metadata=dict(artist='Me'), bitrate=1800, extra_args=['-vcodec', 'libx264'])
        
        fig = plt.figure(figsize=(16,12))
        ax = plt.subplot(111)
        plt.rcParams.update({'font.size': 15})#-- create map
        bmap = Basemap(projection='cyl',llcrnrlat = self.min_lat + self.res_y, urcrnrlat = self.max_lat - self.res_y,\
                    llcrnrlon = self.min_lon + self.res_x, urcrnrlon = self.max_lon - self.res_x, resolution='h', ax = ax)
        #-- draw coastlines and country boundaries, edge of map
        bmap.drawcoastlines()
        bmap.drawcountries()
        bmap.bluemarble()
        
        lons, lats = bmap(self.lons, self.lats)
        
        # contourf 
        try:
            im = bmap.contourf(lons, lats, self.binned_emis_dict[var][0, level, :, :], np.arange(begin_color, end_color, step_color), 
                               extend='max', cmap=cmap, ax = ax)
        except:
            im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.binned_emis_dict[var][0, level, :, :], np.arange(begin_color, end_color, step_color),
                               extend='max', cmap=cmap, ax = ax)
            
        cbar = plt.colorbar(im)
        time = dt.datetime.strptime(start_date, '%d-%m-%Y %H:%M:%S')
        title = ax.text(0.5, 1.05, 'Burned fuel (kg hr$^{-1}$ km$^{-2}$) between 0 and 1 km\n' + str(time),
                            ha="center", transform=ax.transAxes,)
        
        def animate(i):
            global im, title
            for c in im.collections:
                c.remove()
            title.remove()
            try:
                im = bmap.contourf(lons, lats, self.binned_emis_dict[var][i, level, :, :], np.arange(begin_color, end_color, step_color), 
                                   extend='max', cmap=cmap, ax = ax)
            except:
                im = bmap.contourf(lons[:, :-1], lats[:, :-1], self.binned_emis_dict[var][i, level, :, :], np.arange(begin_color, end_color, step_color),
                                   extend='max', cmap=cmap, ax = ax)
            time = dt.datetime.strptime(start_date, '%d-%m-%Y %H:%M:%S') + dt.timedelta(hours = i)
            title = ax.text(0.5, 1.05, 'Burned fuel (kg hr$^{-1}$ km$^{-2}$) between 0 and 1 km\n' + str(time),
                            ha="center", transform=ax.transAxes,)
            
        myAnimation = animation.FuncAnimation(fig, animate, frames = 24, interval = 5e2)
        if save == True:
            myAnimation.save('burntfuel_ground.mp4', writer=writer)
        
        
class gen_emission_density_plots:
    
    '''
    Generates emission density plots over domain 1, Figure 4.11 of report
    '''
    
    def __init__(self):
        
        # generate data
        scenario = scenario1
        op1 = output_processing(nr_domains = 1)
        
        scenario = scenario2
        op2 = output_processing(nr_domains = 1)
        
        scenario = scenario3
        op3 = output_processing(nr_domains = 1)

        tot_nox_sc1 = np.sum(op1.mapping['E_NO'], axis = (0, 1)) + np.sum(op1.mapping['E_NO2'], axis = (0, 1))
        self.tot_nox_sc1 = np.where(np.isnan(tot_nox_sc1), 0, tot_nox_sc1)
        tot_nox_sc2 = np.sum(op2.mapping['E_NO'], axis = (0, 1)) + np.sum(op2.mapping['E_NO2'], axis = (0, 1))
        self.tot_nox_sc2 = np.where(np.isnan(tot_nox_sc2), 0, tot_nox_sc2)
        tot_nox_sc3 = np.sum(op3.mapping['E_NO'], axis = (0, 1)) + np.sum(op3.mapping['E_NO2'], axis = (0, 1))
        self.tot_nox_sc3 = np.where(np.isnan(tot_nox_sc3), 0, tot_nox_sc3)
        
        tot_pm25_sc1 = np.sum(op1.mapping['E_PM25I'], axis = (0, 1)) + np.sum(op1.mapping['E_PM25J'], axis = (0, 1)) * 1e3
        self.tot_pm25_sc1 = np.where(np.isnan(tot_pm25_sc1), 0, tot_pm25_sc1)
        tot_pm25_sc2 = np.sum(op2.mapping['E_PM25I'], axis = (0, 1)) + np.sum(op2.mapping['E_PM25J'], axis = (0, 1)) * 1e3
        self.tot_pm25_sc2 = np.where(np.isnan(tot_pm25_sc2), 0, tot_pm25_sc2)
        tot_pm25_sc3 = np.sum(op3.mapping['E_PM25I'], axis = (0, 1)) + np.sum(op3.mapping['E_PM25J'], axis = (0, 1)) * 1e3
        self.tot_pm25_sc3 = np.where(np.isnan(tot_pm25_sc3), 0, tot_pm25_sc3)

        # get domain props
        self.d01 = import_files(domain = 'd01')

    def NOx_sc1(self):
        
        # NOx emission density
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_nox_sc1, np.arange(0, 3.1, 0.1),
                            extend='max', alpha = 0.7, cmap='terrain_r', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('kg km$^{-2}$ day$^{-1}$')
        plt.title('NO$_{x}$ emission density', fontsize = 30)
        
    def PM25_sc1(self):

        # PM25 emission density
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1)
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_pm25_sc1, np.arange(0, 16, 1),
                            extend='max', alpha = 0.7, cmap='terrain_r', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('g km$^{-2}$ day$^{-1}$')
        plt.title('PM$_{2.5}$ emission density', fontsize = 30)

    def NOx_sc2vssc1(self):

        # NOx emission density Sc2 - Sc1
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1)
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_nox_sc2 - self.tot_nox_sc1, np.arange(-3, 0.1, 0.1),
                            extend='min', alpha = 0.7, cmap='terrain', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('kg km$^{-2}$ day$^{-1}$')
        plt.title('$\\Delta$NO$_{x}$ emission density (Sc$_{II}$ - Sc$_{I}$)', fontsize = 30)
        
    def NOx_sc3vssc1(self):

        # NOx emission density Sc3 - Sc1
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1)
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_nox_sc3 - self.tot_nox_sc1, np.arange(-3, 0.1, 0.1),
                            extend='min', alpha = 0.7, cmap='terrain', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('kg km$^{-2}$ day$^{-1}$')
        plt.title('$\\Delta$NO$_{x}$ emission density (Sc$_{III}$ - Sc$_{I}$)', fontsize = 30)
        
    def PM25_sc2vssc1(self):
        
        # PM25 emission density Sc2 - Sc1
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1)
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_pm25_sc2 - self.tot_pm25_sc1, np.arange(-15, 1, 1),
                            extend='min', alpha = 0.7, cmap='terrain', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('g km$^{-2}$ day$^{-1}$')
        plt.title('$\\Delta$PM$_{2.5}$ emission density (Sc$_{II}$ - Sc$_{I}$)', fontsize = 30)
        
    def PM25_sc3vssc1(self):
        
        # PM25 emission density Sc3 - Sc1
        
        fig = plt.figure(figsize=(12, 8))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1)
        
        map = Basemap(projection='cyl',llcrnrlat = self.d01.min_lat + self.d01.res_y, urcrnrlat = self.d01.max_lat - self.d01.res_y,\
                    llcrnrlon = self.d01.min_lon + self.d01.res_x, urcrnrlon = self.d01.max_lon - self.d01.res_x, resolution='h', ax = ax)
            
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.d01.lons, self.d01.lats)
        im = map.contourf(lons, lats, self.tot_pm25_sc3 - self.tot_pm25_sc1, np.arange(-15, 1, 1),
                            extend='min', alpha = 0.7, cmap='terrain', zorder = 2, ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('g km$^{-2}$ day$^{-1}$')
        plt.title('$\\Delta$PM$_{2.5}$ emission density (Sc$_{III}$ - Sc$_{I}$)', fontsize = 30)