# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 15:37:53 2021

@author: TY van der Duim
"""

'''
REQUIRED FILES:
    
    - Emis_TNO7grid_EMEP2019.nc (included)
    - ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6 (not included, >100 MB)
    - wrfchemi_00z_d01.nc, wrfchemi_00z_d02.nc, wrfchemi_00z_d03.nc, wrfchemi_00z_d04.nc (included)
    - wrfchemi_12z_d01.nc, wrfchemi_12z_d02.nc, wrfchemi_12z_d03.nc, wrfchemi_12z_d04.nc (included)
    - wrfaircraftchemi_00z_d01.nc, wrfaircraftchemi_00z_d02.nc, wrfaircraftchemi_00z_d03.nc, wrfaircraftchemi_00z_d04.nc (included)
    - wrfaircraftchemi_12z_d01.nc, wrfaircraftchemi_12z_d02.nc, wrfaircraftchemi_12z_d03.nc, wrfaircraftchemi_12z_d04.nc (included)
'''

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Import packages-----------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

import netCDF4 as nc
import pandas as pd
import datetime
import numpy as np
import math as m
import seaborn as sns
import matplotlib.pyplot as plt
import random
import matplotlib.patches as mpatches
from scipy import stats
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates
import os
os.environ["PROJ_LIB"] = "C:\\Users\\fafri\\anaconda3\\pkgs\\basemap-1.2.2-py38hcd0dc2c_2\\Library\\share\\"

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Plot settings-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

plt.style.use('seaborn-darkgrid')
plt.rc('text', usetex=False)
plt.rc('font', family='century')
plt.rc('xtick', labelsize=18) 
plt.rc('ytick', labelsize=18) 
plt.rc('font', size=20) 

'''
-------------------------------------------------------------------------------------------------------------
----------------------------------------------------Vars-----------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

wrf_00z = ['wrfchemi_00z_d01.nc', 'wrfchemi_00z_d02.nc', 'wrfchemi_00z_d03.nc', 'wrfchemi_00z_d04.nc']
wrf_12z = ['wrfchemi_12z_d01.nc', 'wrfchemi_12z_d02.nc', 'wrfchemi_12z_d03.nc', 'wrfchemi_12z_d04.nc']
wrfaircraft_00z = ['wrfaircraftchemi_00z_d01.nc', 'wrfaircraftchemi_00z_d02.nc', 'wrfaircraftchemi_00z_d03.nc', 'wrfaircraftchemi_00z_d04.nc']
wrfaircraft_12z = ['wrfaircraftchemi_12z_d01.nc', 'wrfaircraftchemi_12z_d02.nc', 'wrfaircraftchemi_12z_d03.nc', 'wrfaircraftchemi_12z_d04.nc']
wrfareas = ['wrfareas_d01.nc', 'wrfareas_d02.nc', 'wrfareas_d03.nc', 'wrfareas_d04.nc']

# directories
home_dir = # home directory
path = home_dir + 'Files\\'

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------------MAIN----------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

class aux:
    
    '''
    Collection of uxiliary functions used throughout this script.
    
    Methods
    ----------
    fl_to_nm               : Convert flight level to nautical miles
    lon_deg_to_nm          : Convert degrees to nautical miles in longitudinal direction
    lat_deg_to_nm          : Convert degrees to nautical miles in latitudinal direction
    binned_stat            : Bin 1D field onto a defined 1D grid given a certain statistic
    distance_flown         : Calculates distance between two flight points in 3-D space
    gen_data_distance_fuel : Generates dataframe with total quantities and distance flown for all flights of specific A/C type
    '''
        
    def fl_to_nm(self, fl):
        ft = fl * 100
        return ft * 3.048e-4
    
    def lon_deg_to_nm(self, lat, deg):
        circ = 6.378e3 * np.sin(np.radians(90 - lat))
        conv = 2 * m.pi * circ / 360
        return deg * conv
    
    def lat_deg_to_nm(self, deg):
        conv = 2 * m.pi * 6.378e3 / 360
        return deg * conv
    
    def binned_stat(self, x_list, y_list, min_x, max_x, res_x, stat):
        grid = stats.binned_statistic(np.array(x_list).flatten(),
                                                 np.array(y_list).flatten(),
                                                 statistic = stat,
                                                 bins = round((max_x - min_x) / res_x),
                                                 range = [min_x, max_x])
        
        return grid

    def distance_flown(self, row):
        delta_lon = abs(row['LongOrig'] - row['LongDest'])
        delta_lon = np.where(np.isnan(delta_lon), 0, delta_lon)
        delta_lat = abs(row['LatOrig'] - row['LatDest'])
        delta_lat = np.where(np.isnan(delta_lat), 0, delta_lat)
        delta_h = abs(row['FlOrig'] - row['FlDest'])
        delta_h = np.where(np.isnan(delta_h), 0, delta_h)
        d = np.sqrt(self.fl_to_nm(delta_h)**2 + self.lon_deg_to_nm(row['LatOrig'], delta_lon)**2 + self.lat_deg_to_nm(delta_lat)**2)
        return d

    def gen_data_distance_fuel(self, ACtype, atts, data):
        ac_data = data[(data['AircraftIcaoId'] == ACtype) & (data['Att'].isin(atts))]
        ac_data = ac_data.reset_index()
        
        ac_data['Time_Orig'] = ac_data['DateOrig_YYMMDD'] + ' ' + ac_data['TimeOrig_HHMMSS']
        ac_data['Time_Orig'] = pd.to_datetime(ac_data['Time_Orig'], format='%y%m%d %H%M%S')
        
        ac_data['Time_Dest'] = ac_data['DateDest_YYMMDD'] + ' ' + ac_data['TimeDest_HHMMSS']
        ac_data['Time_Dest'] = pd.to_datetime(ac_data['Time_Dest'], format='%y%m%d %H%M%S')
        ac_data['Length_NM'] = pd.to_numeric(ac_data['Length_NM'], downcast='float')
        
        ac_data['LatOrig']  = pd.to_numeric(ac_data['LatOrig'], downcast='float') / 60
        ac_data['LongOrig'] = pd.to_numeric(ac_data['LongOrig'], downcast='float') / 60
        ac_data['LatDest']  = pd.to_numeric(ac_data['LatDest'], downcast='float') / 60
        ac_data['LongDest'] = pd.to_numeric(ac_data['LongDest'], downcast='float') / 60
        ac_data['FlOrig']  = pd.to_numeric(ac_data['FlOrig'], downcast='float')
        ac_data['FlDest'] = pd.to_numeric(ac_data['FlDest'], downcast='float')
        ac_data['Time lapse'] = (ac_data['Time_Dest'] - ac_data['Time_Orig']).astype('timedelta64[s]') / 3600
        
        ac_data['distance'] = ac_data.apply(self.distance_flown, axis = 1)
        ac_data['speed'] = ac_data['distance'] / ac_data['Time lapse']
        ac_data['BurntFuel_kg'] = pd.to_numeric(ac_data['BurntFuel_kg'], downcast='float')
        ac_data['NOX_kg'] = pd.to_numeric(ac_data['NOX_kg'], downcast='float')
        ac_data['CO2_kg'] = pd.to_numeric(ac_data['CO2_kg'], downcast='float')
        ac_data['SOX_kg'] = pd.to_numeric(ac_data['SOX_kg'], downcast='float')
        ac_data['H2O_kg'] = pd.to_numeric(ac_data['H2O_kg'], downcast='float')
        ac_data['CO_kg'] = pd.to_numeric(ac_data['CO_kg'], downcast='float')
        ac_data['HC_kg'] = pd.to_numeric(ac_data['HC_kg'], downcast='float')
        print(ac_data.groupby('Att')[['BurntFuel_kg', 'NOX_kg', 'SOX_kg', 'CO_kg']].sum() / len(ac_data['FlightId'].unique()))
        dict_list = []
        
        for idx, ID in enumerate(ac_data['FlightId'].unique()):

            flight = ac_data[ac_data['FlightId'] == ID]
            
            sums = flight[['distance', 'Time lapse', 'BurntFuel_kg', 'NOX_kg', 'CO2_kg', 'SOX_kg', 'H2O_kg', 'CO_kg', 'HC_kg']].sum(axis = 0)
            dict_list.append(sums.to_dict())
        
        return pd.DataFrame.from_dict(dict_list)


class validate:
    
    '''
    Validation tools of emission data.
    
    Methods
    ----------
    val_magn_acvsground : Check air traffic NO emissions versus anthropogenic NO ground emissions
    val_totalemiss      : Check total NOx emissions in EMEP emission inventory for area approximately equal to the Netherlands
    val_groundarea      : Check ground area of each study domain (km^2)
    fuel_dist (v)       : Validation of AEM output by evaluating total emissions as a function of distance for different A/C types, Figure 6.1 of report
    fuel_LTO  (v)       : Same as fuel_dist, but only for the Landing & Take-Off (LTO) phase, Figure 6.2 of report
    '''
    
    def __init__(self):
        
        self.emissions_output = pd.read_csv(path + 'ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6',
                                            delimiter = ' ', dtype = str)
    
    def val_magn_acvsground(self):
        
        for filenr in range(4):
            print('-----------------GRID {}----------------'.format(filenr + 1))
            emisfilechemi_00z = nc.Dataset(path + 'wrfchemi_new\\' + 'scenario_1\\' + wrf_00z[filenr])
            emisfilechemi_12z = nc.Dataset(path + 'wrfchemi_new\\' + 'scenario_1\\' + wrf_12z[filenr])
        
            e_no_00z = emisfilechemi_00z['E_NO'][:]
            e_no_00z = (e_no_00z).mean() # in moles/km2/hr
            
            e_no_12z = emisfilechemi_12z['E_NO'][:]
            e_no_12z = (e_no_12z).mean()
            
            e_no = np.mean([e_no_00z, e_no_12z])
            
            print('Mean NO emissions for grid {} is {:.2f} moles/km2/hr'.format(filenr + 1, e_no))
            
            # retrieve ground area domain
            area_data = nc.Dataset(path + wrfareas[filenr], mode = 'r')
            area_temp = area_data.variables['area'][:,:][0][0]
            print('Ground area for grid {} is {:.2f} km2'.format(filenr + 1, np.sum(area_temp)))
        
            mol_mass_nox = 4.6e-2 # kgs/mol
            total_nox = e_no * np.sum(area_temp) * 24 * 365 * mol_mass_nox / 1e3 / 1e3 # kilo tonnes/year
            print('Total NO emissions for grid {} is {:.2f} kilo tonnes/year'.format(filenr + 1, total_nox))
                
            # aircraft emission data
            emisfileac_00z = nc.Dataset(path + 'wrfaircraftchemi\\' + wrfaircraft_00z[filenr])
            emisfileac_12z = nc.Dataset(path + 'wrfaircraftchemi\\' + wrfaircraft_12z[filenr])
            
            e_no_00z = np.sum(emisfileac_00z['E_NO'][:], axis = 1)
            e_no_00z = (e_no_00z).mean() # in moles/km2/hr
            
            e_no_12z = np.sum(emisfileac_12z['E_NO'][:], axis = 1)
            e_no_12z = (e_no_12z).mean()
            
            emiss_ac = np.mean([e_no_00z, e_no_12z])
                        
            print('Mean aircraft NO emissions for grid {} is {:.2f} moles/km2/hr'.format(filenr + 1, emiss_ac))
            
            print('Percentage aircraft NO emissions of ground NO emissions for grid {} is {:.2f}%'.format(filenr + 1, emiss_ac / e_no * 100))

    
    def val_totalemiss(self):
        # original emission files with emissions in tonnes/year
        
        data = nc.Dataset(path + 'Emis_TNO7grid_EMEP2019.nc')
        
        lat = data['lat'][:]
        lon = data['lon'][:]
        
        lat_range = [51.25, 53.41666667]
        lon_range = [3.916666667, 6.638888889]
        
        lat_cond = (lat >= lat_range[0]) & (lat <= lat_range[1])
        lon_cond = (lon >= lon_range[0]) & (lon <= lon_range[1])
        lon_cond, lat_cond = np.meshgrid(lon_cond, lat_cond)
        
        coord_cond = np.logical_and(lon_cond, lat_cond)
        
        self.nox_emissions = 0
        
        for sec_nr in range(1,12):
            try:
                datachunk = data['nox_sec0{}'.format(sec_nr)][:][0]
            except:
                datachunk = data['nox_sec{}'.format(sec_nr)][:][0]
            self.nox_emissions += np.where(coord_cond, datachunk, 0).sum()
        
        print('{:.1f} tonnes/year'.format(self.nox_emissions))

    def val_groundarea(self):
                
        for idx, filename in enumerate(wrfareas):
            data = nc.Dataset(path + filename, mode = 'r')
            area_temp4 = data.variables['area'][:,:]
            areas = area_temp4[0][0]
            print('The ground area of domain {} is {} km$^2$.'.format(idx + 1, np.sum(areas)))
            
    def fuel_dist(self, var): # var can be 'BurntFuel_kg', 'NOX_kg', 'CO2_kg', 'SOX_kg', 'H2O_kg', 'CO_kg'
        
        auxil = aux()
        # run functions for desired aircraft types
        atts = ['0', '1', '2', '4', '5', '6', '7']
        self.B744 = auxil.gen_data_distance_fuel('B744', atts, self.emissions_output)
        self.B763 = auxil.gen_data_distance_fuel('B763', atts, self.emissions_output)
        self.A310 = auxil.gen_data_distance_fuel('A310', atts, self.emissions_output)
        
        self.B744[var] = self.B744[var] / 1e3
        self.B763[var] = self.B763[var] / 1e3
        self.A310[var] = self.A310[var] / 1e3
        
        # generate plots
        df_b744 = pd.DataFrame({'distance (km)': [3000, 6000, 9000], 'BurntFuel_kg': [33365 / 1e3, 65463 / 1e3, 103139 / 1e3],
                                'NOX_kg': [340.941 / 1e3, 649.547 / 1e3, 1053.912 / 1e3], 'CO2_kg': [105334 / 1e3, 206667 / 1e3, 325611 / 1e3],
                                'SOX_kg': [6.673 / 1e3, 13.093 / 1e3, 20.628 / 1e3], 'H2O_kg': [41740 / 1e3, 81894 / 1e3, 129027 / 1e3],
                                'CO_kg': [136.399 / 1e3, 178.974 / 1e3, 222.249 / 1e3]}) # Data only includes SO2 (sulfur dioxide)
        df_b763 = pd.DataFrame({'distance (km)': [3000, 5000, 7000], 'BurntFuel_kg': [17604 / 1e3, 28808 / 1e3, 41351 / 1e3],
                                'NOX_kg': [191.924 / 1e3, 305.837 / 1e3, 440.822 / 1e3], 'CO2_kg': [55575 / 1e3, 90946 / 1e3, 130544 / 1e3],
                                'SOX_kg': [3.521 / 1e3, 5.762 / 1e3, 8.270 / 1e3], 'H2O_kg': [22022 / 1e3, 36039 / 1e3, 51730 / 1e3],
                                'CO_kg': [40.016 / 1e3, 57.224 / 1e3, 74.857 / 1e3]}) # Data only includes SO2 (sulfur dioxide)
        df_a310 = pd.DataFrame({'distance (km)': [2000, 3000, 4000], 'BurntFuel_kg': [12103 / 1e3, 17457 / 1e3, 23112 / 1e3],
                                'NOX_kg': [162.951 / 1e3, 225.681 / 1e3, 294.491 / 1e3], 'CO2_kg': [38210 / 1e3, 55112 / 1e3, 72964 / 1e3],
                                'SOX_kg': [2.421 / 1e3, 3.491 / 1e3, 4.622 / 1e3], 'H2O_kg': [15141 / 1e3, 21839 / 1e3, 28913 / 1e3],
                                'CO_kg': [44.307 / 1e3, 55.031 / 1e3, 65.957 / 1e3]}) # Data only includes SO2 (sulfur dioxide)
        
        labeldict = {'BurntFuel_kg': 'burned fuel (tonnes)', 'NOX_kg': 'NO$_X$ (tonnes)', 'CO2_kg': 'CO$_2$ (tonnes)',
                     'SOX_kg': 'SO$_X$ (tonnes)', 'H2O_kg': 'H$_2$O (tonnes)', 'CO_kg': 'CO (tonnes)'}
        
        plt.figure(figsize=(10,8))
        sns.regplot(data=self.B744, x="distance", y=var, label = 'B747-400 (AEM)')
        sns.scatterplot(data=df_b744, x="distance (km)", y=var, label = 'B747-400 (FLEM)',
                        color = 'midnightblue', s = 300, marker = '8', zorder = 1)
        sns.regplot(data=self.B763, x="distance", y=var, label = 'B767-300 (AEM)')
        sns.scatterplot(data=df_b763, x="distance (km)", y=var, label = 'B767-300 (FLEM)',
                        color = 'darkred', s = 300, marker = 'X', zorder = 1)
        sns.regplot(data=self.A310, x="distance", y=var, label = 'A310 (AEM)')
        sns.scatterplot(data=df_a310, x="distance (km)", y=var, label = 'A310 (FLEM)',
                        color = 'darkolivegreen', s = 300, marker = 'P', zorder = 1)
        legend = plt.legend(frameon = True, loc = 2)
        frame = legend.get_frame()
        frame.set_color('white')
        plt.xlabel("distance (km)")
        plt.ylabel(labeldict[var])

    def fuel_LTO(self, var): # var can be 'BurntFuel_kg', 'NOX_kg', 'CO2_kg', 'SOX_kg', 'H2O_kg', 'CO_kg'
        
        auxil = aux()
        # run functions for desired aircraft types
        atts = ['4', '5', '6', '7']
        self.B744_LTO = auxil.gen_data_distance_fuel('B744', atts, self.emissions_output)
        self.B763_LTO = auxil.gen_data_distance_fuel('B763', atts, self.emissions_output)
        self.A310_LTO = auxil.gen_data_distance_fuel('A310', atts, self.emissions_output)
        
        if var != 'SOX_kg':
            self.B744_LTO[var] = self.B744_LTO[var] / 1e3
            self.B763_LTO[var] = self.B763_LTO[var] / 1e3
            self.A310_LTO[var] = self.A310_LTO[var] / 1e3
        
        self.B744_LTO['type'] = 'B747-400'
        self.B763_LTO['type'] = 'B767-300'
        self.A310_LTO['type'] = 'A310'
        
        self.ac_LTO = pd.concat([self.B744_LTO, self.B763_LTO, self.A310_LTO], ignore_index=True)
        
        ac_LTO_EURO_burnedfuel = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 3.31968}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 1.72993}),
                                      pd.DataFrame({'type': ['A310'], 'val': 1.53055})], ignore_index=True)
        
        ac_LTO_EURO_CO = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 25.27/1e3}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 29.65/1e3}),
                                      pd.DataFrame({'type': ['A310'], 'val': 13.92/1e3})], ignore_index=True)
        
        ac_LTO_EURO_NOX = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 44.45/1e3}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 26.67/1e3}),
                                      pd.DataFrame({'type': ['A310'], 'val': 18.68/1e3})], ignore_index=True)

        ac_LTO_EURO_SOX = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 2.79}),
                                              pd.DataFrame({'type': ['B767-300'], 'val': 1.45}),
                                              pd.DataFrame({'type': ['A310'], 'val': 1.29})], ignore_index=True)

        dict_EURO = {'BurntFuel_kg': ac_LTO_EURO_burnedfuel, 'CO_kg': ac_LTO_EURO_CO,
                     'NOX_kg': ac_LTO_EURO_NOX, 'SOX_kg': ac_LTO_EURO_SOX}
        
        ac_LTO_FLEM_burnedfuel = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 3.291}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 1.775}),
                                      pd.DataFrame({'type': ['A310'], 'val': 1.463})], ignore_index=True)
        
        ac_LTO_FLEM_CO = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 57973/1e6}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 14467/1e6}),
                                      pd.DataFrame({'type': ['A310'], 'val': 14796/1e6})], ignore_index=True)
        
        ac_LTO_FLEM_NOX = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 42873/1e6}),
                                      pd.DataFrame({'type': ['B767-300'], 'val': 28195/1e6}),
                                      pd.DataFrame({'type': ['A310'], 'val': 23756/1e6})], ignore_index=True)

        ac_LTO_FLEM_SOX = pd.concat([pd.DataFrame({'type': ['B747-400'], 'val': 658/1e3}),
                                              pd.DataFrame({'type': ['B767-300'], 'val': 355/1e3}),
                                              pd.DataFrame({'type': ['A310'], 'val': 293/1e2})], ignore_index=True)

        dict_FLEM = {'BurntFuel_kg': ac_LTO_FLEM_burnedfuel, 'CO_kg': ac_LTO_FLEM_CO,
                     'NOX_kg': ac_LTO_FLEM_NOX, 'SOX_kg': ac_LTO_FLEM_SOX}
        
        labeldict = {'BurntFuel_kg': 'burned fuel (tonnes)', 'NOX_kg': 'NO$_X$ (tonnes)', 'CO2_kg': 'CO$_2$ (tonnes)',
                     'SOX_kg': 'SO$_X$ (kg)', 'H2O_kg': 'H$_2$O (tonnes)', 'CO_kg': 'CO (tonnes)'}
        
        ymax = {'BurntFuel_kg': 4.5, 'NOX_kg': 0.14, 'CO2_kg': 'CO$_2$ (tonnes)',
                     'SOX_kg': 5, 'H2O_kg': 'H$_2$O (tonnes)', 'CO_kg': 0.06}
        
        plt.figure(figsize=(10,8))
        sns.stripplot(x='type', y=var, data=self.ac_LTO)
        sns.scatterplot(x='type', y='val', data=dict_FLEM[var], s = 300, marker = 'X', color = 'k', label = 'FLEM')
        sns.scatterplot(x='type', y='val', data=dict_EURO[var], s = 300, marker = 'X', color = 'darkred', label = 'EMEP/EEA')
        plt.ylim([0,ymax[var]])
        plt.xlim([-0.5,2.5])
        plt.xlabel('aircraft type')
        plt.ylabel(labeldict[var])
        legend = plt.legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')


class ac_emission_stats:
    
    '''
    Statistics of air traffic emissions.
    
    Methods
    ----------
    fuel_per_phase (v)       : Check air traffic NO emissions versus anthropogenic NO ground emissions
    burnrate_per_phase (v)   : Burned fuel per flight phase shown as boxplots, Figure 4.9a in report
    means_fuel               : Computes emission per flight phase for specific area/domain
    fuel_per_grid (v)        : Uses means_fuel to generate pie charts, Figures 4.9cd in report
    time_per_flight (v)      : Time spent in each flight phase shown as boxplots for a defined nr of flights, Figure 4.9b in report
    filter_binvert_data      : Bins data in 3D space per domain
    create_vert_profiles (v) : Mean emissions as a function of altitude
    '''
    
    def __init__(self):
        self.emissions_output = pd.read_csv(path + 'ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6', delimiter = ' ', dtype = str)
        self.phase_dict = {'0': 'Climb', '1': 'Descent', '2': 'Cruise', '4': 'Taxi-out/in', '5': 'Take-off',
                           '6': 'Climb-out', '7': 'Approach/landing'}
        self.newphase_dict = dict(zip(self.phase_dict.values(), self.phase_dict.keys()))

        self.plotorder = ['Taxi-out/in', 'Take-off', 'Climb-out', 'Climb', 'Cruise', 'Descent', 'Approach/landing']
        self.plotindex = [self.newphase_dict[i] for i in self.plotorder]
        self.reformat()
        self.interpolate()
        
    def reformat(self):
        
        # reformat dates and times
        self.emissions_output['Time_Orig'] = self.emissions_output['DateOrig_YYMMDD'] + ' ' + self.emissions_output['TimeOrig_HHMMSS']
        self.emissions_output['Time_Orig'] = pd.to_datetime(self.emissions_output['Time_Orig'], format='%y%m%d %H%M%S')
        
        self.emissions_output['Time_Dest'] = self.emissions_output['DateDest_YYMMDD'] + ' ' + self.emissions_output['TimeDest_HHMMSS']
        self.emissions_output['Time_Dest'] = pd.to_datetime(self.emissions_output['Time_Dest'], format='%y%m%d %H%M%S')
        self.emissions_output.drop(labels = ['TimeOrig_HHMMSS', 'TimeDest_HHMMSS', 'DateOrig_YYMMDD', 'DateDest_YYMMDD'],
                                   axis = 1, inplace = True)
        
        # reformat locations from minutes to degrees
        self.emissions_output['LatOrig']  = pd.to_numeric(self.emissions_output['LatOrig'], downcast='float') / 60
        self.emissions_output['LongOrig'] = pd.to_numeric(self.emissions_output['LongOrig'], downcast='float') / 60
        self.emissions_output['LatDest']  = pd.to_numeric(self.emissions_output['LatDest'], downcast='float') / 60
        self.emissions_output['LongDest'] = pd.to_numeric(self.emissions_output['LongDest'], downcast='float') / 60
        
        # reformat flight levels from flight level (ft/100) to km
        self.emissions_output['FlOrig']  = pd.to_numeric(self.emissions_output['FlOrig'], downcast='float')
        self.emissions_output['FlOrig'] = aux.fl_to_nm(self, (self.emissions_output['FlOrig']) / 0.5399568035)
        
        self.emissions_output['FlDest']  = pd.to_numeric(self.emissions_output['FlDest'], downcast='float')
        self.emissions_output['FlDest'] = aux.fl_to_nm(self, (self.emissions_output['FlDest']) / 0.5399568035)
        
        # reformat all other vars
        self.agg_cols = ['Length_NM', 'BurntFuel_kg', 'NOX_kg', 'SOX_kg', 'CO_kg', 'HC_kg', 'Acetaldehyde_kg',
                    'Formaldehyde_kg', 'Propianaldehyde_kg', 'Acrolein_kg', 'Styrene_kg', 'Ethylbenzene_kg',
                    'Butadiene_1_3_kg', 'Benzene_kg', 'Toluene_kg', 'Xylene_kg', 'Pm25_kg', 'Pm01_kg']
        
        for col in self.agg_cols:
            print(col)
            self.emissions_output[col] = pd.to_numeric(self.emissions_output[col], downcast='float') 
        
    def interpolate(self):
        
        self.emissions_output['Lat'] = self.emissions_output[['LatOrig', 'LatDest']].mean(axis = 1)
        self.emissions_output['Long'] = self.emissions_output[['LongOrig', 'LongDest']].mean(axis = 1)
        self.emissions_output['Fl'] = self.emissions_output[['FlOrig', 'FlDest']].mean(axis = 1)
        self.emissions_output['Time'] = self.emissions_output[['Time_Orig', 'Time_Dest']].astype(np.int64).mean(axis = 1)
        self.emissions_output['Time'] = pd.to_datetime(self.emissions_output['Time'], unit='ns')
     
    def burnrate_per_phase(self):
        
        self.emissions_output['BurnRate_kg/min']  = pd.to_numeric(self.emissions_output['BurnRate_kg/min'], downcast='float')
        self.fuelflow_per_phase = self.emissions_output.groupby(['Att'])['BurnRate_kg/min'].median()
        di = {'0': 'Climb', '1': 'Descent', '2': 'Cruise', '4': 'Taxi-out/in', '5': 'Take-off', '6': 'Climb-out', '7': 'Approach/landing'}
        emissions_output_plot = self.emissions_output.replace({"Att": di})
        
        vert_offset = [15, 0, 0, 0, 0, 15, 15]
        props = dict(boxstyle='round', facecolor='black', alpha=0.5)
        plotindex_burnrate = [self.fuelflow_per_phase.index.tolist().index(i) for i in self.plotindex]

        plt.rc('font', size=24) 
        plt.figure(figsize=(10,8))
        ax = sns.boxplot(x='Att', y='BurnRate_kg/min', data=emissions_output_plot, 
                    order = self.plotorder, showfliers = False)
        ax.set_xticklabels(ax.get_xticklabels(),rotation=30)
        ax.set_xlabel('attitude')
        ax.set_ylabel('fuel flow (kg/min)')
        
        for idx, xtick in enumerate(plotindex_burnrate):
            ax.text(idx, self.fuelflow_per_phase[xtick] + vert_offset[idx], '{:.1f}'.format(self.fuelflow_per_phase[xtick]), 
                    horizontalalignment='center', size='x-small', color='w', weight='semibold', bbox = props)

        return self.fuelflow_per_phase

    def means_fuel(self, min_lat, max_lat, min_lon, max_lon, var):
    
        grid_output = self.emissions_output[(self.emissions_output['Lat'] > min_lat) & 
                                                 (self.emissions_output['Lat'] < max_lat) & 
                                                 (self.emissions_output['Long'] > min_lon) & 
                                                 (self.emissions_output['Long'] < max_lon)]
        print(len(grid_output))
        fuel_per_phase = grid_output.groupby(['Att'])[var].sum()
        fuel_per_phase = fuel_per_phase / fuel_per_phase.sum() * 100
        di = {'0': 'Climb', '1': 'Descent', '2': 'Cruise', '4': 'Taxi-out/in', '5': 'Take-off', '6': 'Climb-out', '7': 'Approach/landing'}
    
        print(fuel_per_phase)
        return [fuel_per_phase[i] for i in self.plotindex]
    
    def fuel_per_grid(self):
        
        means_grid1 = self.means_fuel(42.25, 61.75, -7.25, 17.25, 'NOX_kg')
        means_grid2 = self.means_fuel(48.75, 55.25, 1.25, 9.416666667, 'NOX_kg')
        means_grid3 = self.means_fuel(51.25, 53.41666667, 3.916666667, 6.638888889, 'NOX_kg')
        means_grid4 = self.means_fuel(51.63888889, 53.10185185, 4.361111111, 6.194444444, 'NOX_kg')
        
        pie, ax = plt.subplots(figsize=[10,6])
        fig = plt.gcf()
        _, _, autopcts1 = plt.pie(x=means_grid1, autopct="%.1f%%", labels=self.plotorder,
                pctdistance=0.9, startangle=180, counterclock=False, rotatelabels = True,
                colors = sns.color_palette(), wedgeprops = {'linewidth': 3})
        _, _, autopcts2 = plt.pie(x=means_grid2, autopct="%.1f%%", pctdistance=0.85, startangle=180, 
                radius=0.80, counterclock=False, rotatelabels = True,
                colors = sns.color_palette("deep"))
        _, _, autopcts3 = plt.pie(x=means_grid3, autopct="%.1f%%", pctdistance=0.8, startangle=180, 
                radius=0.60, counterclock=False, rotatelabels = True,
                colors = sns.color_palette("muted"))
        _, _, autopcts4 = plt.pie(x=means_grid4, autopct="%.1f%%", pctdistance=0.75, startangle=180, 
                radius=0.40, counterclock=False, rotatelabels = True,
                colors = sns.color_palette("pastel"))
        plt.setp(autopcts1, **{'color':'white', 'weight':'bold', 'fontsize':16})
        plt.setp(autopcts2, **{'color':'white', 'weight':'bold', 'fontsize':16})
        plt.setp(autopcts3, **{'color':'white', 'weight':'bold', 'fontsize':14})
        plt.setp(autopcts4, **{'color':'white', 'weight':'bold', 'fontsize':12})
        centre_circle2 = plt.Circle((0,0),0.20,fc='white')
        fig.gca().add_artist(centre_circle2)

    def time_per_phase(self, nr_flights):

        flightlist = random.sample(self.emissions_output['FlightId'].unique().tolist(), nr_flights)
        dict_time_list = []
        
        for idx, flight in enumerate(flightlist):
            print('{:.1f}%'.format(idx / len(flightlist) * 100))
            flight = self.emissions_output[self.emissions_output['FlightId'] == flight]
            flight['Time lapse'] = (flight['Time_Dest'] - flight['Time_Orig']).astype('timedelta64[s]') / 60
        
            time_per_phase = flight.groupby(['Att'])['Time lapse'].sum()
            time_per_phase = time_per_phase / time_per_phase.sum() * 100
            dict_time_list.append(time_per_phase.to_dict())
        
        time_phase_df = pd.DataFrame.from_dict(dict_time_list)
        
        di = {'0': 'Climb', '1': 'Descent', '2': 'Cruise', '4': 'Taxi-out/in', '5': 'Take-off', '6': 'Climb-out', '7': 'Approach/landing'}
        time_phase_df = time_phase_df[['0', '1', '2', '4', '5', '6', '7']]
        
        time_phase_df.columns = list((pd.Series(time_phase_df.columns.tolist())).map(di))
        
        plotorder = ['Taxi-out/in', 'Take-off', 'Climb-out', 'Climb', 'Cruise', 'Descent', 'Approach/landing']
        plotindex = [list(di.values()).index(i) for i in plotorder]

        plt.rc('font', size=24) 
        plt.figure(figsize=(10,8))
        ax = sns.boxplot(data=time_phase_df, order = plotorder, showfliers = False)
        medians = time_phase_df.median(axis = 0)
        props = dict(boxstyle='round', facecolor='black', alpha=0.5)
        vert_offset = [0, 10, 10, 0, 0, 0, 10]
        
        for idx, xtick in enumerate(plotindex):
            ax.text(idx, medians[xtick] + vert_offset[idx], '{:.1f}%'.format(medians[xtick]), 
                    horizontalalignment='center', size='x-small', color='w', weight='semibold', bbox = props)
        
        ax.set_xticklabels(ax.get_xticklabels(),rotation=30)
        ax.set_ylim([-5, 105])
        
        legend_patch = mpatches.Patch(color='black', alpha = 0.5, label='median value')
        plt.legend(handles=[legend_patch], prop={'size': 16})
        ax.annotate('x.x%', (4.81, 99.4), color='k', weight='bold', 
                        fontsize=11, ha='center', va='center')
        ax.set_xlabel('attitude')
        ax.set_ylabel('time spent in flight phase (%)')

    def filter_binvert_data(self, gridnr, min_lat, max_lat, min_lon, max_lon, var, option):
        
        grid_emis = self.emissions_output[(self.emissions_output['Lat'] > min_lat) & 
                        (self.emissions_output['Lat'] < max_lat) & 
                        (self.emissions_output['Long'] > min_lon) & 
                        (self.emissions_output['Long'] < max_lon)]
        
        [firstvert, bin_edges, binnr] = aux.binned_stat(self, grid_emis['Fl'], grid_emis[var], grid_emis['Fl'].min(),
                    grid_emis['Fl'].max(), 1, 'sum')
        
        if option == 'moles':
            areas = {1: 3619216, 2: 403866, 3: 44560, 4: 20247}
            firstvert = firstvert / 24 / areas[gridnr] / 4.6e-2
        elif option == 'prop':
            firstvert = firstvert / firstvert.sum() * 100
        data = pd.DataFrame(firstvert, columns=['proportion'])
        
        return grid_emis, data, bin_edges, binnr
    
    def create_vert_profiles(self, option, var):
        
        [data_grid1, data1, bin_edges, _] = self.filter_binvert_data(1, 42.25, 61.75, -7.25, 17.25, var, option)
        [data_grid2, data2, bin_edges, _] = self.filter_binvert_data(2, 48.75, 55.25, 1.25, 9.416666667, var, option)
        [data_grid3, data3, bin_edges, _] = self.filter_binvert_data(3, 51.25, 53.41666667, 3.916666667, 6.638888889, var, option)
        [data_grid4, data4, bin_edges, _] = self.filter_binvert_data(4, 51.63888889, 53.10185185, 4.361111111, 6.194444444, var, option)
        
        fig, ax1 = plt.subplots(figsize=(12,6))
        ax2 = ax1.twinx()
        sns.lineplot(x = 'proportion', y = bin_edges + 0.5, data = data1, marker='o', sort = False, color = 'red', ax=ax1)
        sns.barplot(x = 'proportion', y = bin_edges + 0.5, data = data1, orient = 'h', color = 'red',
                    alpha = 0.2, ax=ax2, label = 'grid 1')
        sns.lineplot(x = 'proportion', y = bin_edges[:-1] + 0.5, data = data2, marker='o', sort = False, color = 'blue', ax=ax1)
        sns.barplot(x = 'proportion', y = bin_edges[:-1][::-1] + 0.5, data = data2, orient = 'h', color = 'blue',
                    alpha = 0.2, ax=ax2, label = 'grid 2')
        sns.lineplot(x = 'proportion', y = bin_edges[:-1] + 0.5, data = data3, marker='o', sort = False, color = 'green', ax=ax1)
        sns.barplot(x = 'proportion', y = bin_edges[:-1][::-1] + 0.5, data = data3, orient = 'h', color = 'green',
                    alpha = 0.2, ax=ax2, label = 'grid 3')
        sns.lineplot(x = 'proportion', y = bin_edges[:-1] + 0.5, data = data4, marker='o', sort = False, color = 'black', ax=ax1)
        sns.barplot(x = 'proportion', y = bin_edges[:-1][::-1] + 0.5, data = data4, orient = 'h', color = 'black',
                    alpha = 0.2, ax=ax2, label = 'grid 4')
        ax2.set(yticklabels=[])
        
        if option == 'prop':
            ax1.set_xlabel('proportion column NO$_x$ emissions')
        elif option == 'moles':
            ax1.set_xlabel('NO$_x$ aircraft emissions (moles km$^{-2}$ hr$^{-1}$)')
        ax1.set_ylabel('altitude (km)')
        plt.legend()



class time_series_SNAPS:
    
    '''
    Anthropogenic emissions as a function of time.
    
    Methods
    ----------
    __heatmap_months__ (v)      : SNAP emission factors per month, Figure 4.2 in report
    __factorplot_hrs__ (v)      : SNAP emission factors per hour, Figure 4.3 in report
    __time_series_species__ (v) : Hourly fluctuations during a day in June 2019 shown as modeled, for NOx, NH3 and ketones, Figure 4.4 in report
    '''
    
    def __heatmap_months__(self):
        
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        
        dict_emis_months = {'Power generation': [1.316, 1.227, 1.099, 1.060, 0.899, 0.775, 0.732, 0.718, 0.810, 0.994, 1.129, 1.241],															
                            'Residential, commercial and\n other combustion': [2.000, 2.230, 1.870, 1.220, 0.570, 0.370, 0.200, 0.180, 0.240, 0.580, 0.970, 1.570],																											
                            'Industrial combustion': [1.104, 1.153, 1.148, 1.057, 0.977, 0.945, 0.854, 0.772, 0.856, 0.987, 1.053, 1.094],																					
                            'Industrial processes': [0.977, 1.036, 1.071, 1.041, 1.023, 1.024, 0.962, 0.880, 0.945, 1.027, 1.024, 0.990],																						
                            'Extraction and distribution\n of fossil fuels': [0.994, 1.001, 1.001, 0.999, 1.000, 0.999, 1.001, 1.001, 1.001, 1.001, 1.001, 1.001],																									
                            'Solvent use': [0.906, 0.915, 0.968, 1.017, 1.035, 1.071, 1.057, 1.024, 1.054, 1.042, 0.977, 0.934],																		
                            'Road transport': [0.893, 0.930, 0.977, 1.011, 1.033, 1.041, 1.036, 1.027, 1.025, 1.064, 1.020, 0.943],																				
                            'Other mobile sources': [0.894, 0.939, 1.008, 1.036, 1.038, 1.082, 1.051, 0.982, 1.025, 1.047, 0.982, 0.916],																				
                            'Waste treatment and disposal': [1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000, 1.000],																								
                            'Agriculture': [0.538, 0.703, 1.319, 1.571, 1.253, 1.061, 0.878, 0.875, 0.984, 1.044, 1.023, 0.751]
                            }
        
        emis_months = pd.DataFrame(dict_emis_months)
        emis_months.index = months
        
        plt.figure()
        ax = sns.heatmap(emis_months.T, annot=True, fmt='.2f', linewidths=.5, cmap="seismic",
                    center=1)
        plt.xticks(rotation=0)
        plt.yticks(rotation=15)
        ax.add_patch(Rectangle((5,0),1,10, fill=False, edgecolor='black', lw=3))
        
    def __factorplot_hrs__(self):

        hrs = np.arange(1,25)
        
        dict_emis_hrs = {'Power generation': [0.79, 0.72, 0.72, 0.71, 0.74, 0.80, 0.92, 1.08, 1.19, 1.22, 1.21, 1.21, 1.17, 1.15, 1.14, 1.13, 1.10, 1.07, 1.04, 1.02, 1.02, 1.01, 0.96, 0.88],															
                            'Residential, commercial and\n other combustion': [0.38, 0.36, 0.36, 0.36, 0.37, 0.50, 1.19, 1.53, 1.57, 1.56, 1.35, 1.16, 1.07, 1.06, 1.00, 0.98, 0.99, 1.12, 1.41, 1.52, 1.39, 1.35, 1, 0.42],																											
                            'Industrial combustion': [0.75, 0.75, 0.78, 0.82, 0.88, 0.95, 1.02, 1.09, 1.16, 1.22, 1.28, 1.30, 1.22, 1.24, 1.25, 1.16, 1.08, 1.01, 0.95, 0.90, 0.85, 0.81, 0.78, 0.75],																					
                            'Industrial processes': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],																						
                            'Extraction and distribution\n of fossil fuels': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],																									
                            'Solvent use': [0.50, 0.35, 0.20, 0.10, 0.10, 0.20, 0.75, 1.25, 1.40, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.50, 1.40, 1.25, 1.10, 1.00, 0.90, 0.80, 0.70],																		
                            'Road transport': [0.19, 0.09, 0.06, 0.05, 0.09, 0.22, 0.86, 1.84, 1.86, 1.41, 1.24, 1.20, 1.32, 1.44, 1.45, 1.59, 2.03, 2.08, 1.51, 1.06, 0.74, 0.62, 0.61, 0.44],																				
                            'Other mobile sources': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],																				
                            'Waste treatment and disposal': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],																								
                            'Agriculture': [0.60, 0.60, 0.60, 0.60, 0.60, 0.65, 0.75, 0.90, 1.10, 1.35, 1.45, 1.60, 1.65, 1.75, 1.70, 1.55, 1.35, 1.10, 0.90, 0.75, 0.65, 0.60, 0.60, 0.60]
                            }
        
        emis_hrs = pd.DataFrame(dict_emis_hrs)
        emis_hrs.index = hrs
        emis_hrs['hour of the day'] = emis_hrs.index
        df = emis_hrs.melt('hour of the day', var_name='Sector', value_name='emission factor')
        
        plt.rc('font', size=24) 
        g = sns.factorplot(x='hour of the day', y='emission factor', hue='Sector', data=df,
                           legend = False)
        plt.grid()
        g.add_legend(prop={'size': 16}, ncol = 2, title = 'Sector', frameon = True,
                     bbox_to_anchor=(0.55, 0.18), loc='lower center', facecolor = 'white')


    def __time_series_species__(self):
        
        hours = ['00', '12']
        
        def column_total_mean_concentration(compound):
            conc_lst = []
            
            for hr in hours:
                file_excl = nc.Dataset(path + 'wrfchemi_new\\scenario_1\\' + 'wrfchemi_{}z_d04.nc'.format(hr))
                conc = file_excl[compound][:]
                conc = np.sum(conc, axis = (1))
                conc = np.mean(conc, axis = (1,2))
                conc_lst.append(conc)
        
            return np.hstack(conc_lst)
        
        e_nox = column_total_mean_concentration('E_NO') + column_total_mean_concentration('E_NO2')
        e_nh3 = column_total_mean_concentration('E_NH3')
        e_ket = column_total_mean_concentration('E_KET')
        
        start_timenw = '01-01-2013 00:00:00'
        end_timenw = '01-01-2013 23:00:00'
        
        timesnw = pd.date_range(start = datetime.datetime.strptime(start_timenw, '%d-%m-%Y %H:%M:%S'), 
                                              end = datetime.datetime.strptime(end_timenw, '%d-%m-%Y %H:%M:%S'),
                                              freq = '1H').to_pydatetime().tolist()
        
        plt.rc('xtick', labelsize=18) 
        fig, ax1 = plt.subplots(figsize=[10,6])
        sns.lineplot(timesnw, e_nox, color = 'lightseagreen', label = 'Nitrogen oxides (NO$_x$)', ax = ax1)
        sns.lineplot(timesnw, e_nh3, color = 'indigo', label = 'Ammonia (NH$_3$)', ax = ax1)
        ax2 = ax1.twinx()
        sns.lineplot(timesnw, e_ket, color = 'maroon', label = 'Ketones', ax = ax2)
        ax1.set_xlabel('hour of the day')
        ax1.set_ylabel('NO$_x$/NH$_3$' + ' (moles km$^{-2}$ hr$^{-1}$)')
        ax2.set_ylabel('Ketones' + ' (moles km$^{-2}$ hr$^{-1}$)')
        
        ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        datemin = np.datetime64(timesnw[0], 'h')
        datemax = np.datetime64(timesnw[-1], 'h')
        ax1.set_xlim(datemin, datemax)
        ax1.xaxis.set_major_locator(mdates.HourLocator(interval=2))
        fig.autofmt_xdate()
        ax1.yaxis.grid(False) # horizontal lines
        ax2.yaxis.grid(False) # horizontal lines
        ax1.legend(loc=2, frameon = True, facecolor = 'white')
        ax2.legend(loc=0, frameon = True, facecolor = 'white')

   