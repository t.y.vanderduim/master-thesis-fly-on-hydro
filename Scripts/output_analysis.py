# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 23:37:44 2021

@author: TY van der Duim
"""

'''
REQUIRED FILES:
    
    - scen{1-4}_wrfout_d0{1-4}.nc (not included, >100 MB)
    - agg_vocscen{1-4}_wrfout_d0{1-4}.nc (not included, >100 MB)
    - pm_scen{1-4}_wrfout_d0{1-4}.nc (not included, >100 MB)
    - albedo_scen{1-4}_wrfout_d0{1-4}.nc (included)
    - Grid_WRF.xlsx (included)
    - ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6 (not included, >100 MB)
    - vert_levels.npy (included)
    - 2019-AQ-METEO.csv (not included, >100 MB)
    - PanEuropean_metadata.csv (included)
    - EEA_{var}_{num}.csv (not included, >100 MB)
    - mean_sc{1-4}_wrfout_d0{1-4}.nc (included)
'''

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Import packages-----------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

import netCDF4 as nc
import pandas as pd
import numpy as np
import math as m
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import os
os.environ["PROJ_LIB"] = "C:\\Users\\fafri\\anaconda3\\pkgs\\basemap-1.2.2-py38hcd0dc2c_2\\Library\\share\\"
from mpl_toolkits.basemap import Basemap
import datetime as dt
from ac_emissions import auxiliary
import matplotlib.cm as cm
from scipy.ndimage import uniform_filter1d
import matplotlib.tri as tri
import matplotlib.colors as mcolors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
import matplotlib as mpl
from scipy import stats
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
from matplotlib.cm import ScalarMappable
import skill_metrics as sm
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Plot settings-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

plt.style.use('seaborn-darkgrid')
plt.rc('text', usetex=False)
plt.rc('font', family='century')
plt.rc('xtick', labelsize=18) 
plt.rc('ytick', labelsize=18) 
plt.rc('font', size=20)

'''
-------------------------------------------------------------------------------------------------------------
----------------------------------------------------Vars-----------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

# directories
home_dir = # home directory
path = home_dir + 'Files\\'

# conversion factors ppbs to micrograms/m3
F_pm25 = 1
F_nox = 0.0409 * 1e3 * 46.01
F_o3 = 0.0409 * 1e3 * 48.00

# WHO limits (micrograms per cubic meter)
lim_ann_no2_who = 10
lim_ann_pm25_who = 5
lim_24hr_no2_who = 25
lim_24hr_pm25_who = 15
lim_8hr_o3 = 100

# airport locations (degrees), indicated by IATA code
lon0_ein = 5.63888889
lat0_ein = 51.44444444
lon0_rot = 4.52777778
lat0_rot = 51.88888889
lon0_ams = 4.75
lat0_ams = 52.3333
lon0_lon = -0.25
lat0_lon = 51.5
lon0_man = -2.25
lat0_man = 53.5
lon0_cdg = 2.58333
lat0_cdg = 49

# indices
idx_nospinup = 48 # hours, first two simulation days (model spin-up time)
ground_lvl = 0

# colors for each scenario
color_sc1 = 'black'
color_sc2 = 'steelblue'
color_sc3 = 'deepskyblue'
color_sc4 = 'darkblue'

sc14colors = ['#696969','#00008B']
sc23colors = ['#4682B4','#00BFFF']

# constants
bolz = 5.6698e-8 # W m-2 K-4, Boltzmann constant

# other parameters
fuel_lim = 7e4 # filter locations based on emitted mass
new_res_lon = 1296 # new interpolated resolution in x
new_res_lat = 1026 # new interpolated resolution in y

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------------MAIN----------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

class aux_func:
    
    '''
    Collection of auxiliary functions used throughout this script.
    
    Methods
    ----------
    sign                    : Add '+' sign when value is above zero, else do nothing (number will automatically contain '-')
    rolling_mean_along_axis : Calculates the rolling mean given a window size, array and axis
    align_yaxis             : Align given values of two opposite y axes
    '''
    
    def sign(val):
    
        if val >= 0:
            return '+'
        else:
            return ''
    
    def rolling_mean_along_axis(array, W, axis=0):
        ''' 
        array: Input ndarray
        W: Window size
        axis: Axis along which we will apply rolling/sliding mean
        ''' 
        hW = W // 2
        L = np.shape(array)[axis] - W + 1   
        indexer = [slice(None) for _ in range(array.ndim)]
        indexer[axis] = slice(hW, hW + L)
        return uniform_filter1d(array, W, axis = axis)[tuple(indexer)]
    
    def align_yaxis(ax1, val_1, ax2, val_2):
        # adjust ax2 ylimit so that val_2 in ax2 is aligned to val_1 in ax1
        _, y1 = ax1.transData.transform((0, val_1))
        _, y2 = ax2.transData.transform((0, val_2))
        inv = ax2.transData.inverted()
        _, dy = inv.transform((0, 0)) - inv.transform((0, y1 - y2))
        miny, maxy = ax2.get_ylim()
        ax2.set_ylim(miny + dy, maxy + dy)
    
    
class MidpointNormalize(mcolors.Normalize):
    '''
    Used for colorbars to center divergent colorbars at a defined midpoint
    '''
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        mcolors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        v_ext = np.max([np.abs(self.vmin), np.abs(self.vmax)])
        x, y = [-v_ext, self.midpoint, v_ext], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))
    

class import_files: # e.g. d01 = import_files(domain = 'd01')
    
    '''
    Get domain-specific data files (WRF-Chem output) and properties
    
    Methods
    ----------
    ind_files    : Loads files containing WRF-Chem output data
    time_mean    : Calculates the time mean of a variable, taking into account the model spin-up time (2 days)
    domain_props : Imports simulation domain properties
    '''
    
    def __init__(self, domain):
        
        self.dom = domain
        
        print('importing individual files for {}'.format(self.dom))
        self.ind_files()
        print('finding domain properties for {}'.format(self.dom))
        self.domain_props()
        
    def ind_files(self):
        
        if self.dom == 'd01':
            # domain 1
            self.scen1_wrfout = nc.Dataset(path + 'GEN\\' + 'scen1_wrfout_d01.nc')
            self.scen2_wrfout = nc.Dataset(path + 'GEN\\' + 'scen2_wrfout_d01.nc')
            self.scen3_wrfout = nc.Dataset(path + 'GEN\\' + 'scen3_wrfout_d01.nc')
            self.scen4_wrfout = nc.Dataset(path + 'GEN\\' + 'scen4_wrfout_d01.nc')
            self.scen1_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen1_wrfout_d01.nc')
            self.scen2_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen2_wrfout_d01.nc')
            self.scen3_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen3_wrfout_d01.nc')
            self.scen4_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen4_wrfout_d01.nc')
            self.scen1_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen1_wrfout_d01.nc')
            self.scen2_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen2_wrfout_d01.nc')
            self.scen3_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen3_wrfout_d01.nc')
            self.scen4_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen4_wrfout_d01.nc')
            self.scen1_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen1_wrfout_d01.nc')
            self.scen2_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen2_wrfout_d01.nc')
            self.scen3_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen3_wrfout_d01.nc')
            self.scen4_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen4_wrfout_d01.nc')
        
            print("Included variables are {}".format(list(self.scen1_wrfout.variables.keys())))

        if self.dom == 'd02':
            # domain 2
            self.scen1_wrfout = nc.Dataset(path + 'GEN\\' + 'scen1_wrfout_d02.nc')
            self.scen2_wrfout = nc.Dataset(path + 'GEN\\' + 'scen2_wrfout_d02.nc')
            self.scen3_wrfout = nc.Dataset(path + 'GEN\\' + 'scen3_wrfout_d02.nc')
            self.scen4_wrfout = nc.Dataset(path + 'GEN\\' + 'scen4_wrfout_d02.nc')
            self.scen1_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen1_wrfout_d02.nc')
            self.scen2_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen2_wrfout_d02.nc')
            self.scen3_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen3_wrfout_d02.nc')
            self.scen4_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen4_wrfout_d02.nc')
            self.scen1_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen1_wrfout_d02.nc')
            self.scen2_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen2_wrfout_d02.nc')
            self.scen3_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen3_wrfout_d02.nc')
            self.scen4_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen4_wrfout_d02.nc')
            self.scen1_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen1_wrfout_d02.nc')
            self.scen2_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen2_wrfout_d02.nc')
            self.scen3_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen3_wrfout_d02.nc')
            self.scen4_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen4_wrfout_d02.nc')
        
        if self.dom == 'd03':
            # domain 3
            self.scen1_wrfout = nc.Dataset(path + 'GEN\\' + 'scen1_wrfout_d03.nc')
            self.scen2_wrfout = nc.Dataset(path + 'GEN\\' + 'scen2_wrfout_d03.nc')
            self.scen3_wrfout = nc.Dataset(path + 'GEN\\' + 'scen3_wrfout_d03.nc')
            self.scen4_wrfout = nc.Dataset(path + 'GEN\\' + 'scen4_wrfout_d03.nc')
            self.scen1_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen1_wrfout_d03.nc')
            self.scen2_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen2_wrfout_d03.nc')
            self.scen3_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen3_wrfout_d03.nc')
            self.scen4_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen4_wrfout_d03.nc')
            self.scen1_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen1_wrfout_d03.nc')
            self.scen2_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen2_wrfout_d03.nc')
            self.scen3_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen3_wrfout_d03.nc')
            self.scen4_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen4_wrfout_d03.nc')
            self.scen1_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen1_wrfout_d03.nc')
            self.scen2_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen2_wrfout_d03.nc')
            self.scen3_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen3_wrfout_d03.nc')
            self.scen4_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen4_wrfout_d03.nc')
        
        if self.dom == 'd04':
            # domain 4
            self.scen1_wrfout = nc.Dataset(path + 'GEN\\' + 'scen1_wrfout_d04.nc')
            self.scen2_wrfout = nc.Dataset(path + 'GEN\\' + 'scen2_wrfout_d04.nc')
            self.scen3_wrfout = nc.Dataset(path + 'GEN\\' + 'scen3_wrfout_d04.nc')
            self.scen4_wrfout = nc.Dataset(path + 'GEN\\' + 'scen4_wrfout_d04.nc')
            self.scen1_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen1_wrfout_d04.nc')
            self.scen2_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen2_wrfout_d04.nc')
            self.scen3_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen3_wrfout_d04.nc')
            self.scen4_wrfout_voc = nc.Dataset(path + 'VOCs\\' + 'agg_vocscen4_wrfout_d04.nc')
            self.scen1_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen1_wrfout_d04.nc')
            self.scen2_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen2_wrfout_d04.nc')
            self.scen3_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen3_wrfout_d04.nc')
            self.scen4_wrfout_pm = nc.Dataset(path + 'PM25\\' + 'pm_scen4_wrfout_d04.nc')
            self.scen1_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen1_wrfout_d04.nc')
            self.scen2_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen2_wrfout_d04.nc')
            self.scen3_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen3_wrfout_d04.nc')
            self.scen4_wrfout_RF = nc.Dataset(path + 'ALBEDO\\' + 'albedo_scen4_wrfout_d04.nc')
        
    def time_mean(self, array, var):
        
        try:
            return np.mean(array[var][idx_nospinup:, :, :, :], axis = 0)
        except:
            return np.mean(array[var][idx_nospinup:, :, :], axis = 0)
        
    def domain_props(self):
        
        grid = 'grid ' + self.dom[-1]
        
        grid_wrf = pd.read_excel(path + 'Grid_WRF.xlsx', 'input_res')
        ncols = np.shape(grid_wrf)[1]
            
        names_init = ['name', 'unit']
        nr_grids = int(ncols - 2)
        names = ['grid {0}'.format(gridnr + 1) for gridnr in range(nr_grids)]
        names = names_init + names
        
        grid_wrf.columns = names
        grid_wrf = grid_wrf.dropna(subset = ['name']).reset_index(drop = True)
        grid_wrf = grid_wrf.drop(grid_wrf[grid_wrf.name == 
                               'Grid dimensions (the area that is covered by the grid)'].index).reset_index(drop = True)
        grid_wrf.columns = grid_wrf.columns.str.rstrip()
        
        self.min_lat = grid_wrf[grid_wrf.name == 'latitude bottom (center cell)'].iloc[0][grid]
        self.max_lat = grid_wrf[grid_wrf.name == 'latitude top (center cell)'].iloc[0][grid]
        self.min_lon = grid_wrf[grid_wrf.name == 'longitude left (center cell)'].iloc[0][grid]
        self.max_lon = grid_wrf[grid_wrf.name == 'longitude right (center cell)'].iloc[0][grid]
        self.res_x = grid_wrf[(grid_wrf.name == 'dx =') & (grid_wrf.unit == 'degrees')].iloc[0][grid]
        self.res_y = grid_wrf[(grid_wrf.name == 'dy =') & (grid_wrf.unit == 'degrees')].iloc[0][grid]
        
        self.lons, self.lats = np.meshgrid(np.arange(self.min_lon, self.max_lon + self.res_x, self.res_x),
                                           np.arange(self.min_lat, self.max_lat + self.res_y, self.res_y))



'''
Get domain data
'''
d01 = import_files(domain = 'd01')
d02 = import_files(domain = 'd02')
d03 = import_files(domain = 'd03')
d04 = import_files(domain = 'd04')


class aircraft_data:
    
    '''
    Get aircraft fuel burn ground level for each domain
    
    Methods
    ----------
    ac_fuelburn         : Imports and formats AEM output on air traffic emissions
    range_airportfl (v) : Shows ratio short, medium and long-haul arriving & departing flights at each defined airport
    '''
    
    def __init__(self):
        self.ac_fuelburn()
        
    def ac_fuelburn(self):
        self.output_file = pd.read_csv(path + 'ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6',
                                       delimiter = ' ', dtype = str)
        
        keep_cols = ['FlightId', 'ADEP', 'ADES', 'FlOrig', 'FlDest', 'LatOrig', 'LongOrig',
                      'LatDest', 'LongDest', 'Length_NM', 'BurntFuel_kg']
        
        self.output_file = self.output_file[keep_cols]
        self.output_file['Length_NM'] = pd.to_numeric(self.output_file['Length_NM'], downcast='float')
        
        fl_length = self.output_file.groupby(['FlightId'])['Length_NM'].sum()
        
        fl_length = fl_length.multiply(1.852).to_dict() # convert NM to km by multiplying and convert to dict
    
        self.output_file['total_dist'] = self.output_file['FlightId'].map(fl_length)
        
        self.output_file['range'] = pd.cut(self.output_file['total_dist'], bins=[0,1500,4000,20000], labels=['short', 'medium', 'long'], right=False)
        
        # reformat locations from minutes to degrees
        self.output_file['LatOrig']  = pd.to_numeric(self.output_file['LatOrig'], downcast='float') / 60
        self.output_file['LongOrig'] = pd.to_numeric(self.output_file['LongOrig'], downcast='float') / 60
        self.output_file['LatDest']  = pd.to_numeric(self.output_file['LatDest'], downcast='float') / 60
        self.output_file['LongDest'] = pd.to_numeric(self.output_file['LongDest'], downcast='float') / 60
        
        # reformat flight levels from flight level (ft/100) to km
        self.output_file['FlOrig']  = pd.to_numeric(self.output_file['FlOrig'], downcast='float')
        self.output_file['FlOrig'] = auxiliary.fl_to_nm((self.output_file['FlOrig']) / 0.5399568035)
        self.output_file['FlDest']  = pd.to_numeric(self.output_file['FlDest'], downcast='float')
        self.output_file['FlDest'] = auxiliary.fl_to_nm((self.output_file['FlDest']) / 0.5399568035)
        
        self.output_file['BurntFuel_kg'] = pd.to_numeric(self.output_file['BurntFuel_kg'], downcast='float')
        
        self.output_file['Lat'] = self.output_file[['LatOrig', 'LatDest']].mean(axis = 1)
        self.output_file['Long'] = self.output_file[['LongOrig', 'LongDest']].mean(axis = 1)
        self.output_file['Fl'] = self.output_file[['FlOrig', 'FlDest']].mean(axis = 1)
        
        vert_lay = list(np.load(path + 'vert_levels.npy'))
        
        self.output_file = self.output_file[self.output_file['Fl'] < vert_lay[1]]
        
        self.burntfuel = []
        
        for dom in [d01, d02, d03, d04]:
            self.burntfuel.append(auxiliary.bin_2d(self.output_file['Long'].tolist(), self.output_file['Lat'].tolist(),
                                     self.output_file['BurntFuel_kg'].tolist(), dom.min_lon - dom.res_x / 2, dom.max_lon + dom.res_x / 2, 
                                     dom.min_lat - dom.res_y / 2, dom.max_lat + dom.res_y / 2,
                                     dom.res_x, dom.res_y, 'sum'))
        
    def range_airportfl(self, airports, airportlabels): # e.g. ac_data.range_airportfl(['LIMC', 'EDDM', 'LOWW', 'LFPG', 'EGLL', 'EHAM', 'EIDW', 'EGCC', 'EDDF', 'EBBR'], ['MXP', 'MUC', 'VIE', 'CDG', 'LHR', 'AMS', 'DUB', 'MAN', 'FRA', 'BRU'])
        
        airport_range = []

        for airport in airports:
            egll = self.output_file[(self.output_file['ADEP'] == airport) | (self.output_file['ADES'] == airport)]
            egll = egll.drop_duplicates(['FlightId'])
            airport_range.append(egll['range'].value_counts() / len(egll['range']) * 100)
            print('{}:\n{}'.format(airport, egll['range'].value_counts() / len(egll['range']) * 100))

        self.airport_range = pd.concat(airport_range, axis = 1, keys = airportlabels)
        
        self.airport_range.T.plot.bar(stacked=True, rot=45, color = plt.get_cmap('RdYlGn')(np.linspace(0.15, 0.85, 3)))
        plt.xlabel('airport')
        plt.ylabel('% range DEP/DES flights')
        legend = plt.legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')
        
ac_data = aircraft_data()
burntfuel = ac_data.burntfuel

def create_df_merged_dom(timeshift, periods, **var):
    
    '''
    Aggregates data on all domains into 1 dataframe, highest resolution data at each unique location
    '''
    
    lats_d01_incl = (d01.lats < d02.lats[1,0]) | (d01.lats > d02.lats[-1,0])
    lons_d01_incl = (d01.lons < d03.lons[0,1]) | (d01.lons > d02.lons[0,-1])
    d01_incl = np.logical_or(lats_d01_incl, lons_d01_incl)
    
    lats_d02_incl = (d02.lats < d03.lats[1,0]) | (d02.lats > d03.lats[-1,0])
    lons_d02_incl = (d02.lons < d03.lons[0,1]) | (d02.lons > d03.lons[0,-1])
    d02_incl = np.logical_or(lats_d02_incl, lons_d02_incl)
    
    lats_d03_incl = (d03.lats < d04.lats[1,0]) | (d03.lats > d04.lats[-1,0])
    lons_d03_incl = (d03.lons < d04.lons[0,1]) | (d03.lons > d04.lons[0,-1])
    d03_incl = np.logical_or(lats_d03_incl, lons_d03_incl)
    
    lats_d04_incl = (d04.lats > d04.lats[0,0]) & (d04.lats < d04.lats[-1,0])
    lons_d04_incl = (d04.lons > d04.lons[0,0]) & (d04.lons < d04.lons[0,-1])
    d04_incl = np.logical_and(lats_d04_incl, lons_d04_incl)
    
    start_date = dt.datetime.strptime('01-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S') + dt.timedelta(hours = timeshift)
    end_date = dt.datetime.strptime('15-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
    daterange = pd.date_range(start_date, end_date, periods = periods)

    dict_list = []
    
    for time in range(len(daterange)):
        print(daterange[time])
        for scenario in range(4):
            
            for idx, (d_filter, lons, lats) in enumerate(zip([d01_incl, d02_incl, d03_incl, d04_incl],
                                            [d01.lons, d02.lons, d03.lons, d04.lons],
                                            [d01.lats, d02.lats, d03.lats, d04.lats])):
            
                lons_incl = lons[d_filter].flatten()
                lats_incl = lats[d_filter].flatten()
                domflag_incl = [idx + 1] * len(lats_incl)
                scen_incl = [scenario + 1] * len(lats_incl)
                time_incl = [daterange[time]] * len(lats_incl)
                
                df = pd.DataFrame(zip(lons_incl, lats_incl, domflag_incl, scen_incl, time_incl),
                                  columns = ['lon', 'lat', 'domain', 'scenario', 'date'])

                for key, value in var.items():
                    if key == 'burntfuel':
                        df[key] = value[idx].T[d_filter].flatten()
                    else:
                        try:
                            df[key] = value[scenario][idx][time][d_filter].flatten()
                        except:
                            df[key] = value[scenario][idx][d_filter].flatten()
    
                dict_data = df.to_dict('list')
                dict_list.append(dict_data)
                    
    emis_df = pd.DataFrame.from_dict(dict_list).apply(pd.Series.explode).reset_index().drop('index', axis = 1)
    
    floatvars = ['NO', 'NO2', 'O3', 'PM25', 'burntfuel', 'VOC', 'SWDOWN', 'T2', 'GRDFLX',
                 'GLW', 'HFX', 'LH', 'ALBEDO', 'EMISS']
    
    for var in floatvars:
        try:
            emis_df[var] = pd.to_numeric(emis_df[var], downcast = 'float')
            if var == 'NO' or var == 'NO2':
                emis_df[var] = emis_df[var] * F_nox
            elif var == 'O3':
                emis_df[var] = emis_df[var] * F_o3
            elif var == 'PM25':
                emis_df[var] = emis_df[var] * F_pm25
                emis_df[var] = np.where(emis_df[var] > 1e4, np.nan, emis_df[var]) # remove invalid vals
            else:
                continue
        except:
            pass
        
    return emis_df


class generic_timemean:
    
    '''
    Two-week mean analysis (mean over total simulation time)
    
    Methods
    ----------
    gen_timemeanlists      : Auxiliary method that generates 2D arrays aggregating data on 4D vars on different domains and scenarios
    main_compounds         : Generates the aggregated dataframe of main variables (NOx, O3, PM25, U10, V10) and linearly interpolates variables onto resolution of d04 (highest resolution)
    total_AQ_total (v)     : 3D plot of time-mean, domain-mean PM25, NO2 and O3 concentrations, Figure 5.1 of report
    gen_timemean3Dlists    : Auxiliary method that generates 2D arrays aggregating data on 3D vars on different domains and scenarios
    gen_timemean3Dlists_RF : Same as gen_timemean3Dlists, but for different files (scen{1-4}_wrfout_RF files)
    RF_vars                : Generates the aggregated dataframe of radiative forcing related vars and linearly interpolates vars onto resolution of d04 (highest resolution)
    meteo_vars             : Generates the aggregated dataframe of meteorological variables (Q2, T2, U10, V10, RAINC, RAINNC) and linearly interpolates vars onto resolution of d04 (highest resolution)
    MAD                    : Function used to calculate the mean absolute deviance (MAD) of an array
    NO2_PM25_vs_annual (v) : Generates Kernel Density Estimates for NO2 and PM25 concentrations, compares with annual WHO guideline concentrations, Figures 5.6 & 5.7 of report
    RP_mean_analysis (v)   : Mean Reduction Potential NO2, O3 & PM25, Figure 5.4 of report
    mean_PBLH (v)          : Mean planetary boundary layer height plotted on a map, Figure 5.3 of report
    locs_shift (v)         : Time mean NO2 or PM25 concentrations shown on map with locations where WHO guideline concentration is exceeded in scenario I but not in scenario IV, Figure 5.8 in report
    pre_proc_local_AQ      : selects location with fuel burn above fuel_lim and retrieves data at those sites
    airports_highfuel (v)  : scatterplot on map of sites where fuel burn above fuel_lim, Figure 4.12 of report
    local_AQ_airports (v)  : 3D plot of time-mean, local (airport) PM25, NO2 and O3 concentrations, Figure 5.12 of report
    spatial_plots (v)      : Generates time-mean plots on a map for PM25, NO2 and O3 concentrations, Figure 5.2 of report
    spread_emis (v)        : Plots histogram of compound concentrations and gives number of locations where air traffic movements are found
    VOCNOX_map (v)         : Plots the VOX/NOx ratio on a map with the major airports indicated, Figure 5.15 of report
    gen_timemeanlists_pm   : Same as gen_timemean3Dlists, but for different files (scen{1-4}_wrfout_pm files)
    aerosols               : Generates the aggregated dataframe of individual aerosols (PM2.5 decomposition) and linearly interpolates variables onto resolution of d04 (highest resolution)
    RP_PM25 (v)            : Mean Reduction Potential aerosols, Figure 5.5 of report
    PM25_decomp (v)        : Breakdown of PM2.5 shown as barplot for different scenarios at different airports, Figure 5.13 of report
    QQplots (v)            : Quantile-Quantile plots used in Section 5.2.2 of report to show normality concentration distribution
    '''
    
    def __init__(self):
        
        self.xi = np.linspace(d01.min_lon, d01.max_lon, new_res_lon)
        self.yi = np.linspace(d01.min_lat, d01.max_lat, new_res_lat)
        self.Xi, self.Yi = np.meshgrid(self.xi, self.yi)
        
        self.main_compounds()
        
    def gen_timemeanlists(self, var):
        
        try:
            arr = [[d01.time_mean(d01.scen1_wrfout,var)[ground_lvl,:,:], d02.time_mean(d02.scen1_wrfout,var)[ground_lvl,:,:],
                    d03.time_mean(d03.scen1_wrfout,var)[ground_lvl,:,:], d04.time_mean(d04.scen1_wrfout,var)[ground_lvl,:,:]],
                  [d01.time_mean(d01.scen2_wrfout,var)[ground_lvl,:,:], d02.time_mean(d02.scen2_wrfout,var)[ground_lvl,:,:],
                    d03.time_mean(d03.scen2_wrfout,var)[ground_lvl,:,:], d04.time_mean(d04.scen2_wrfout,var)[ground_lvl,:,:]],
                  [d01.time_mean(d01.scen3_wrfout,var)[ground_lvl,:,:], d02.time_mean(d02.scen3_wrfout,var)[ground_lvl,:,:],
                    d03.time_mean(d03.scen3_wrfout,var)[ground_lvl,:,:], d04.time_mean(d04.scen3_wrfout,var)[ground_lvl,:,:]],
                  [d01.time_mean(d01.scen4_wrfout,var)[ground_lvl,:,:], d02.time_mean(d02.scen4_wrfout,var)[ground_lvl,:,:],
                    d03.time_mean(d03.scen4_wrfout,var)[ground_lvl,:,:], d04.time_mean(d04.scen4_wrfout,var)[ground_lvl,:,:]]]
        except:
            arr = [[d01.time_mean(d01.scen1_wrfout,var)[:,:], d02.time_mean(d02.scen1_wrfout,var)[:,:],
                    d03.time_mean(d03.scen1_wrfout,var)[:,:], d04.time_mean(d04.scen1_wrfout,var)[:,:]],
                  [d01.time_mean(d01.scen2_wrfout,var)[:,:], d02.time_mean(d02.scen2_wrfout,var)[:,:],
                    d03.time_mean(d03.scen2_wrfout,var)[:,:], d04.time_mean(d04.scen2_wrfout,var)[:,:]],
                  [d01.time_mean(d01.scen3_wrfout,var)[:,:], d02.time_mean(d02.scen3_wrfout,var)[:,:],
                    d03.time_mean(d03.scen3_wrfout,var)[:,:], d04.time_mean(d04.scen3_wrfout,var)[:,:]],
                  [d01.time_mean(d01.scen4_wrfout,var)[:,:], d02.time_mean(d02.scen4_wrfout,var)[:,:],
                    d03.time_mean(d03.scen4_wrfout,var)[:,:], d04.time_mean(d04.scen4_wrfout,var)[:,:]]]

        return arr
    
    def main_compounds(self):
        
        VOC = [[d01.time_mean(d01.scen1_wrfout_voc,'voc')[ground_lvl,:,:], d02.time_mean(d02.scen1_wrfout_voc,'voc')[ground_lvl,:,:],
                d03.time_mean(d03.scen1_wrfout_voc,'voc')[ground_lvl,:,:], d04.time_mean(d04.scen1_wrfout_voc,'voc')[ground_lvl,:,:]],
              [d01.time_mean(d01.scen2_wrfout_voc,'voc')[ground_lvl,:,:], d02.time_mean(d02.scen2_wrfout_voc,'voc')[ground_lvl,:,:],
                d03.time_mean(d03.scen2_wrfout_voc,'voc')[ground_lvl,:,:], d04.time_mean(d04.scen2_wrfout_voc,'voc')[ground_lvl,:,:]],
              [d01.time_mean(d01.scen3_wrfout_voc,'voc')[ground_lvl,:,:], d02.time_mean(d02.scen3_wrfout_voc,'voc')[ground_lvl,:,:],
                d03.time_mean(d03.scen3_wrfout_voc,'voc')[ground_lvl,:,:], d04.time_mean(d04.scen3_wrfout_voc,'voc')[ground_lvl,:,:]],
              [d01.time_mean(d01.scen4_wrfout_voc,'voc')[ground_lvl,:,:], d02.time_mean(d02.scen4_wrfout_voc,'voc')[ground_lvl,:,:],
                d03.time_mean(d03.scen4_wrfout_voc,'voc')[ground_lvl,:,:], d04.time_mean(d04.scen4_wrfout_voc,'voc')[ground_lvl,:,:]]]

        self.emis_df = create_df_merged_dom(timeshift = 0, periods = 1, NO = self.gen_timemeanlists('no'),
                                            NO2 = self.gen_timemeanlists('no2'), O3 = self.gen_timemeanlists('o3'), 
                                            PM25 = self.gen_timemeanlists('PM2_5_DRY'), VOC = VOC,
                                            U10 = self.gen_timemeanlists('U10'), V10 = self.gen_timemeanlists('V10'),
                                            burntfuel = burntfuel)

        print('total # ground grid cells: {}'.format(len(self.emis_df['burntfuel'])))
        print('total # ground grid cells with fuel consumption: {}'.format(len(self.emis_df[self.emis_df['burntfuel'] != 0]['burntfuel'])))
        print('total # ground grid cells with more than a {} kg daily fuel consumption: {}'.format(fuel_lim, len(self.emis_df[self.emis_df['burntfuel'] > fuel_lim]['burntfuel'])))

        '''
        Linearly interpolate domain area
        '''
        
        self.bool_d03d04 = ((self.Xi > d03.min_lon) & (self.Xi < d03.max_lon)) & ((self.Yi > d03.min_lat) & (self.Yi < d03.max_lat))
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp_field = tri.Triangulation(self.emis_df[(self.emis_df['scenario'] == 1)]['lon'], self.emis_df[(self.emis_df['scenario'] == 1)]['lat'])
        
        interpolator_no = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 1)]['NO'])
        self.no_sc1 = interpolator_no(self.Xi, self.Yi)
        interpolator_no = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 2)]['NO'])
        self.no_sc2 = interpolator_no(self.Xi, self.Yi)
        interpolator_no = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 3)]['NO'])
        self.no_sc3 = interpolator_no(self.Xi, self.Yi)
        interpolator_no = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 4)]['NO'])
        self.no_sc4 = interpolator_no(self.Xi, self.Yi)
        
        interpolator_no2 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 1)]['NO2'])
        self.no2_sc1 = interpolator_no2(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 2)]['NO2'])
        self.no2_sc2 = interpolator_no2(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 3)]['NO2'])
        self.no2_sc3 = interpolator_no2(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 4)]['NO2'])
        self.no2_sc4 = interpolator_no2(self.Xi, self.Yi)
        
        interpolator_o3 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 1)]['O3'])
        self.o3_sc1 = interpolator_o3(self.Xi, self.Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 2)]['O3'])
        self.o3_sc2 = interpolator_o3(self.Xi,self. Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 3)]['O3'])
        self.o3_sc3 = interpolator_o3(self.Xi, self.Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 4)]['O3'])
        self.o3_sc4 = interpolator_o3(self.Xi, self.Yi)
        
        interpolator_pm25 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 1)]['PM25'])
        self.pm25_sc1 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 2)]['PM25'])
        self.pm25_sc2 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 3)]['PM25'])
        self.pm25_sc3 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 4)]['PM25'])
        self.pm25_sc4 = interpolator_pm25(self.Xi, self.Yi)
        
        interpolator_voc = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 1)]['VOC'])
        self.voc_sc1 = interpolator_voc(self.Xi, self.Yi)
        interpolator_voc = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 2)]['VOC'])
        self.voc_sc2 = interpolator_voc(self.Xi, self.Yi)
        interpolator_voc = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 3)]['VOC'])
        self.voc_sc3 = interpolator_voc(self.Xi, self.Yi)
        interpolator_voc = tri.LinearTriInterpolator(interp_field, self.emis_df[(self.emis_df['scenario'] == 4)]['VOC'])
        self.voc_sc4 = interpolator_voc(self.Xi, self.Yi)
        
    def total_AQ_total(self):
        
        self.mean_vals = pd.DataFrame({'NO2': [np.mean(self.no2_sc1[self.bool_d03d04]), np.mean(self.no2_sc2[self.bool_d03d04]),
                                               np.mean(self.no2_sc3[self.bool_d03d04]), np.mean(self.no2_sc4[self.bool_d03d04])],
                                       'O3': [np.mean(self.o3_sc1), np.mean(self.o3_sc2), np.mean(self.o3_sc3), np.mean(self.o3_sc4)],
                                       'PM25': [np.mean(self.pm25_sc1), np.mean(self.pm25_sc2), np.mean(self.pm25_sc3), np.mean(self.pm25_sc4)]})


        xs = self.mean_vals['NO2'].values
        ys = self.mean_vals['O3'].values
        zs = self.mean_vals['PM25'].values
        
        v = []
        h = min(zs)
        lw = 1
        
        for k in range(0, len(xs) - 1):
            x = [xs[k], xs[k+1], xs[k+1], xs[k]]
            y = [ys[k], ys[k+1], ys[k+1], ys[k]]
            z = [zs[k], zs[k+1],       h,     h]
            v.append(list(zip(x, y, z))) 
            
        poly3dCollection = Poly3DCollection(v)
        poly3dCollection.set_alpha(0.2)
        poly3dCollection.set_facecolor('black')
        
        fig = plt.figure()
        plt.style.use('seaborn-white')
        ax = Axes3D(fig)
        ax.add_collection3d(poly3dCollection)
        ax.xaxis.labelpad=20
        ax.yaxis.labelpad=20
        ax.zaxis.labelpad=20
        ax.plot(xs.flatten(), ys.flatten(), zs.flatten(), color = 'black')
        ax.scatter(xs.flatten(), ys.flatten(), zs.flatten(), s = 150, color = [color_sc1, color_sc2, color_sc3, color_sc4])
        
        ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                [h, zs.flatten()[0]], color = color_sc1, lw = lw)
        ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                [h, zs.flatten()[1]], color = color_sc2, lw = lw)
        ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                [h, zs.flatten()[2]], color = color_sc3, lw = lw)
        ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                [h, zs.flatten()[3]], color = color_sc4, lw = lw)
        
        ax.plot([xs.flatten()[0], xs.flatten()[0]], [min(ys.flatten()) -  0.06, ys.flatten()[0]], 
                [h, h], linestyle = 'dashed', color = color_sc1, lw = lw)
        ax.plot([xs.flatten()[1], xs.flatten()[1]], [min(ys.flatten()) -  0.06, ys.flatten()[1]], 
                [h, h], linestyle = 'dashed', color = color_sc2, lw = lw)
        ax.plot([xs.flatten()[2], xs.flatten()[2]], [min(ys.flatten()) -  0.06, ys.flatten()[2]], 
                [h, h], linestyle = 'dashed', color = color_sc3, lw = lw)
        ax.plot([xs.flatten()[3], xs.flatten()[3]], [min(ys.flatten()) -  0.06, ys.flatten()[3]], 
                [h, h], linestyle = 'dashed', color = color_sc4, lw = lw)
        
        ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], max(ys.flatten()) + 0.01], 
                [zs.flatten()[0], zs.flatten()[0]], color = color_sc1, lw = lw)
        ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], max(ys.flatten()) + 0.01], 
                [zs.flatten()[1], zs.flatten()[1]], color = color_sc2, lw = lw)
        ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], max(ys.flatten()) + 0.01], 
                [zs.flatten()[2], zs.flatten()[2]], color = color_sc3, lw = lw)
        ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], max(ys.flatten()) + 0.01], 
                [zs.flatten()[3], zs.flatten()[3]], color = color_sc4, lw = lw)
        
        ax.plot([max(xs.flatten()) + 0.021, xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                [h, h], linestyle = 'dashed', color = color_sc1, lw = lw)
        ax.plot([max(xs.flatten()) + 0.021, xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                [h, h], linestyle = 'dashed', color = color_sc2, lw = lw)
        ax.plot([max(xs.flatten()) + 0.021, xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                [h, h], linestyle = 'dashed', color = color_sc3, lw = lw)
        ax.plot([max(xs.flatten()) + 0.021, xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                [h, h], linestyle = 'dashed', color = color_sc4, lw = lw)
        
        ax.plot([max(xs.flatten()) + 0.02, xs.flatten()[0]], [max(ys.flatten()) + 0.01, max(ys.flatten()) + 0.01], 
                [zs.flatten()[0], zs.flatten()[0]], linestyle = 'dashed', color = color_sc1, lw = lw)
        ax.plot([max(xs.flatten()) + 0.02, xs.flatten()[1]], [max(ys.flatten()) + 0.01, max(ys.flatten()) + 0.01], 
                [zs.flatten()[1], zs.flatten()[1]], linestyle = 'dashed', color = color_sc2, lw = lw)
        ax.plot([max(xs.flatten()) + 0.02, xs.flatten()[2]], [max(ys.flatten()) + 0.01, max(ys.flatten()) + 0.01], 
                [zs.flatten()[2], zs.flatten()[2]], linestyle = 'dashed', color = color_sc3, lw = lw)
        ax.plot([max(xs.flatten()) + 0.02, xs.flatten()[3]], [max(ys.flatten()) + 0.01, max(ys.flatten()) + 0.01], 
                [zs.flatten()[3], zs.flatten()[3]], linestyle = 'dashed', color = color_sc4, lw = lw)
        
        
        ax.text(xs.flatten()[0] - 0.002, ys.flatten()[0], zs.flatten()[0], "Scenario I", color=color_sc1, ha = 'right')
        ax.text(xs.flatten()[1] - 0.002, ys.flatten()[1], zs.flatten()[1], "Scenario II", color=color_sc2, ha = 'right')
        ax.text(xs.flatten()[2] - 0.002, ys.flatten()[2], zs.flatten()[2], "Scenario III", color=color_sc3, ha = 'right')
        ax.text(xs.flatten()[3] - 0.002, ys.flatten()[3], zs.flatten()[3], "Scenario IV", color=color_sc4, ha = 'right')
        
        x = (xs - xs[0]) / xs[0] * 100
        y = (ys - ys[0]) / ys[0] * 100
        z = (zs - zs[0]) / zs[0] * 100
        
        ax.text(8.677, max(ys.flatten()), zs.flatten()[1] - 0.002, '{:.2f}%'.format(z[1]), size = 15,
                color=color_sc2, zdir = 'x')
        ax.text(8.677, max(ys.flatten()), zs.flatten()[2] - 0.002, '{:.2f}%'.format(z[2]), size = 15,
                color=color_sc3, zdir = 'x')
        ax.text(8.676, max(ys.flatten()), zs.flatten()[3] - 0.002, '{:.2f}%'.format(z[3]), size = 15,
                color=color_sc4, zdir = 'x')
        
        ax.text(8.681, ys.flatten()[1] - 0.014, min(zs.flatten()), '{:.2f}%'.format(y[1]), size = 15,
                color=color_sc2, zdir = 'x')
        ax.text(8.681, ys.flatten()[2] - 0.014, min(zs.flatten()), '{:.2f}%'.format(y[2]), size = 15,
                color=color_sc3, zdir = 'x')
        ax.text(8.681, ys.flatten()[3] - 0.017, min(zs.flatten()), '{:.2f}%'.format(y[3]), size = 15,
                color=color_sc4, zdir = 'x')
        
        ax.text(xs.flatten()[1] - 0.002, 75.079, min(zs.flatten()), '{:.2f}%'.format(x[1]), size = 15,
                color=color_sc2, zdir = 'y')
        ax.text(xs.flatten()[2] - 0.002, 75.079, min(zs.flatten()), '{:.2f}%'.format(x[2]), size = 15,
                color=color_sc3, zdir = 'y')
        ax.text(xs.flatten()[3] - 0.002, 75.079, min(zs.flatten()), '{:.2f}%'.format(x[3]), size = 15,
                color=color_sc4, zdir = 'y')
        
        ax.quiver(xs.flatten()[0], ys.flatten()[0], zs.flatten()[0], xs.flatten()[1] - xs.flatten()[0], 
                  ys.flatten()[1] - ys.flatten()[0], zs.flatten()[1] - zs.flatten()[0], arrow_length_ratio = 0.03, color = 'k', label = 'Munich Airport (MUC)')
        ax.quiver(xs.flatten()[1], ys.flatten()[1], zs.flatten()[1], xs.flatten()[2] - xs.flatten()[1], 
                  ys.flatten()[2] - ys.flatten()[1], zs.flatten()[2] - zs.flatten()[1], arrow_length_ratio = 0.03, color = 'k')
        ax.quiver(xs.flatten()[2], ys.flatten()[2], zs.flatten()[2], xs.flatten()[3] - xs.flatten()[2], 
                  ys.flatten()[3] - ys.flatten()[2], zs.flatten()[3] - zs.flatten()[2], arrow_length_ratio = 0.03, color = 'k')
        
        ax.set_xlim(min(xs), max(xs) + 0.02)
        ax.set_ylim(min(ys) - 0.051, max(ys))
        ax.set_zlim(min(zs), max(zs) + 0.01)
        ax.set_xlabel("NO$_2$ ($\\mu$g m$^{-3}$)", fontweight='bold')
        ax.set_ylabel("O$_3$ ($\\mu$g m$^{-3}$)", fontweight='bold')
        ax.set_zlabel("PM$_{2.5}$ ($\\mu$g m$^{-3}$)", fontweight='bold')
                
        
    def gen_timemean3Dlists(self, var):
        
        arr = [[d01.time_mean(d01.scen1_wrfout,var)[:,:], d02.time_mean(d02.scen1_wrfout,var)[:,:],
               d03.time_mean(d03.scen1_wrfout,var)[:,:], d04.time_mean(d04.scen1_wrfout,var)[:,:]],
              [d01.time_mean(d01.scen2_wrfout,var)[:,:], d02.time_mean(d02.scen2_wrfout,var)[:,:],
               d03.time_mean(d03.scen2_wrfout,var)[:,:], d04.time_mean(d04.scen2_wrfout,var)[:,:]],
              [d01.time_mean(d01.scen3_wrfout,var)[:,:], d02.time_mean(d02.scen3_wrfout,var)[:,:],
               d03.time_mean(d03.scen3_wrfout,var)[:,:], d04.time_mean(d04.scen3_wrfout,var)[:,:]],
              [d01.time_mean(d01.scen4_wrfout,var)[:,:], d02.time_mean(d02.scen4_wrfout,var)[:,:],
               d03.time_mean(d03.scen4_wrfout,var)[:,:], d04.time_mean(d04.scen4_wrfout,var)[:,:]]]
 
        return arr
    
    def gen_timemean3Dlists_RF(self, var):
        
        arr = [[d01.time_mean(d01.scen1_wrfout_RF,var)[:,:], d02.time_mean(d02.scen1_wrfout_RF,var)[:,:],
               d03.time_mean(d03.scen1_wrfout_RF,var)[:,:], d04.time_mean(d04.scen1_wrfout_RF,var)[:,:]],
              [d01.time_mean(d01.scen2_wrfout_RF,var)[:,:], d02.time_mean(d02.scen2_wrfout_RF,var)[:,:],
               d03.time_mean(d03.scen2_wrfout_RF,var)[:,:], d04.time_mean(d04.scen2_wrfout_RF,var)[:,:]],
              [d01.time_mean(d01.scen3_wrfout_RF,var)[:,:], d02.time_mean(d02.scen3_wrfout_RF,var)[:,:],
               d03.time_mean(d03.scen3_wrfout_RF,var)[:,:], d04.time_mean(d04.scen3_wrfout_RF,var)[:,:]],
              [d01.time_mean(d01.scen4_wrfout_RF,var)[:,:], d02.time_mean(d02.scen4_wrfout_RF,var)[:,:],
               d03.time_mean(d03.scen4_wrfout_RF,var)[:,:], d04.time_mean(d04.scen4_wrfout_RF,var)[:,:]]]
 
        return arr
    
    def RF_vars(self):
 
        self.emis_df_rf = create_df_merged_dom(timeshift = 0, periods = 1, T2 = self.gen_timemean3Dlists('T2'),
                                               SWDOWN = self.gen_timemean3Dlists('SWDOWN'), GLW = self.gen_timemean3Dlists('GLW'),
                                               HFX = self.gen_timemean3Dlists('HFX'), LH = self.gen_timemean3Dlists('LH'),
                                               GRDFLX = self.gen_timemean3Dlists('GRDFLX'), ALBEDO = self.gen_timemean3Dlists_RF('ALBEDO'),
                                               EMISS = self.gen_timemean3Dlists_RF('EMISS'))

        self.emis_df_rf['surf_balance'] = self.emis_df_rf['GLW'] - (self.emis_df_rf['EMISS'] * bolz * self.emis_df_rf['T2']**4 + 
                                                                (1 - self.emis_df_rf['EMISS']) * self.emis_df_rf['GLW']) + (1 - self.emis_df_rf['ALBEDO']) * self.emis_df_rf['SWDOWN'] - (self.emis_df_rf['GRDFLX'] + self.emis_df_rf['LH'] + self.emis_df_rf['HFX'])
        
        '''
        Linearly interpolate domain area
        '''
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp_field = tri.Triangulation(self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['lon'], self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['lat'])
        
        interpolator_t2 = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['T2'])
        self.t2_sc1 = interpolator_t2(self.Xi, self.Yi)
        interpolator_t2 = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['T2'])
        self.t2_sc2 = interpolator_t2(self.Xi, self.Yi)
        interpolator_t2 = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['T2'])
        self.t2_sc3 = interpolator_t2(self.Xi, self.Yi)
        interpolator_t2 = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['T2'])
        self.t2_sc4 = interpolator_t2(self.Xi, self.Yi)
        
        interpolator_swdown = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['SWDOWN'])
        self.swdown_sc1 = interpolator_swdown(self.Xi, self.Yi)
        interpolator_swdown = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['SWDOWN'])
        self.swdown_sc2 = interpolator_swdown(self.Xi, self.Yi)
        interpolator_swdown = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['SWDOWN'])
        self.swdown_sc3 = interpolator_swdown(self.Xi, self.Yi)
        interpolator_swdown = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['SWDOWN'])
        self.swdown_sc4 = interpolator_swdown(self.Xi, self.Yi)
        
        interpolator_glw = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['GLW'])
        self.glw_sc1 = interpolator_glw(self.Xi, self.Yi)
        interpolator_glw = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['GLW'])
        self.glw_sc2 = interpolator_glw(self.Xi, self.Yi)
        interpolator_glw = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['GLW'])
        self.glw_sc3 = interpolator_glw(self.Xi, self.Yi)
        interpolator_glw = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['GLW'])
        self.glw_sc4 = interpolator_glw(self.Xi, self.Yi)
        
        interpolator_hfx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['HFX'])
        self.hfx_sc1 = interpolator_hfx(self.Xi, self.Yi)
        interpolator_hfx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['HFX'])
        self.hfx_sc2 = interpolator_hfx(self.Xi, self.Yi)
        interpolator_hfx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['HFX'])
        self.hfx_sc3 = interpolator_hfx(self.Xi, self.Yi)
        interpolator_hfx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['HFX'])
        self.hfx_sc4 = interpolator_hfx(self.Xi, self.Yi)
        
        interpolator_lh = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['LH'])
        self.lh_sc1 = interpolator_lh(self.Xi, self.Yi)
        interpolator_lh = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['LH'])
        self.lh_sc2 = interpolator_lh(self.Xi, self.Yi)
        interpolator_lh = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['LH'])
        self.lh_sc3 = interpolator_lh(self.Xi, self.Yi)
        interpolator_lh = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['LH'])
        self.lh_sc4 = interpolator_lh(self.Xi, self.Yi)
        
        interpolator_grdflx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['GRDFLX'])
        self.grdflx_sc1 = interpolator_grdflx(self.Xi, self.Yi)
        interpolator_grdflx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['GRDFLX'])
        self.grdflx_sc2 = interpolator_grdflx(self.Xi, self.Yi)
        interpolator_grdflx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['GRDFLX'])
        self.grdflx_sc3 = interpolator_grdflx(self.Xi, self.Yi)
        interpolator_grdflx = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['GRDFLX'])
        self.grdflx_sc4 = interpolator_grdflx(self.Xi, self.Yi)
        
        interpolator_alb = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['ALBEDO'])
        self.alb_sc1 = interpolator_alb(self.Xi, self.Yi)
        interpolator_alb = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['ALBEDO'])
        self.alb_sc2 = interpolator_alb(self.Xi, self.Yi)
        interpolator_alb = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['ALBEDO'])
        self.alb_sc3 = interpolator_alb(self.Xi, self.Yi)
        interpolator_alb = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['ALBEDO'])
        self.alb_sc4 = interpolator_alb(self.Xi, self.Yi)
        
        interpolator_emiss = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 1)]['EMISS'])
        self.emiss_sc1 = interpolator_emiss(self.Xi, self.Yi)
        interpolator_emiss = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 2)]['EMISS'])
        self.emiss_sc2 = interpolator_emiss(self.Xi, self.Yi)
        interpolator_emiss = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 3)]['EMISS'])
        self.emiss_sc3 = interpolator_emiss(self.Xi, self.Yi)
        interpolator_emiss = tri.LinearTriInterpolator(interp_field, self.emis_df_rf[(self.emis_df_rf['scenario'] == 4)]['EMISS'])
        self.emiss_sc4 = interpolator_emiss(self.Xi, self.Yi)
        
        self.ulr_sc1 = self.emiss_sc1 * bolz * self.t2_sc1**4 + (1 - self.emiss_sc1) * self.glw_sc1
        self.ulr_sc2 = self.emiss_sc2 * bolz * self.t2_sc2**4 + (1 - self.emiss_sc2) * self.glw_sc2
        self.ulr_sc3 = self.emiss_sc3 * bolz * self.t2_sc3**4 + (1 - self.emiss_sc3) * self.glw_sc3
        self.ulr_sc4 = self.emiss_sc4 * bolz * self.t2_sc4**4 + (1 - self.emiss_sc4) * self.glw_sc4
        
        self.surf_bal_sc1 = self.glw_sc1 - self.ulr_sc1 + (1 - self.alb_sc1) * self.swdown_sc1 - (self.grdflx_sc1 + self.lh_sc1 + self.hfx_sc1)
        self.surf_bal_sc2 = self.glw_sc2 - self.ulr_sc2 + (1 - self.alb_sc2) * self.swdown_sc2 - (self.grdflx_sc2 + self.lh_sc2 + self.hfx_sc2)
        self.surf_bal_sc3 = self.glw_sc3 - self.ulr_sc3 + (1 - self.alb_sc3) * self.swdown_sc3 - (self.grdflx_sc3 + self.lh_sc3 + self.hfx_sc3)
        self.surf_bal_sc4 = self.glw_sc4 - self.ulr_sc4 + (1 - self.alb_sc4) * self.swdown_sc4 - (self.grdflx_sc4 + self.lh_sc4 + self.hfx_sc4)
    
    def meteo_vars(self):

        self.meteo = create_df_merged_dom(timeshift = 0, periods = 1, Q2 = self.gen_timemean3Dlists('Q2'),
                                          T2 = self.gen_timemean3Dlists('T2'), U10 = self.gen_timemean3Dlists('U10'),
                                          V10 = self.gen_timemean3Dlists('V10'), RAINC = self.gen_timemean3Dlists('RAINC'),
                                          RAINNC = self.gen_timemean3Dlists('RAINNC'), burntfuel = burntfuel)
        
    def MAD(self, df):
        
        sums = 0
        df = np.asarray(df)
        mean_arr = np.nanmean(df)
        
        for i in range(len(df)):
            
            dev = abs(df[i] - mean_arr)
            if np.isnan(dev) == True:
                dev = 0
            sums += dev
           
        return sums / len(df)

    def NO2_PM25_vs_annual(self):
        
        no2_who_ann_exceed_1 = (self.no2_sc1[self.bool_d03d04] > lim_ann_no2_who).sum() / self.no2_sc1[self.bool_d03d04].count() * 100
        no2_who_ann_exceed_2 = (self.no2_sc2[self.bool_d03d04] > lim_ann_no2_who).sum() / self.no2_sc2[self.bool_d03d04].count() * 100
        no2_who_ann_exceed_3 = (self.no2_sc3[self.bool_d03d04] > lim_ann_no2_who).sum() / self.no2_sc3[self.bool_d03d04].count() * 100
        no2_who_ann_exceed_4 = (self.no2_sc4[self.bool_d03d04] > lim_ann_no2_who).sum() / self.no2_sc4[self.bool_d03d04].count() * 100
        
        dict_no2_who = {'1': no2_who_ann_exceed_1, '2': no2_who_ann_exceed_2, '3': no2_who_ann_exceed_3,
                      '4': no2_who_ann_exceed_4}
        
        pm25_who_ann_exceed_1 = (self.pm25_sc1 > lim_ann_pm25_who).sum() / self.pm25_sc1.count() * 100
        pm25_who_ann_exceed_2 = (self.pm25_sc2 > lim_ann_pm25_who).sum() / self.pm25_sc2.count() * 100
        pm25_who_ann_exceed_3 = (self.pm25_sc3 > lim_ann_pm25_who).sum() / self.pm25_sc3.count() * 100
        pm25_who_ann_exceed_4 = (self.pm25_sc4 > lim_ann_pm25_who).sum() / self.pm25_sc4.count() * 100
        
        dict_pm_who = {'1': pm25_who_ann_exceed_1, '2': pm25_who_ann_exceed_2, '3': pm25_who_ann_exceed_3,
                      '4': pm25_who_ann_exceed_4}
        
        self.dict_pm_median = {'1': np.ma.median(self.pm25_sc1), '2': np.ma.median(self.pm25_sc2),
                '3': np.ma.median(self.pm25_sc3), '4': np.ma.median(self.pm25_sc4)}
        self.dict_no2_median = {'1': np.ma.median(self.no2_sc1[self.bool_d03d04]),
                              '2': np.ma.median(self.no2_sc2[self.bool_d03d04]),
                              '3': np.ma.median(self.no2_sc3[self.bool_d03d04]),
                              '4': np.ma.median(self.no2_sc4[self.bool_d03d04])}
        
        def gen_distplot(self, data, scenario, color, var):
            
            ax_obj = axs[0 if scenario == 1 or scenario == 2 else 1, 0 if scenario == 1 or scenario == 3 else 1]
            int_to_roman = {'1': 'Sc$_{I}$', '2': 'Sc$_{II}$', '3': 'Sc$_{III}$', '4': 'Sc$_{IV}$'}
            
            if var == 'NO2':
                ax = sns.distplot(data,
                        label = 'median {:.2f} $\\mu$g m$^{{-3}}$'.format(self.dict_no2_median[str(scenario)]), 
                        color = color, hist = False, rug = True, ax = ax_obj)
                l = ax.lines[0]
                x = l.get_xydata()[:,0]
                y = l.get_xydata()[:,1]
                ax.set(xlim=(0))
                ax.fill_between(x, y, color=color, where=(x > lim_ann_no2_who), hatch = 'o', alpha=0.3,
                                label = 'above WHO guideline ({:.2f}%)'.format(dict_no2_who[str(scenario)]))
                ax.set_title(str(int_to_roman[str(scenario)]), size = 30)
            
            elif var == 'PM25':
                ax = sns.distplot(data,
                        label = 'median {:.2f} $\\mu$g m$^{{-3}}$'.format(self.dict_pm_median[str(scenario)]), 
                        color = color, hist = False, rug = True, ax = ax_obj)
                l = ax.lines[0]
                x = l.get_xydata()[:,0]
                y = l.get_xydata()[:,1]
                ax.fill_between(x, y, color=color, where=(x > lim_ann_pm25_who), hatch = 'o', alpha=0.3,
                                label = 'above WHO guideline ({:.2f}%)'.format(dict_pm_who[str(scenario)]))
                ax.set_title(str(int_to_roman[str(scenario)]), size = 30)
            if var == 'NO2':
                var = 'NO$_2$'
            if var == 'PM25':
                var = 'PM$_{2.5}$'
                
            ax_obj.set_xlabel(var + ' ($\\mu$g m$^{-3}$)')
            ax_obj.set_ylabel('probability')
            legend = ax_obj.legend(frameon = True)
            frame = legend.get_frame()
            frame.set_color('white')
            
        fig, axs = plt.subplots(2, 2)
        
        gen_distplot(self, self.no2_sc1[self.bool_d03d04], 1, color_sc1, 'NO2')
        gen_distplot(self, self.no2_sc2[self.bool_d03d04], 2, color_sc2, 'NO2')
        gen_distplot(self, self.no2_sc3[self.bool_d03d04], 3, color_sc3, 'NO2')
        gen_distplot(self, self.no2_sc4[self.bool_d03d04], 4, color_sc4, 'NO2')
        
        for ax in fig.get_axes():
            ax.label_outer()
            
        plt.show()
        
        fig, axs = plt.subplots(2, 2)
        
        gen_distplot(self, self.pm25_sc1.flatten().tolist(), 1, color_sc1, 'PM25')
        gen_distplot(self, self.pm25_sc2.flatten().tolist(), 2, color_sc2, 'PM25')
        gen_distplot(self, self.pm25_sc3.flatten().tolist(), 3, color_sc3, 'PM25')
        gen_distplot(self, self.pm25_sc4.flatten().tolist(), 4, color_sc4, 'PM25')
        
        for ax in fig.get_axes():
            ax.label_outer()
            
        plt.show()

    def RP_mean_analysis(self):
        
        self.mean_vals = pd.DataFrame({'NO2': [np.mean(self.no2_sc1[self.bool_d03d04]), np.mean(self.no2_sc2[self.bool_d03d04]),
                                               np.mean(self.no2_sc3[self.bool_d03d04]), np.mean(self.no2_sc4[self.bool_d03d04])],
                                       'O3': [np.mean(self.o3_sc1), np.mean(self.o3_sc2), np.mean(self.o3_sc3), np.mean(self.o3_sc4)],
                                       'PM25': [np.mean(self.pm25_sc1), np.mean(self.pm25_sc2), np.mean(self.pm25_sc3), np.mean(self.pm25_sc4)]})

        self.dict_no2_mean = {'1': np.mean(self.no2_sc1[self.bool_d03d04]),
                              '2': np.mean(self.no2_sc2[self.bool_d03d04]),
                              '3': np.mean(self.no2_sc3[self.bool_d03d04]),
                              '4': np.mean(self.no2_sc4[self.bool_d03d04])}
        self.dict_o3_mean = {'1': np.mean(self.o3_sc1),
                             '2': np.mean(self.o3_sc2),
                             '3': np.mean(self.o3_sc3),
                             '4': np.mean(self.o3_sc4)}
        self.dict_pm_mean = {'1': np.mean(self.pm25_sc1),
                             '2': np.mean(self.pm25_sc2),
                             '3': np.mean(self.pm25_sc3),
                             '4': np.mean(self.pm25_sc4)}
        
        def RP(self, compound, scenario):
        
            compound_to_dict = {'NO2': self.dict_no2_mean, 'O3': self.dict_o3_mean, 'PM25': self.dict_pm_mean}
            
            return (compound_to_dict[compound]['1'] - compound_to_dict[compound][str(scenario)]) / (compound_to_dict[compound]['1'] - 
                                                                                       compound_to_dict[compound]['4']) * 100
            
        self.RP_mean = pd.DataFrame({'NO$_2$': [RP(self, 'NO2', 2), RP(self, 'NO2', 3)],
                                   'O$_3$': [RP(self, 'O3', 2), RP(self, 'O3', 3)],
                                   'PM$_{25}$': [RP(self, 'PM25', 2), RP(self, 'PM25', 3)],
                                   'scenario': ['Sc$_{II}$', 'Sc$_{III}$']})
        
        self.RP_mean = pd.melt(self.RP_mean, id_vars = 'scenario', var_name = 'compound', value_name = 'RP (%)')
        
        fig, ax = plt.subplots()
        bars = sns.barplot(x = 'compound', y = 'RP (%)', hue = 'scenario', data = self.RP_mean, 
                           palette = [color_sc2, color_sc3], ax = ax)
        
        for p in bars.patches:
                bars.annotate('{:.0f}%'.format(p.get_height()), (p.get_x() + 0.5 * p.get_width(), p.get_height()),
                            ha='center', va='bottom', color= 'black')
                
        plt.gca().invert_yaxis()
        ax.set_ylim([110,-10])
        legend = bars.legend(frameon = True, bbox_to_anchor=(0.13, 0.58))
        frame = legend.get_frame()
        frame.set_color('white')
        
        compress = 0.1
        x_start = np.array([-0.5, 0.5, 1.5]) + compress
        x_end = np.array(x_start+[1, 1, 1]) - compress * 2
        targets = (100, 100, 100)
        plt.hlines(targets, x_start, x_end, linestyles = 'dashed', colors = ['k', 'k', 'k'])
        targets = (0, 0, 0)
        plt.hlines(targets, x_start, x_end, linestyles = 'dashed', colors = ['k', 'k', 'k'])
        plt.vlines(x_start, [0, 0, 0], [100, 100, 100], linestyles = 'dashed', colors = ['k', 'k', 'k'])
        plt.vlines(x_end, [0, 0, 0], [100, 100, 100], linestyles = 'dashed', colors = ['k', 'k', 'k'])
        
        bot_lab = [self.dict_no2_mean['4'], self.dict_o3_mean['4'], self.dict_pm_mean['4']]
        top_lab = [self.dict_no2_mean['1'], self.dict_o3_mean['1'], self.dict_pm_mean['1']]
        xmid = (x_start + x_end) / 2
        
        for idx, x in enumerate(xmid):
            plt.text(x, 100 + 5, '{:.2f}'.format(bot_lab[idx]), ha = 'center')
            plt.text(x, 0 - 2, '{:.2f}'.format(top_lab[idx]), ha = 'center')
        
        plt.hlines(100, x_start[0] - 0.3, x_start[0] - 0.1, linestyles = 'dashed', colors = 'k')
        plt.text(x_start[0] - 0.2, 100 + 5, 'Sc$_{IV}$ ($\\mu$g m$^{{-3}}$)', ha = 'center')   
        plt.hlines(0, x_start[0] - 0.3, x_start[0] - 0.1, linestyles = 'dashed', colors = 'k')
        plt.text(x_start[0] - 0.2, 0 - 2, 'Sc$_{I}$ ($\\mu$g m$^{{-3}}$)', ha = 'center')

    def mean_PBLH(self):
        
        means = nc.Dataset(path + 'meanmeteo\\mean_sc1_wrfout_d01.nc')
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(d01.lons, d01.lats)
        
        im = map.contourf(lon, lat, np.mean(means['PBLH'][:], axis = 0), np.arange(60, 660, 20),
                        extend='both', cmap='terrain_r', ax = ax)
        im.cmap.set_under('whitesmoke')
        cbar = plt.colorbar(im)
        cbar.set_label('{} (m)'.format('mean PBL height'))
        
    def locs_shift(self, var): # var could be 'NO2' or 'PM25'
        
        domain = d03 if var == 'NO2' else d01
        domains_data = [3,4] if var == 'NO2' else [1,2,3,4]
        lim_val = lim_ann_no2_who if var == 'NO2' else lim_ann_pm25_who
        
        interp = tri.Triangulation(self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'].isin(domains_data))]['lon'], self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'].isin(domains_data))]['lat'])
        interpolator = tri.LinearTriInterpolator(interp, self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'].isin(domains_data))][var])
        C_sc1 = interpolator(self.Xi, self.Yi)
        
        interp = tri.Triangulation(self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'].isin(domains_data))]['lon'], self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'].isin(domains_data))]['lat'])
        interpolator = tri.LinearTriInterpolator(interp, self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'].isin(domains_data))][var])
        C_sc4 = interpolator(self.Xi, self.Yi)
        
        no2diff = C_sc4 - C_sc1
        
        bools = np.where((C_sc1 > lim_val) & (C_sc4 <= lim_val), 1, 0)
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = domain.min_lat, urcrnrlat = domain.max_lat,\
                    llcrnrlon = domain.min_lon, urcrnrlon = domain.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        im = map.contourf(lon, lat, C_sc1, np.arange(0, 21, 1), extend='max', 
                          cmap='seismic', ax = ax, norm = MidpointNormalize(midpoint = lim_val))
        cbar = plt.colorbar(im)
        cbar.set_label('{} ($\\mu$g m$^{{-3}}$)'.format('PM$_{2.5}$' if var == 'PM25' else 'NO$_2$'))
        
        Xi = self.Xi[np.where((C_sc1 > lim_val) & (C_sc4 <= lim_val))]
        Yi = self.Yi[np.where((C_sc1 > lim_val) & (C_sc4 <= lim_val))]
        map.scatter(Xi, Yi, color = 'g', s = 3)

    def pre_proc_local_AQ(self):
        
        self.df = self.emis_df[self.emis_df['burntfuel'] >= fuel_lim][['lon','lat']].drop_duplicates().reset_index().drop('index', axis = 1)
        self.df['label'] = ['MXP', 'ZRH', 'VIE', 'MUC', 'CDG', 'LHR',
                                'LHR', 'DUB', 'MAN', 'CDG', 'FRA', 'AMS']
        fbbrus = self.df.merge(self.emis_df, how="left", on = ['lat', 'lon'])
        
        self.emis_df_ac = self.df.merge(self.emis_df, how="left", on = ['lat', 'lon'])
        self.emis_df_ac['burntfuel'][self.emis_df_ac['label'] == 'BRU'] = fbbrus[fbbrus['label'] == 'BRU']['burntfuel']
        self.emis_df_ac['burntfuel'][self.emis_df_ac['label'] == 'AMS'] = fbbrus[fbbrus['label'] == 'AMS']['burntfuel']
        
        self.emis_df_ac['domain'] = pd.to_numeric(self.emis_df_ac['domain'], downcast = 'integer')
        self.emis_df_ac[self.emis_df_ac['label'] == 'AMS']['burntfuel'] = self.emis_df_ac[self.emis_df_ac['label'] == 'AMS']['burntfuel'] + 122841.555 + 139274.25
        self.emis_df_ac[(self.emis_df_ac['lon'] == -0.25) & (self.emis_df_ac['lat'] == 51.5)]['burntfuel'] = self.emis_df_ac[(self.emis_df_ac['lon'] == -0.25) & 
                                                                                                                             (self.emis_df_ac['lat'] == 51)]['burntfuel'] + self.emis_df_ac[(self.emis_df_ac['lon'] == -0.25) & 
                                                                                                                                                                                            (self.emis_df_ac['lat'] == 51.5)]['burntfuel']
        indexNames = self.emis_df_ac[(self.emis_df_ac['lon'] == -0.25) & (self.emis_df_ac['lat'] == 51)].index
        self.emis_df_ac.drop(indexNames, inplace=True)
        #gt.emis_df_ac.replace({np.nan: 'BRU'}, inplace = True)
        group_emis = self.emis_df_ac.groupby(['label', 'scenario'])[['NO', 'NO2', 'O3', 'PM25', 'VOC', 'burntfuel', 'domain']].mean().reset_index()
        self.emis_df_ac = pd.concat([self.emis_df_ac[self.emis_df_ac['label'].isna()][group_emis.columns], group_emis])
        
        self.sc1 = self.emis_df_ac[self.emis_df_ac['scenario'] == 1]
        self.sc2 = self.emis_df_ac[self.emis_df_ac['scenario'] == 2]
        self.sc3 = self.emis_df_ac[self.emis_df_ac['scenario'] == 3]
        self.sc4 = self.emis_df_ac[self.emis_df_ac['scenario'] == 4]
        
    def airports_highfuel(self):
        
        try:
            self.df
        except:
            self.pre_proc_local_AQ()
            
        fig, ax = plt.subplots(figsize=(12,8))
        plt.rcParams.update({'font.size': 20})
        bmap = Basemap(projection='cyl',llcrnrlat = d01.min_lat - 1, urcrnrlat = d01.max_lat + 1,\
                    llcrnrlon = d01.min_lon - 1, urcrnrlon = d01.max_lon + 2, resolution='h', ax = ax)
        
        #bmap.bluemarble()
        bmap.drawcoastlines()
        bmap.drawcountries()
        bmap.drawparallels(np.arange(-90., 120.,5.),labels=[1,0,0,0],fontsize=14,color='k')
        bmap.drawmeridians(np.arange(-180.,180.,5.),labels=[0,0,0,1],fontsize=14,color='k')
        bmap.drawmapboundary(fill_color='steelblue',zorder=0) 
        bmap.fillcontinents(color='white',zorder=1)
        lons, lats = bmap(self.df['lon'], self.df['lat'])
        scatter = bmap.scatter(lons, lats, color = 'k', s = self.df.merge(self.emis_df, how="left", 
                                                                     on = ['lat', 'lon'])[self.df.merge(self.emis_df, how="left", on = ['lat', 'lon'])['scenario'] == 2]['burntfuel'].values / 1e3, zorder = 2)
        
        lons = [d01.min_lon, d01.min_lon, d01.max_lon, d01.max_lon]
        lats = [d01.min_lat, d01.max_lat, d01.max_lat, d01.min_lat]
        
        x, y = bmap(lons, lats)
        
        for row in self.df.iterrows():
            if row[0] != 4 and row[0] != 5: # skip double labels
                x, y = bmap(row[1]['lon'] + 0.3, row[1]['lat'] + 0.5)
                plt.text(x, y, row[1]['label'], color = 'white',
                              fontsize=15, ha = 'left', bbox=dict(facecolor='black', 
                                                                  edgecolor='none',
                                                                  boxstyle='round',
                                                                    alpha=0.7), zorder = 2)
        handles, labels = scatter.legend_elements(prop="sizes", alpha=1)
        legend = ax.legend([handles[a] for a in [0,4,8]], [labels[a] for a in [0,4,8]], loc="upper right", title="fuel consumption\n(metric tonnes)", frameon = True)
        frame = legend.get_frame()
        frame.set_facecolor('white')
        frame.set_edgecolor('black')    

        
    def local_AQ_airports(self, airport): # airport could be 'FRA', 'LHR', 'DUB', 'CDG', 'ZRH', 'MAN', 'AMS', 'VIE', 'MXP', 'MUC'
        
        try:
            self.emis_df_ac
        except:
            self.pre_proc_local_AQ()
            
        plt.style.use('seaborn-white')

        xs = self.emis_df_ac[(self.emis_df_ac['label'] == airport)]['NO2'].reset_index().drop(['index'], axis = 1).values.flatten()
        ys = self.emis_df_ac[(self.emis_df_ac['label'] == airport)]['O3'].reset_index().drop(['index'], axis = 1).values.flatten()
        zs = self.emis_df_ac[(self.emis_df_ac['label'] == airport)]['PM25'].reset_index().drop(['index'], axis = 1).values.flatten()
        
        # get relative concentration changes w.r.t. baseline scenario
        xs = (xs - xs[0]) / xs[0] * 100
        ys = (ys - ys[0]) / ys[0] * 100
        zs = (zs - zs[0]) / zs[0] * 100
        
        v = []
        lw = 1
        
        x_offset_dict = {'FRA': 4, 'LHR': 4, 'DUB': 4, 'CDG': 2, 'ZRH': 2, 'MAN': 1, 'AMS': 0.5,
                         'VIE': 0.15, 'MXP': 1, 'MUC': 2}
        y_offset_dict = {'FRA': 1, 'LHR': 1, 'DUB': 0.2, 'CDG': 0.5, 'ZRH': 0.1, 'MAN': 0.13, 'AMS': 0.05,
                         'VIE': 0.015, 'MXP': 0.1, 'MUC': 0.25}
        y_shift_x_dict = {'FRA': 0.3, 'LHR': 0.3, 'DUB': 0.08, 'CDG': 0.12, 'ZRH': 0.03, 'MAN': 0.07, 'AMS': 0.02,
                         'VIE': 0.005, 'MXP': 0.08, 'MUC': 0.03} 
        x_shift_y_dict = {'FRA': 0.9, 'LHR': 0.9, 'DUB': 0.9, 'CDG': 0.4, 'ZRH': 0.4, 'MAN': 0.4, 'AMS': 0.2,
                         'VIE': 0.03, 'MXP': 0.4, 'MUC': 0.2} 
        airportlabel = {'FRA': 'Frankfurt Airport (FRA)', 'LHR': 'London Heathrow Airport (LHR)',
                        'DUB': 'Dublin Airport (DUB)', 'CDG': 'Charles de Gaulle Airport (CDG)',
                        'ZRH': 'Zurich Airport (ZRH)', 'MAN': 'Manchester Airport (MAN)',
                        'AMS': 'Amsterdam Schiphol Airport (AMS)', 'VIE': 'Vienna Airport (VIE)',
                        'MXP': 'Milan Malpensa Airport (MXP)', 'MUC': 'Munich Airport (MUC)'} 
        text_sc_dict = {'FRA': [0, 0, 0, 0, -0.3, -0.3, -0.3, -0.3, 0, 0, 0, 0], 'LHR': [2, 0, 0, 0, -0.5, -0.3, -0.3, -0.3, 0, 0, 0, 0],
                        'DUB': [-0.5, -0.5, 0, 0, -0.03, -0.03, -0.05, -0.05, -0.05, -0.1, -0.1, -0.1],
                        'CDG': [0, 0, 0, 0, -0.01, -0.01, -0.01, -0.01, -0.2, -0.25, -0.1, -0.1],
                        'ZRH': [-0.2, -0.2, -0.2, -0.2, 0, 0, 0, 0, 0, 0, 0, 0],
                        'MAN': [-0.2, -0.2, -0.2, -0.2, 0, 0, 0, 0, 0, 0, 0, 0],
                        'AMS': [-0.1, -0.1, -0.1, -0.1, 0, 0, 0, 0, 0, 0, 0, 0],
                        'VIE': [-0.02, -0.01, -0.02, -0.02, 0, -0.005, 0, 0, 0, 0.01, 0, 0],
                        'MXP': [-0.2, -0.2, -0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        'MUC': [-0.15, -0.15, 0, 0, -0.05, -0.03, -0.1, -0.05, 0, -0.05, -0.1, -0.1]}
        text_z_dict = {'FRA': [-3, -3, -3, 0.22, 0.22, 0.2], 'LHR': [-3, -3, -3, 0.25, 0.25, 0.25],
                       'DUB': [-3, -3, -3, 0.3, 0.3, 0.25], 'CDG': [-1.5, -1.5, -1.5, 0.43, 0.38, 0.43],
                       'ZRH': [-1.5, -1.5, -1.5, 0.3, 0.3, 0.3], 'MAN': [-0.3, -0.3, -0.3, 0, 0, 0],
                       'AMS': [-0.3, -0.3, -0.3, -0.03, -0.03, -0.03], 'VIE': [-0.1, -0.1, -0.1, 0.02, 0.02, 0.02],
                       'MXP': [-1.5, -1.5, -1.5, 0.15, 0.05, 0.15], 'MUC': [-0.7, -0.7, -0.7, 0.45, 0.48, 0.53]}
        text_y_dict = {'FRA': [-2.7, -2.7, -2.7, -0.25, -0.25, -0.35], 'LHR': [-2.5, -2.5, -2.5, -0.2, -0.2, -0.2],
                       'DUB': [-2.5, -2.5, -2.5, -0.05, -0.05, -0.05], 'CDG': [-1.3, -1.3, -1.3, -0.35, -0.12, -0.12],
                       'ZRH': [-1.3, -1.3, -1.3, -0.06, -0.02, -0.02], 'MAN': [0, 0, 0, 0, 0, 0],
                       'AMS': [-0.3, -0.3, -0.3, -0.03, -0.03, -0.03],
                       'VIE': [-0.1, -0.1, -0.08, -0.012, -0.01, -0.012],
                       'MXP': [-1.3, -1.3, -1.3, -0.04, -0.04, -0.04],
                       'MUC': [-0.6, -0.6, -0.6, -0.15, -0.15, -0.15]}
        text_x_dict = {'FRA': [-0.7, -0.7, -0.7, 0, 0, 0], 'LHR': [-0.9, -0.7, -0.7, 0, 0, 0],
                       'DUB': [-0.9, -0.7, -0.7, 0, 0, 0], 'CDG': [-0.5, -0.5, -0.5, 0, 0, 0],
                       'ZRH': [-0.5, -0.5, -0.5, -0.03, -0.03, -0.03], 'MAN': [-0.15, -0.25, -0.35, -0.35, -0.35, -0.3],
                       'AMS': [-0.1, -0.1, -0.1, -0.01, -0.01, 0],
                       'VIE': [-0.03, -0.03, -0.03, -0.09, -0.09, -0.087],
                       'MXP': [-0.5, -0.5, -0.5, -0.23, -0.23, -0.23],
                       'MUC': [-0.2, -0.2, -0.2, -1.05, -1.05, -1.05]}
        
        x_offset = x_offset_dict[airport]
        y_offset = y_offset_dict[airport]
        z_offset = 0.3 if airport == 'MAN' else 0
        y_shift_x = y_shift_x_dict[airport]
        x_shift_y = x_shift_y_dict[airport]
        h = min(zs) if airport == 'MAN' else 0
        
        # Code to convert data in 3D polygons
        for k in range(0, len(xs) - 1):
            x = [xs[k], xs[k+1], xs[k+1], xs[k]]
            y = [ys[k], ys[k+1], ys[k+1], ys[k]]
            z = [zs[k], zs[k+1],       h,     h]
            v.append(list(zip(x, y, z))) 
            
        poly3dCollection = Poly3DCollection(v)
        poly3dCollection.set_alpha(0.2)   # Order reversed 
        poly3dCollection.set_facecolor('black')   # Order reversed 
        
        # figure
        fig = plt.figure()
        ax = Axes3D(fig)
        ax.add_collection3d(poly3dCollection)
        ax.xaxis.labelpad=20
        ax.yaxis.labelpad=20
        ax.zaxis.labelpad=20
        
        ax.scatter(xs.flatten(), ys.flatten(), zs.flatten(), s = 150, color = [color_sc1, color_sc2, color_sc3, color_sc4])
        
        if airport == 'MAN':
            
            # projection onto horizontal surface
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                    [0, max(zs.flatten()) + z_offset], color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                    [0, max(zs.flatten()) + z_offset], color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                    [0, max(zs.flatten()) + z_offset], color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                    [0, max(zs.flatten()) + z_offset], color = color_sc4, lw = lw)
            
            # ground projection line
            ax.plot([xs.flatten()[0], xs.flatten()[1]], [ys.flatten()[0], ys.flatten()[1]], 
                    [0, 0], linestyle = 'dashed', color = 'black', lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[2]], [ys.flatten()[1], ys.flatten()[2]], 
                    [0, 0], linestyle = 'dashed', color = 'black', lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[3]], [ys.flatten()[2], ys.flatten()[3]], 
                    [0, 0], linestyle = 'dashed', color = 'black', lw = lw)
            
            # show O3 and NO2 concentrations for each scenario with dashed lines
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[0]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[1]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[2]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[3]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc4, lw = lw)
            
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                    [max(zs.flatten()) + z_offset, max(zs.flatten()) + z_offset], linestyle = 'dashed', color = color_sc4, lw = lw)
            
            # projection onto side surface
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[0], zs.flatten()[0]], color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[1], zs.flatten()[1]], color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[2], zs.flatten()[2]], color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[3], zs.flatten()[3]], color = color_sc4, lw = lw)
            
            # show PM25 concentrations for each scenario with dashed lines
            ax.plot([max(xs.flatten()) + 0.4 + x_offset, xs.flatten()[0]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[0], zs.flatten()[0]], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([max(xs.flatten()) + 0.4 + x_offset, xs.flatten()[1]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[1], zs.flatten()[1]], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([max(xs.flatten()) + 0.4 + x_offset, xs.flatten()[2]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[2], zs.flatten()[2]], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([max(xs.flatten()) + 0.4 + x_offset, xs.flatten()[3]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[3], zs.flatten()[3]], linestyle = 'dashed', color = color_sc4, lw = lw)
        
            # text positioning
            ax.text(xs.flatten()[0] + text_sc_dict[airport][0], ys.flatten()[0] + text_sc_dict[airport][4], zs.flatten()[0] + text_sc_dict[airport][8], "Scenario I", color=color_sc1, ha = 'right')
            ax.text(xs.flatten()[1] + text_sc_dict[airport][1], ys.flatten()[1] + text_sc_dict[airport][5], zs.flatten()[1] + text_sc_dict[airport][9], "Scenario II", color=color_sc2, ha = 'right')
            ax.text(xs.flatten()[2] + text_sc_dict[airport][2], ys.flatten()[2] + text_sc_dict[airport][6], zs.flatten()[2] + text_sc_dict[airport][10], "Scenario III", color=color_sc3, ha = 'right')
            ax.text(xs.flatten()[3] + text_sc_dict[airport][3], ys.flatten()[3] + text_sc_dict[airport][7], zs.flatten()[3] + text_sc_dict[airport][11], "Scenario IV", color=color_sc4, ha = 'right')
                
            ax.text(x_offset + text_z_dict[airport][0], max(ys.flatten()) + y_offset / 2, zs.flatten()[1] + text_z_dict[airport][3], '{}{:.2f}%'.format(aux_func.sign(zs[1]),zs[1]), size = 15,
                    color=color_sc2, zdir = 'x')
            ax.text(x_offset + text_z_dict[airport][1], max(ys.flatten()) + y_offset / 2, zs.flatten()[2] + text_z_dict[airport][4], '{}{:.2f}%'.format(aux_func.sign(zs[2]),zs[2]), size = 15,
                    color=color_sc3, zdir = 'x')
            ax.text(x_offset + text_z_dict[airport][2], max(ys.flatten()) + y_offset / 2, zs.flatten()[3] + text_z_dict[airport][5], '{}{:.2f}%'.format(aux_func.sign(zs[3]),zs[3]), size = 15,
                    color=color_sc4, zdir = 'x')
            
            ax.text(x_offset + text_y_dict[airport][0], ys.flatten()[1], max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(ys[1]),ys[1]), size = 15,
                    color=color_sc2, zdir = 'x')
            ax.text(x_offset + text_y_dict[airport][1], ys.flatten()[2], max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(ys[2]),ys[2]), size = 15,
                    color=color_sc3, zdir = 'x')
            ax.text(x_offset + text_y_dict[airport][2], ys.flatten()[3], max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(ys[3]),ys[3]), size = 15,
                    color=color_sc4, zdir = 'x')
            
            ax.text(xs.flatten()[1] + text_x_dict[airport][0], text_x_dict[airport][3] - y_offset, max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(xs[1]),xs[1]), size = 15,
                    color=color_sc2, zdir = 'y')
            ax.text(xs.flatten()[2] + text_x_dict[airport][1], text_x_dict[airport][4] - y_offset, max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(xs[2]),xs[2]), size = 15,
                    color=color_sc3, zdir = 'y')
            ax.text(xs.flatten()[3] + text_x_dict[airport][2], text_x_dict[airport][5] - y_offset, max(zs.flatten()) + z_offset, '{}{:.2f}%'.format(aux_func.sign(xs[3]),xs[3]), size = 15,
                    color=color_sc4, zdir = 'y')
        
        else:   
            # projection onto horizontal surface
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                    [zs.flatten()[0], 0], color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                    [zs.flatten()[1], 0], color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                    [zs.flatten()[2], 0], color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                    [zs.flatten()[3], 0], color = color_sc4, lw = lw)
            
            # show O3 and NO2 concentrations for each scenario with dashed lines
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[0]], 
                    [0, 0], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[1]], 
                    [0, 0], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[2]], 
                    [0, 0], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [min(ys.flatten()) - y_shift_x - y_offset, ys.flatten()[3]], 
                    [0, 0], linestyle = 'dashed', color = color_sc4, lw = lw)
            
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[0]], [ys.flatten()[0], ys.flatten()[0]], 
                    [0, 0], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[1]], [ys.flatten()[1], ys.flatten()[1]], 
                    [0, 0], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[2]], [ys.flatten()[2], ys.flatten()[2]], 
                    [0, 0], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[3]], [ys.flatten()[3], ys.flatten()[3]], 
                    [0, 0], linestyle = 'dashed', color = color_sc4, lw = lw)
            
            # projection onto side surface
            ax.plot([xs.flatten()[0], xs.flatten()[0]], [ys.flatten()[0], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[0], zs.flatten()[0]], color = color_sc1, lw = lw)
            ax.plot([xs.flatten()[1], xs.flatten()[1]], [ys.flatten()[1], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[1], zs.flatten()[1]], color = color_sc2, lw = lw)
            ax.plot([xs.flatten()[2], xs.flatten()[2]], [ys.flatten()[2], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[2], zs.flatten()[2]], color = color_sc3, lw = lw)
            ax.plot([xs.flatten()[3], xs.flatten()[3]], [ys.flatten()[3], max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[3], zs.flatten()[3]], color = color_sc4, lw = lw)
            
            # show PM25 concentrations for each scenario with dashed lines
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[0]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[0], zs.flatten()[0]], linestyle = 'dashed', color = color_sc1, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[1]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[1], zs.flatten()[1]], linestyle = 'dashed', color = color_sc2, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[2]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[2], zs.flatten()[2]], linestyle = 'dashed', color = color_sc3, lw = lw)
            ax.plot([max(xs.flatten()) + x_shift_y + x_offset, xs.flatten()[3]], [max(ys.flatten()) + y_offset / 2, max(ys.flatten()) + y_offset / 2], 
                    [zs.flatten()[3], zs.flatten()[3]], linestyle = 'dashed', color = color_sc4, lw = lw)
            
            # text positioning
            ax.text(xs.flatten()[0] + text_sc_dict[airport][0], ys.flatten()[0] + text_sc_dict[airport][4], zs.flatten()[0] + text_sc_dict[airport][8], "Scenario I", color=color_sc1, ha = 'right')
            ax.text(xs.flatten()[1] + text_sc_dict[airport][1], ys.flatten()[1] + text_sc_dict[airport][5], zs.flatten()[1] + text_sc_dict[airport][9], "Scenario II", color=color_sc2, ha = 'right')
            ax.text(xs.flatten()[2] + text_sc_dict[airport][2], ys.flatten()[2] + text_sc_dict[airport][6], zs.flatten()[2] + text_sc_dict[airport][10], "Scenario III", color=color_sc3, ha = 'right')
            ax.text(xs.flatten()[3] + text_sc_dict[airport][3], ys.flatten()[3] + text_sc_dict[airport][7], zs.flatten()[3] + text_sc_dict[airport][11], "Scenario IV", color=color_sc4, ha = 'right')
            
            ax.text(x_offset + text_z_dict[airport][0], max(ys.flatten()) + y_offset / 2,
                    zs.flatten()[1] + text_z_dict[airport][3], '{}{:.2f}%'.format(aux_func.sign(zs[1]),zs[1]), size = 15,
                    color=color_sc2, zdir = 'x')
            ax.text(x_offset + text_z_dict[airport][1], max(ys.flatten()) + y_offset / 2,
                    zs.flatten()[2] + text_z_dict[airport][4], '{}{:.2f}%'.format(aux_func.sign(zs[2]),zs[2]), size = 15,
                    color=color_sc3, zdir = 'x')
            ax.text(x_offset + text_z_dict[airport][2], max(ys.flatten()) + y_offset / 2,
                    zs.flatten()[3] + text_z_dict[airport][5], '{}{:.2f}%'.format(aux_func.sign(zs[3]),zs[3]), size = 15,
                    color=color_sc4, zdir = 'x')
            
            ax.text(x_offset + text_y_dict[airport][0], ys.flatten()[1] + text_y_dict[airport][3], 0, '{}{:.2f}%'.format(aux_func.sign(ys[1]),ys[1]), size = 15,
                    color=color_sc2, zdir = 'x')
            ax.text(x_offset + text_y_dict[airport][1], ys.flatten()[2] + text_y_dict[airport][4], 0, '{}{:.2f}%'.format(aux_func.sign(ys[2]),ys[2]), size = 15,
                    color=color_sc3, zdir = 'x')
            ax.text(x_offset + text_y_dict[airport][2], ys.flatten()[3] + text_y_dict[airport][5], 0, '{}{:.2f}%'.format(aux_func.sign(ys[3]),ys[3]), size = 15,
                    color=color_sc4, zdir = 'x')
            
            ax.text(xs.flatten()[1] + text_x_dict[airport][0], text_x_dict[airport][3] - y_offset, 0, '{}{:.2f}%'.format(aux_func.sign(xs[1]),xs[1]), size = 15,
                    color=color_sc2, zdir = 'y')
            ax.text(xs.flatten()[2] + text_x_dict[airport][1], text_x_dict[airport][4] - y_offset, 0, '{}{:.2f}%'.format(aux_func.sign(xs[2]),xs[2]), size = 15,
                    color=color_sc3, zdir = 'y')
            ax.text(xs.flatten()[3] + text_x_dict[airport][2], text_x_dict[airport][5] - y_offset, 0, '{}{:.2f}%'.format(aux_func.sign(xs[3]),xs[3]), size = 15,
                    color=color_sc4, zdir = 'y')
        
        ax.quiver(xs.flatten()[0], ys.flatten()[0], zs.flatten()[0], xs.flatten()[1] - xs.flatten()[0], 
                  ys.flatten()[1] - ys.flatten()[0], zs.flatten()[1] - zs.flatten()[0], arrow_length_ratio = 0.03, color = 'k', label = airportlabel[airport])
        ax.quiver(xs.flatten()[1], ys.flatten()[1], zs.flatten()[1], xs.flatten()[2] - xs.flatten()[1], 
                  ys.flatten()[2] - ys.flatten()[1], zs.flatten()[2] - zs.flatten()[1], arrow_length_ratio = 0.03, color = 'k')
        ax.quiver(xs.flatten()[2], ys.flatten()[2], zs.flatten()[2], xs.flatten()[3] - xs.flatten()[2], 
                  ys.flatten()[3] - ys.flatten()[2], zs.flatten()[3] - zs.flatten()[2], arrow_length_ratio = 0.03, color = 'k')
        
        ax.set_xlim(min(xs), max(xs) + x_offset)
        ax.set_ylim(min(ys) - y_offset, max(ys) + y_offset / 2)
        ax.set_zlim(min(zs), max(zs) + z_offset)
        ax.set_xlabel("$\\Delta$NO$_2$ (%)", fontweight='bold')
        ax.set_ylabel("$\\Delta$O$_3$ (%)", fontweight='bold')
        ax.set_zlabel("$\\Delta$PM$_{2.5}$ (%)", fontweight='bold')
        plt.gca().invert_zaxis()
        plt.legend()
        
    def spatial_plots(self):
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        
        im = map.contourf(lon, lat, self.no2_sc2, np.arange(0, 15, 1),
                        extend='max', cmap='terrain_r', ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('NO$_2$', size = 30)
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        im = map.contourf(lon, lat, self.no2_sc4 - self.no2_sc1, np.arange(-1, 0.75, 0.05),
                        extend='both', cmap='seismic', ax = ax, norm = MidpointNormalize(midpoint = 0))
        cbar = plt.colorbar(im)
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('$\\Delta$NO$_2$ (Sc$_{IV}$ - Sc$_{I}$)', size = 30)
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        
        im = map.contourf(lon, lat, self.o3_sc2, np.arange(50, 102, 2),
                        extend='both', cmap='terrain_r', ax = ax)
        cbar = plt.colorbar(im)
        im.cmap.set_under('whitesmoke')
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('O$_3$', size = 30)
                
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        im = map.contourf(lon, lat, self.o3_sc4 - self.o3_sc1, np.arange(-1, 0.75, 0.05),
                        extend='both', cmap='seismic', ax = ax, norm = MidpointNormalize(midpoint = 0))
        cbar = plt.colorbar(im)
        im.cmap.set_under('black')
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('$\\Delta$O$_3$ (Sc$_{IV}$ - Sc$_{I}$)', size = 30)
                
        means = nc.Dataset(path + 'meanmeteo\\mean_sc2_wrfout_d01.nc')
        uvel_sc2 = np.mean(means['U10'][:], axis = 0)
        vvel_sc2 = np.mean(means['V10'][:], axis = 0)
        
        means = nc.Dataset(path + 'meanmeteo\\mean_sc1_wrfout_d01.nc')
        uvel_sc1 = np.mean(means['U10'][:], axis = 0)
        vvel_sc1 = np.mean(means['V10'][:], axis = 0)
        
        uvel_diff = uvel_sc1 - uvel_sc2
        vvel_diff = vvel_sc1 - vvel_sc2
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        
        im = map.contourf(lon, lat, self.pm25_sc2, np.arange(0, 21, 1),
                        extend='max', cmap='terrain_r', ax = ax)
        cbar = plt.colorbar(im)
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('PM$_{2.5}$', size = 30)
        
        lon, lat = map(d01.lons, d01.lats)
        
        im = map.quiver(lon, lat, uvel_sc2, vvel_sc2, scale = 120)
        plt.quiverkey(im, X=0.3, Y=-0.05, U = 5,
                     label='length = '+str(5) + ' m/s', labelpos='E')

        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        im = map.contourf(lon, lat, self.pm25_sc4 - self.pm25_sc1, np.arange(-1, 0.75, 0.05),
                        extend='both', cmap='seismic', ax = ax, norm = MidpointNormalize(midpoint = 0))
        cbar = plt.colorbar(im)
        cbar.set_label('{}'.format('$\\mu$g m$^{-3}$'))
        plt.title('$\\Delta$PM$_{2.5}$ (Sc$_{IV}$ - Sc$_{I}$)', size = 30)
        
        lon, lat = map(d01.lons, d01.lats)
        
        im = map.quiver(lon, lat, uvel_diff, vvel_diff, scale = 4)
        plt.quiverkey(im, X=0.3, Y=-0.05, U = 0.1,
                     label='length = '+str(0.1) + ' m/s', labelpos='E')
                
    def spread_emis(self, var):
        
        sc1 = self.emis_df[self.emis_df['scenario'] == 1]
        sc2 = self.emis_df[self.emis_df['scenario'] == 2]
        
        delta_NO2_fc = sc2[var].reset_index().drop('index', axis = 1) - sc1[var].reset_index().drop('index', axis = 1)
        delta_NO2_fc['fc'] = sc1['burntfuel']
        delta_NO2_fc['bin_fc'] = pd.cut(x = delta_NO2_fc['fc'], bins = [0, 1e-6, 1e7], labels = ['no fc', 'fc'],
                                        include_lowest = True)
        
        print(delta_NO2_fc.groupby('bin_fc')['NO2'].sum())
        
        delta_NO2_fc[var] = np.where((delta_NO2_fc[var] > 0.2) | (delta_NO2_fc[var] < -0.4), np.nan, delta_NO2_fc[var])
        plt.figure()
        delta_NO2_fc.hist(column = var, bins = 100)
        
    def VOCNOX_map(self):
        
        try:
            self.df
        except:
            self.pre_proc_local_AQ()
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp_sc1 = tri.Triangulation(self.emis_df[(self.emis_df['scenario'] == 1)]['lon'], self.emis_df[(self.emis_df['scenario'] == 1)]['lat'])
        interpolator_no = tri.LinearTriInterpolator(interp_sc1, self.emis_df[(self.emis_df['scenario'] == 1)]['NO'])
        no = interpolator_no(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_sc1, self.emis_df[(self.emis_df['scenario'] == 1)]['NO2'])
        no2 = interpolator_no2(self.Xi, self.Yi)
        interpolator_voc = tri.LinearTriInterpolator(interp_sc1, self.emis_df[(self.emis_df['scenario'] == 1)]['VOC'])
        voc = interpolator_voc(self.Xi, self.Yi)
        
        ratio = voc / (no + no2)
        
        plt.figure()
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h')
            
        map.drawcoastlines()
        map.drawcountries()
        
        lon, lat = map(self.Xi, self.Yi)
        im = map.contourf(lon, lat, ratio, np.arange(0.6, 3.8, 0.2),
                        extend='both', alpha = 0.8, cmap='terrain_r')
        im.cmap.set_under('whitesmoke')
        im.cmap.set_over('mediumblue')
        cbar = plt.colorbar(im)
        cbar.set_label('VOC/NO$_x$ ratio')
        
        df_plot = self.df.drop(self.df.index[[4, 5]])
        
        lons, lats = map(df_plot['lon'], df_plot['lat'])
        map.scatter(lons, lats, color = 'k', s = 70)
        
        for row in df_plot.iterrows():
            if row[0] != 4 and row[0] != 5:
                x, y = map(row[1]['lon'] + 0.3, row[1]['lat'] + 0.5)
                if row[1]['label'] == 'VIE':
                    plt.text(x - 0.5, y, row[1]['label'], color = 'white',
                                  fontsize=15, ha = 'right', bbox=dict(facecolor='black', 
                                                                      edgecolor='none',
                                                                      boxstyle='round',
                                                                      alpha=0.8))
                else:
                    plt.text(x, y, row[1]['label'], color = 'white',
                                  fontsize=15, ha = 'left', bbox=dict(facecolor='black', 
                                                                      edgecolor='none',
                                                                      boxstyle='round',
                                                                      alpha=0.8))
    
    def gen_timemeanlists_pm(self, var):
        
        arr = [[d01.time_mean(d01.scen1_wrfout_pm,var)[ground_lvl,:,:], d02.time_mean(d02.scen1_wrfout_pm,var)[ground_lvl,:,:],
                d03.time_mean(d03.scen1_wrfout_pm,var)[ground_lvl,:,:], d04.time_mean(d04.scen1_wrfout_pm,var)[ground_lvl,:,:]],
              [d01.time_mean(d01.scen2_wrfout_pm,var)[ground_lvl,:,:], d02.time_mean(d02.scen2_wrfout_pm,var)[ground_lvl,:,:],
                d03.time_mean(d03.scen2_wrfout_pm,var)[ground_lvl,:,:], d04.time_mean(d04.scen2_wrfout_pm,var)[ground_lvl,:,:]],
              [d01.time_mean(d01.scen3_wrfout_pm,var)[ground_lvl,:,:], d02.time_mean(d02.scen3_wrfout_pm,var)[ground_lvl,:,:],
                d03.time_mean(d03.scen3_wrfout_pm,var)[ground_lvl,:,:], d04.time_mean(d04.scen3_wrfout_pm,var)[ground_lvl,:,:]],
              [d01.time_mean(d01.scen4_wrfout_pm,var)[ground_lvl,:,:], d02.time_mean(d02.scen4_wrfout_pm,var)[ground_lvl,:,:],
                d03.time_mean(d03.scen4_wrfout_pm,var)[ground_lvl,:,:], d04.time_mean(d04.scen4_wrfout_pm,var)[ground_lvl,:,:]]]

        return arr
    
    def aerosols(self):

        self.emis_df_aero = create_df_merged_dom(timeshift = 0, periods = 1, bc_a01 = self.gen_timemeanlists_pm('bc_a01'),
                                                 bc_a02 = self.gen_timemeanlists_pm('bc_a02'), bc_a03 = self.gen_timemeanlists_pm('bc_a03'), 
                                                 oc_a01 = self.gen_timemeanlists_pm('oc_a01'), oc_a02 = self.gen_timemeanlists_pm('oc_a02'),
                                                 oc_a03 = self.gen_timemeanlists_pm('oc_a03'), oin_a01 = self.gen_timemeanlists_pm('oin_a01'),
                                                 oin_a02 = self.gen_timemeanlists_pm('oin_a02'), oin_a03 = self.gen_timemeanlists_pm('oin_a03'),
                                                 so4_a01 = self.gen_timemeanlists_pm('so4_a01'), so4_a02 = self.gen_timemeanlists_pm('so4_a02'),
                                                 so4_a03 = self.gen_timemeanlists_pm('so4_a03'), no3_a01 = self.gen_timemeanlists_pm('no3_a01'),
                                                 no3_a02 = self.gen_timemeanlists_pm('no3_a02'), no3_a03 = self.gen_timemeanlists_pm('no3_a03'),
                                                 na_a01 = self.gen_timemeanlists_pm('na_a01'), na_a02 = self.gen_timemeanlists_pm('na_a02'),
                                                 na_a03 = self.gen_timemeanlists_pm('na_a03'), nh4_a01 = self.gen_timemeanlists_pm('nh4_a01'),
                                                 nh4_a02 = self.gen_timemeanlists_pm('nh4_a02'), nh4_a03 = self.gen_timemeanlists_pm('nh4_a03'), 
                                                 PM25 = self.gen_timemeanlists('PM2_5_DRY'), burntfuel = burntfuel)
        print('created emis_df_aero')
        
        for compound in ['bc_a01', 'bc_a02', 'bc_a03', 'oc_a01', 'oc_a02', 'oc_a03', 'oin_a01', 'oin_a02', 'oin_a03',
                          'so4_a01', 'so4_a02', 'so4_a03', 'no3_a01', 'no3_a02', 'no3_a03', 'na_a01', 'na_a02', 'na_a03',
                          'nh4_a01', 'nh4_a02', 'nh4_a03']:
            self.emis_df_aero[compound] = pd.to_numeric(self.emis_df_aero[compound], downcast = 'float')
            
        self.emis_df_aero['bc'] = self.emis_df_aero['bc_a01'] + self.emis_df_aero['bc_a02'] + self.emis_df_aero['bc_a03']
        self.emis_df_aero['oc'] = self.emis_df_aero['oc_a01'] + self.emis_df_aero['oc_a02'] + self.emis_df_aero['oc_a03']
        self.emis_df_aero['oin'] = self.emis_df_aero['oin_a01'] + self.emis_df_aero['oin_a02'] + self.emis_df_aero['oin_a03']
        self.emis_df_aero['so4'] = self.emis_df_aero['so4_a01'] + self.emis_df_aero['so4_a02'] + self.emis_df_aero['so4_a03']
        self.emis_df_aero['no3'] = self.emis_df_aero['no3_a01'] + self.emis_df_aero['no3_a02'] + self.emis_df_aero['no3_a03']
        self.emis_df_aero['na'] = self.emis_df_aero['na_a01'] + self.emis_df_aero['na_a02'] + self.emis_df_aero['na_a03']
        self.emis_df_aero['nh4'] = self.emis_df_aero['nh4_a01'] + self.emis_df_aero['nh4_a02'] + self.emis_df_aero['nh4_a03']

        self.emis_df_aero['share'] = (self.emis_df_aero['bc'] + self.emis_df_aero['oc'] + self.emis_df_aero['oin'] + \
                                      self.emis_df_aero['so4'] + self.emis_df_aero['no3'] + self.emis_df_aero['na'] + self.emis_df_aero['nh4']) / self.emis_df_aero['PM25'] * 100

        self.emis_df_aero['other fine PM'] = self.emis_df_aero['PM25'] - (self.emis_df_aero['bc'] + self.emis_df_aero['oc'] + self.emis_df_aero['oin'] + \
                                                                          self.emis_df_aero['so4'] + self.emis_df_aero['no3'] + self.emis_df_aero['na'] + self.emis_df_aero['nh4'])
        
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp = tri.Triangulation(self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['lon'], self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['lat'])
        
        interpolator_bc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['bc'])
        self.bc_sc1 = interpolator_bc(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['PM25'])
        self.pm25_sc1 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_oc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['oc'])
        self.oc_sc1 = interpolator_oc(self.Xi, self.Yi)
        interpolator_oin = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['oin'])
        self.oin_sc1 = interpolator_oin(self.Xi, self.Yi)
        interpolator_so4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['so4'])
        self.so4_sc1 = interpolator_so4(self.Xi, self.Yi)
        interpolator_no3 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['no3'])
        self.no3_sc1 = interpolator_no3(self.Xi, self.Yi)
        interpolator_na = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['na'])
        self.na_sc1 = interpolator_na(self.Xi, self.Yi)
        interpolator_nh4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 1)]['nh4'])
        self.nh4_sc1 = interpolator_nh4(self.Xi, self.Yi)
        
        interpolator_bc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['bc'])
        self.bc_sc2 = interpolator_bc(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['PM25'])
        self.pm25_sc2 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_oc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['oc'])
        self.oc_sc2 = interpolator_oc(self.Xi, self.Yi)
        interpolator_oin = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['oin'])
        self.oin_sc2 = interpolator_oin(self.Xi, self.Yi)
        interpolator_so4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['so4'])
        self.so4_sc2 = interpolator_so4(self.Xi, self.Yi)
        interpolator_no3 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['no3'])
        self.no3_sc2 = interpolator_no3(self.Xi, self.Yi)
        interpolator_na = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['na'])
        self.na_sc2 = interpolator_na(self.Xi, self.Yi)
        interpolator_nh4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 2)]['nh4'])
        self.nh4_sc2 = interpolator_nh4(self.Xi, self.Yi)
        
        interpolator_bc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['bc'])
        self.bc_sc3 = interpolator_bc(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['PM25'])
        self.pm25_sc3 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_oc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['oc'])
        self.oc_sc3 = interpolator_oc(self.Xi, self.Yi)
        interpolator_oin = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['oin'])
        self.oin_sc3 = interpolator_oin(self.Xi, self.Yi)
        interpolator_so4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['so4'])
        self.so4_sc3 = interpolator_so4(self.Xi, self.Yi)
        interpolator_no3 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['no3'])
        self.no3_sc3 = interpolator_no3(self.Xi, self.Yi)
        interpolator_na = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['na'])
        self.na_sc3 = interpolator_na(self.Xi, self.Yi)
        interpolator_nh4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 3)]['nh4'])
        self.nh4_sc3 = interpolator_nh4(self.Xi, self.Yi)
        
        interpolator_bc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['bc'])
        self.bc_sc4 = interpolator_bc(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['PM25'])
        self.pm25_sc4 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_oc = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['oc'])
        self.oc_sc4 = interpolator_oc(self.Xi, self.Yi)
        interpolator_oin = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['oin'])
        self.oin_sc4 = interpolator_oin(self.Xi, self.Yi)
        interpolator_so4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['so4'])
        self.so4_sc4 = interpolator_so4(self.Xi, self.Yi)
        interpolator_no3 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['no3'])
        self.no3_sc4 = interpolator_no3(self.Xi, self.Yi)
        interpolator_na = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['na'])
        self.na_sc4 = interpolator_na(self.Xi, self.Yi)
        interpolator_nh4 = tri.LinearTriInterpolator(interp, self.emis_df_aero[(self.emis_df_aero['scenario'] == 4)]['nh4'])
        self.nh4_sc4 = interpolator_nh4(self.Xi, self.Yi)
        
    def RP_PM25(self):
        
        try:
            self.emis_df_aero
        except NameError:
            self.aerosols()

        mean_pmcomp = self.emis_df_aero.groupby('scenario')[['bc', 'oc', 'oin', 'so4', 'no3', 'na', 'nh4', 'other fine PM']].mean()    
        mean_pmcomp_adj = mean_pmcomp
        mean_pmcomp_adj.loc[2] = (mean_pmcomp_adj.loc[1] - mean_pmcomp_adj.loc[2]) / (mean_pmcomp_adj.loc[1] - mean_pmcomp_adj.loc[4]) * 100
        mean_pmcomp_adj.loc[3] = (mean_pmcomp_adj.loc[1] - mean_pmcomp_adj.loc[3]) / (mean_pmcomp_adj.loc[1] - mean_pmcomp_adj.loc[4]) * 100
        
        mean_pmcomp_adj = mean_pmcomp_adj.loc[[2,3]]
        mean_pmcomp_adj['scenario'] = ['Sc$_{II}$', 'Sc$_{III}$']
        RP_mean = pd.melt(mean_pmcomp_adj, id_vars = 'scenario', var_name = 'compound', value_name = 'RP (%)')
        
        category_names = ['Black\ncarbon', 'Organic\ncarbon', 'Other\ninorganics', 'Nitrate',
                          'Sulfate', 'Sodium', 'Ammonium', 'Other\n fine PM']
        
        fig, ax = plt.subplots()
        bars = sns.barplot(x = 'compound', y = 'RP (%)', hue = 'scenario', data = RP_mean, 
                           palette = [color_sc2, color_sc3], ax = ax)
        
        for p in bars.patches:
                bars.annotate('{:.0f}%'.format(p.get_height()), (p.get_x() + 0.5 * p.get_width(), p.get_height()),
                            ha='center', va='bottom', color= 'black', fontsize = 17)
        
        plt.gca().invert_yaxis()
        ax.set_ylim([110,-60])
        ax.set_xticklabels(category_names)
        legend = bars.legend(frameon = True, bbox_to_anchor=(0.12, 0.45))
        frame = legend.get_frame()
        frame.set_color('white')
        
        compress = 0.1
        x_start = np.array([-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5]) + compress
        x_end = np.array(x_start+[1, 1, 1, 1, 1, 1, 1, 1]) - compress * 2
        targets = (100, 100, 100, 100, 100, 100, 100, 100)
        plt.hlines(targets, x_start, x_end, linestyles = 'dashed', colors = ['k', 'k', 'k', 'k', 'k', 'k', 'k', 'k'])
        targets = (0, 0, 0, 0, 0, 0, 0, 0)
        plt.hlines(targets, x_start, x_end, linestyles = 'dashed', colors = ['k', 'k', 'k', 'k', 'k', 'k', 'k', 'k'])
        plt.vlines(x_start, [0, 0, 0, 0, 0, 0, 0, 0], [100, 100, 100, 100, 100, 100, 100, 100], linestyles = 'dashed', colors = ['k', 'k', 'k', 'k', 'k', 'k', 'k', 'k'])
        plt.vlines(x_end, [0, 0, 0, 0, 0, 0, 0, 0], [100, 100, 100, 100, 100, 100, 100, 100], linestyles = 'dashed', colors = ['k', 'k', 'k', 'k', 'k', 'k', 'k', 'k'])
        
        bot_lab = mean_pmcomp.loc[4].tolist()
        top_lab = mean_pmcomp.loc[1].tolist()
        xmid = (x_start + x_end) / 2
        
        for idx, x in enumerate(xmid):
            plt.text(x, 100 + 7, '{:.2f}'.format(bot_lab[idx]), ha = 'center', fontsize = 17)
            plt.text(x, 0 - 2, '{:.2f}'.format(top_lab[idx]), ha = 'center', fontsize = 17)
        
        plt.hlines(100, x_start[0] - 0.9, x_start[0] - 0.1, linestyles = 'dashed', colors = 'k')
        plt.text(x_start[0] - 0.5, 100 + 6, 'Sc$_{IV}$ ($\\mu$g m$^{{-3}}$)', ha = 'center', fontsize = 16)   
        plt.hlines(0, x_start[0] - 0.9, x_start[0] - 0.1, linestyles = 'dashed', colors = 'k')
        plt.text(x_start[0] - 0.5, 0 - 3, 'Sc$_{I}$ ($\\mu$g m$^{{-3}}$)', ha = 'center', fontsize = 16)

    def PM25_decomp(self):
        category_names = ['Black carbon (P)', 'Organic carbon (P)', 'Other inorganics (P)', 'Sodium (P)',
                          'Sulfate (S)', 'Nitrate (S)', 'Ammonium (S)', 'Other fine PM (P/S)']
            
        emis_df_aero_ap = self.emis_df_aero[self.emis_df_aero['burntfuel'] > fuel_lim]
        df = emis_df_aero_ap[['lon','lat']].drop_duplicates().reset_index().drop('index', axis = 1)
        df['label'] = ['MXP', 'ZRH', 'VIE', 'MUC', 'CDG', 'LHR',
                        'LHR', 'DUB', 'MAN', 'CDG', 'FRA', 'AMS']  

        emis_df_aero_ap = emis_df_aero_ap.merge(df, how="left", on = ['lat', 'lon'])
        emis_df_aero_ap['domain'] = pd.to_numeric(emis_df_aero_ap['domain'], downcast = 'integer')
        
        emis_df_aero_ap[emis_df_aero_ap['label'] == 'AMS']['burntfuel'] = emis_df_aero_ap[emis_df_aero_ap['label'] == 'AMS']['burntfuel'] + 122841.555 + 139274.25
        emis_df_aero_ap[(emis_df_aero_ap['lon'] == -0.25) & (emis_df_aero_ap['lat'] == 51.5)]['burntfuel'] = emis_df_aero_ap[(emis_df_aero_ap['lon'] == -0.25) & (emis_df_aero_ap['lat'] == 51)]['burntfuel'] + emis_df_aero_ap[(emis_df_aero_ap['lon'] == -0.25) & (emis_df_aero_ap['lat'] == 51.5)]['burntfuel']
        indexNames = emis_df_aero_ap[(emis_df_aero_ap['lon'] == -0.25) & (emis_df_aero_ap['lat'] == 51)].index
        emis_df_aero_ap.drop(indexNames, inplace=True)

        group_emis = emis_df_aero_ap.groupby(['label', 'scenario'])[['bc_a01', 'bc_a02', 'bc_a03', 'oc_a01', 'oc_a02', 'oc_a03', 'oin_a01', 'oin_a02', 'oin_a03',
                                 'so4_a01', 'so4_a02', 'so4_a03', 'no3_a01', 'no3_a02', 'no3_a03', 'na_a01', 'na_a02', 'na_a03',
                                 'nh4_a01', 'nh4_a02', 'nh4_a03', 'bc', 'oc', 'oin', 'so4', 'no3', 'na', 'nh4', 'other fine PM']].mean()
        group_emis.reset_index(inplace = True)
        emis_df_aero_ap = pd.concat([emis_df_aero_ap[emis_df_aero_ap['label'].isna()][group_emis.columns], group_emis])
        
        pmcomp = emis_df_aero_ap[emis_df_aero_ap['label'].isin(['MAN', 'CDG', 'LHR', 'FRA'])][['label', 'scenario', 'bc',
                                                                                        'oc', 'oin', 'so4', 'no3', 'na', 'nh4', 'other fine PM']]
        
        pmcomp = pd.melt(pmcomp, id_vars = ['scenario', 'label'], value_vars = ['bc', 'oc', 'oin', 'so4', 'no3', 'na', 'nh4', 'other fine PM'],
                         var_name = 'var', value_name = 'concentration')
        
        def gen_input_dict(scen):
            results = {
                'CDG': [pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[0],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[1],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[2],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[5],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[3],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[4],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[6],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'CDG')]['concentration'].iloc[7]],
                'FRA': [pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[0],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[1],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[2],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[5],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[3],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[4],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[6],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'FRA')]['concentration'].iloc[7]],
                'LHR': [pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[0],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[1],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[2],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[5],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[3],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[4],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[6],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'LHR')]['concentration'].iloc[7]],
                'MAN': [pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[0],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[1],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[2],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[5],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[3],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[4],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[6],
                        pmcomp[(pmcomp['scenario'] == scen) & (pmcomp['label'] == 'MAN')]['concentration'].iloc[7]]
            }
        
            return results
        
        df1 = pd.DataFrame(gen_input_dict(1), index = category_names)
        df2 = pd.DataFrame(gen_input_dict(2), index = category_names)
        df3 = pd.DataFrame(gen_input_dict(3), index = category_names)
        df4 = pd.DataFrame(gen_input_dict(4), index = category_names)
        ylabs = [' \nParis Charles de \n Gaulle (CDG)', 'Frankfurt \nAirport (FRA)', 'London \nHeathrow (LHR)', 'Manchester \nAirport (MAN)']
        
        def plot_clustered_stacked(dfall):
        
            n_df = len(dfall)
            n_col = len(dfall[0].columns) 
            n_ind = len(dfall[0].index)
            plt.figure(figsize=(15, 8))
            axe = plt.subplot(111)
        
            for idx, df in enumerate(dfall): # for each data frame
                
                axe = df.plot(kind="barh",
                              linewidth=5,
                              stacked=True,
                              ax=axe,
                              legend=False,
                              grid=False,
                              color=np.vstack([plt.get_cmap('summer')(np.linspace(0.15, 0.85, 4)), plt.get_cmap('cool')(np.linspace(0.15, 0.85, 4))])
                              )  # make bar plots
        
        
            corr = [0,0.01,0.02,0.03]
            h,l = axe.get_legend_handles_labels()
            for i in range(0, n_df * n_col, n_col):
                cor = corr[int(i / 7)]
                for j, pa in enumerate(h[i:i+n_col]):
                    for rect in pa.patches: # for each index
                        rect.set_y(rect.get_y() + 1 / float(n_df + 1) * i / float(n_col) + cor - 0.125)
                        rect.set_height(1 / float(n_df + 1))
                        #if j != 1:
                        axe.text(rect.get_x() + rect.get_width() / 2,
                                rect.get_height() / 1.75 + rect.get_y(),
                                '{:.2f}'.format(rect.get_width(), 1),
                                ha='center',
                                va='center',
                                color='k',
                                weight='bold',
                                size=17
                                )

            center_top = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2. + 0.03
            center_down = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2. - 0.17
            tops = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2. + 0.25
            downs = (np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2. - 0.25 - 0.15
            
            ticks = []
            for i in range(len(center_top)):
                ticks.append([downs[i], center_down[i], center_top[i], tops[i]])
                axe.text(-2, 0.15 + i, r'{}'.format(ylabs[i]), fontsize=18, fontweight = 'bold')
        
            ticklabels = ['Sc$_{I}$', 'Sc$_{II}$', 'Sc$_{III}$', 'Sc$_{IV}$', 'Sc$_{I}$', 'Sc$_{II}$', 'Sc$_{III}$', 'Sc$_{IV}$',
                          'Sc$_{I}$', 'Sc$_{II}$', 'Sc$_{III}$', 'Sc$_{IV}$', 'Sc$_{I}$', 'Sc$_{II}$', 'Sc$_{III}$', 'Sc$_{IV}$']
            ticks = np.reshape(np.array(ticks), -1)
            axe.set_yticks(ticks)
            axe.set_yticklabels(ticklabels, rotation = 0, fontsize = 16)
            axe.set_xticks(np.arange(0, 11.5, 0.5))
            axe.set_xticklabels(np.arange(0, 11.5, 0.5))
            axe.invert_yaxis()
            axe.set_xlabel('total PM$_{2.5}$ ($\\mu$g m$^{-3}$)')
            axe.legend(h[:n_col], l[:n_col], ncol = 4, bbox_to_anchor=(0, 1),
                      loc='lower left')
        
            return axe
        
        plot_clustered_stacked([df1.T, df2.T, df3.T, df4.T])
        
    def QQplots(self):
        
        no2_sc1 = self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'] == 3)]['NO2'].values
        no2_sc2 = self.emis_df[(self.emis_df['scenario'] == 2) & (self.emis_df['domain'] == 3)]['NO2'].values
        no2_sc3 = self.emis_df[(self.emis_df['scenario'] == 3) & (self.emis_df['domain'] == 3)]['NO2'].values
        no2_sc4 = self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'] == 3)]['NO2'].values
        
        no2_2_1_diff = (no2_sc2 - no2_sc1) / no2_sc1 * 100
        no2_2_1_diff = (no2_2_1_diff - np.mean(no2_2_1_diff)) / np.std(no2_2_1_diff)
        no2_3_1_diff = (no2_sc3 - no2_sc1) / no2_sc1 * 100
        no2_3_1_diff = (no2_3_1_diff - np.mean(no2_3_1_diff)) / np.std(no2_3_1_diff)
        no2_4_1_diff = (no2_sc4 - no2_sc1) / no2_sc1 * 100
        no2_4_1_diff = (no2_4_1_diff - np.mean(no2_4_1_diff)) / np.std(no2_4_1_diff)
        
        o3_sc1 = self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'] == 3)]['O3'].values
        o3_sc2 = self.emis_df[(self.emis_df['scenario'] == 2) & (self.emis_df['domain'] == 3)]['O3'].values
        o3_sc3 = self.emis_df[(self.emis_df['scenario'] == 3) & (self.emis_df['domain'] == 3)]['O3'].values
        o3_sc4 = self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'] == 3)]['O3'].values
        
        o3_2_1_diff = (o3_sc2 - o3_sc1) / o3_sc1 * 100
        o3_2_1_diff = (o3_2_1_diff - np.mean(o3_2_1_diff)) / np.std(o3_2_1_diff)
        o3_3_1_diff = (o3_sc3 - o3_sc1) / o3_sc1 * 100
        o3_3_1_diff = (o3_3_1_diff - np.mean(o3_3_1_diff)) / np.std(o3_3_1_diff)
        o3_4_1_diff = (o3_sc4 - o3_sc1) / o3_sc1 * 100
        o3_4_1_diff = (o3_4_1_diff - np.mean(o3_4_1_diff)) / np.std(o3_4_1_diff)
        
        pm25_sc1 = self.emis_df[(self.emis_df['scenario'] == 1) & (self.emis_df['domain'] == 3)]['PM25'].values
        pm25_sc2 = self.emis_df[(self.emis_df['scenario'] == 2) & (self.emis_df['domain'] == 3)]['PM25'].values
        pm25_sc3 = self.emis_df[(self.emis_df['scenario'] == 3) & (self.emis_df['domain'] == 3)]['PM25'].values
        pm25_sc4 = self.emis_df[(self.emis_df['scenario'] == 4) & (self.emis_df['domain'] == 3)]['PM25'].values
        
        pm25_2_1_diff = (pm25_sc2 - pm25_sc1) / pm25_sc1 * 100
        pm25_2_1_diff = (pm25_2_1_diff - np.mean(pm25_2_1_diff)) / np.std(pm25_2_1_diff)
        pm25_3_1_diff = (pm25_sc3 - pm25_sc1) / pm25_sc1 * 100
        pm25_3_1_diff = (pm25_3_1_diff - np.mean(pm25_3_1_diff)) / np.std(pm25_3_1_diff)
        pm25_4_1_diff = (pm25_sc4 - pm25_sc1) / pm25_sc1 * 100
        pm25_4_1_diff = (pm25_4_1_diff - np.mean(pm25_4_1_diff)) / np.std(pm25_4_1_diff)
        
        fig = plt.figure(figsize=(12,8))
        
        ax1 = fig.add_subplot(331)
        sm.qqplot(no2_2_1_diff, line='s', ax = ax1, markerfacecolor = color_sc1, markeredgecolor = color_sc1)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('NO$_2$\nSample Quantiles')
        ax1.set_title('Sc$_{II}$ - Sc$_{I}$')
        
        ax1 = fig.add_subplot(332)
        sm.qqplot(no2_3_1_diff, line='s', ax = ax1, markerfacecolor = color_sc3, markeredgecolor = color_sc3)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('')
        ax1.set_title('Sc$_{III}$ - Sc$_{I}$')
        
        ax1 = fig.add_subplot(333)
        sm.qqplot(no2_4_1_diff, line='s', ax = ax1, markerfacecolor = color_sc4, markeredgecolor = color_sc4)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('')
        ax1.set_title('Sc$_{IV}$ - Sc$_{I}$')
        
        ax1 = fig.add_subplot(334)
        sm.qqplot(o3_2_1_diff, line='s', ax = ax1, markerfacecolor = color_sc1, markeredgecolor = color_sc1)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('O$_3$\nSample Quantiles')
        
        ax1 = fig.add_subplot(335)
        sm.qqplot(o3_3_1_diff, line='s', ax = ax1, markerfacecolor = color_sc3, markeredgecolor = color_sc3)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('')
        
        ax1 = fig.add_subplot(336)
        sm.qqplot(o3_4_1_diff, line='s', ax = ax1, markerfacecolor = color_sc4, markeredgecolor = color_sc4)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_xlabel('')
        ax1.set_ylabel('')
        
        ax1 = fig.add_subplot(337)
        sm.qqplot(pm25_2_1_diff, line='s', ax = ax1, markerfacecolor = color_sc1, markeredgecolor = color_sc1)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_ylabel('PM$_{2.5}$\nSample Quantiles')
        
        ax1 = fig.add_subplot(338)
        sm.qqplot(pm25_3_1_diff, line='s', ax = ax1, markerfacecolor = color_sc3, markeredgecolor = color_sc3)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_ylabel('')
        
        ax1 = fig.add_subplot(339)
        sm.qqplot(pm25_4_1_diff, line='s', ax = ax1, markerfacecolor = color_sc4, markeredgecolor = color_sc4)
        ax1.set_xlim([-4, 4])
        ax1.set_ylim([-4, 4])
        ax1.set_ylabel('')



class _24hr_meandata:
    
    '''
    Daily (24 hour) mean analysis. Restricted to NO2 and PM2.5 as the WHO has 24-hour guidelines for those two vars (not for O3)
    
    Methods
    ----------
    gen_inoutput        : Generates the aggregated dataframe of main variables (NO2 and PM2.5) and linearly interpolates variables onto resolution of d04 (highest resolution)
    limit_exc_24hr      : Evaluates exceedances of daily WHO guideline concentrations and the locations and freqs at which this happens
    timeseries_24hr (v) : Violinplots of daily mean concentrations + WHO guideline exceedances, Figures 5.9 & 5.10 of report. Scenarios 1 & 4 are paired, as well as scenarios 2 & 3
    '''
    
    def __init__(self):
        
        self.W = 24
        self.gen_inoutput()
        
    def gen_inoutput(self):
        no2_list = [[np.mean(d01.scen1_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen1_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
                     np.mean(d03.scen1_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen1_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
            [np.mean(d01.scen2_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen2_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
             np.mean(d03.scen2_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen2_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
            [np.mean(d01.scen3_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen3_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
             np.mean(d03.scen3_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen3_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
            [np.mean(d01.scen4_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen4_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
             np.mean(d03.scen4_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen4_wrfout['no2'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)]]
        pm25_list = [[np.mean(d01.scen1_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen1_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
                      np.mean(d03.scen1_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen1_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
                    [np.mean(d01.scen2_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen2_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
                     np.mean(d03.scen2_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen2_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
                    [np.mean(d01.scen3_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen3_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), 
                     np.mean(d03.scen3_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen3_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)],
                    [np.mean(d01.scen4_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d02.scen4_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0),
                     np.mean(d03.scen4_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 39, 48), axis = 0), np.mean(d04.scen4_wrfout['PM2_5_DRY'][idx_nospinup:-1,0,:,:].reshape(24, -1, 78, 99), axis = 0)]]

        self.emis_df_24h = create_df_merged_dom(timeshift = 24, periods = 14, NO2 = no2_list, PM25 = pm25_list, burntfuel = burntfuel)

        xi = np.linspace(d01.min_lon, d01.max_lon, new_res_lon)
        yi = np.linspace(d01.min_lat, d01.max_lat, new_res_lat)
        self.Xi, self.Yi = np.meshgrid(xi, yi)
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp_sc1 = tri.Triangulation(self.emis_df_24h[(self.emis_df_24h['scenario'] == 1)]['lon'], self.emis_df_24h[(self.emis_df_24h['scenario'] == 1)]['lat'])
        interpolator_no2 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 1)]['NO2'])
        self.no2_sc1 = interpolator_no2(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 1)]['PM25'])
        self.pm25_sc1 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 2)]['NO2'])
        self.no2_sc2 = interpolator_no2(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 2)]['PM25'])
        self.pm25_sc2 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 3)]['NO2'])
        self.no2_sc3 = interpolator_no2(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 3)]['PM25'])
        self.pm25_sc3 = interpolator_pm25(self.Xi, self.Yi)
        interpolator_no2 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 4)]['NO2'])
        self.no2_sc4 = interpolator_no2(self.Xi, self.Yi)
        interpolator_pm25 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_24h[(self.emis_df_24h['scenario'] == 4)]['PM25'])
        self.pm25_sc4 = interpolator_pm25(self.Xi, self.Yi)
        
    def limit_exc_24hr(self, var, scen):
        
        domains = [1,2,3,4] if var == 'PM25' else [3,4]
        lim_val = lim_24hr_pm25_who if var == 'PM25' else lim_24hr_no2_who
        domain = d01 if var == 'PM25' else d03
        
        xi = np.linspace(domain.min_lon, domain.max_lon, 500)
        yi = np.linspace(domain.min_lat, domain.max_lat, 500)
        self.Xi, self.Yi = np.meshgrid(xi, yi)
        
        interp_sc1 = tri.Triangulation(self.emis_df_24h[(self.emis_df_24h['scenario'] == scen) & (self.emis_df_24h['domain'].isin(domains)) & (self.emis_df_24h['date'] == self.emis_df_24h['date'].unique()[0])]['lon'],
                                       self.emis_df_24h[(self.emis_df_24h['scenario'] == scen) & (self.emis_df_24h['domain'].isin(domains)) & (self.emis_df_24h['date'] == self.emis_df_24h['date'].unique()[0])]['lat'])
        
        data_inter = []
        
        for date in self.emis_df_24h['date'].unique():
            data = self.emis_df_24h[self.emis_df_24h['date'] == date]
            interpolator = tri.LinearTriInterpolator(interp_sc1, data[(data['scenario'] == scen) & (data['domain'].isin(domains))][var])
            data_inter.append(interpolator(self.Xi, self.Yi))
        
        data_inter = np.reshape(np.stack(data_inter), (14, -1))
        
        self.df_24hr = pd.DataFrame(zip(np.repeat(self.emis_df_24h['date'].unique(), np.shape(data_inter)[1]), 
                                data_inter.flatten(), np.tile(self.Xi.flatten(), np.shape(data_inter)[0]),
                                np.tile(self.Yi.flatten(), np.shape(data_inter)[0])), columns = ['date', var, 'lon', 'lat'])
    
        self.groupby = self.df_24hr.groupby('date')[['date', var]]
        self.grouped = self.groupby.mean()
    
        self.exc_ind = self.df_24hr[self.df_24hr[var] > lim_val].groupby('date')[var].count() / self.df_24hr.groupby('date')[['date', var]].count()['date'].iloc[0] * 100
    
        exceed = self.df_24hr[self.df_24hr[var] > lim_val]
        num_timelocs = len(self.df_24hr)
        num_loc = len(self.df_24hr[self.df_24hr['date'] == self.df_24hr['date'].iloc[0]])
        
        self.ind_loc_exc = exceed.groupby(['lon', 'lat'])[['lon', 'lat']].count()
        print('Scenario {}:\nMean exceedances per location where lim is exceeded: {:.2f}'.format(
            scen, self.ind_loc_exc['lon'].sum() / len(self.ind_loc_exc)))
        self.perc_exc = self.ind_loc_exc['lon'].sum() / num_timelocs * 100
        perc_loc_exc = len(self.ind_loc_exc) / num_loc * 100
        print('% exceedances of total: {:.2f}%\n%Locations exceeding limit once: {:.2f}%'.format(self.perc_exc, perc_loc_exc))
        perc_loc_exc = len(self.ind_loc_exc[self.ind_loc_exc['lon'] > 3]) / num_loc * 100
        print('% locations exceeding limit four times or more: {:.2f}%'.format(perc_loc_exc))
        print(num_loc, num_timelocs)
        
        return self.df_24hr
        
    def timeseries_24hr(self, var, scens): # var could be 'NO2' or 'PM25', scens could be '14' (1 & 4) or '23' (2 & 3)
        
        if scens == '14':
            self.limit_exc_24hr(var, 1)
            sc1 = self.df_24hr
            self.limit_exc_24hr(var, 4)
            sc4 = self.df_24hr
            comb_sc = sc1.append(sc4, ignore_index=True)
            comb_sc['scenario'] = np.repeat(['Sc$_{I}$', 'Sc$_{IV}$'], int(len(comb_sc) / 2))
        elif scens == '23':
            self.limit_exc_24hr(var, 2)
            sc2 = self.df_24hr
            self.limit_exc_24hr(var, 3)
            sc3 = self.df_24hr
            comb_sc = sc2.append(sc3, ignore_index=True)
            comb_sc['scenario'] = np.repeat(['Sc$_{II}$', 'Sc$_{III}$'], int(len(comb_sc) / 2))
        
        dates = self.df_24hr['date'].unique()
        lim_val = lim_24hr_pm25_who if var == 'PM25' else lim_24hr_no2_who
        
        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 10), 
                                 sharex=True, gridspec_kw=dict(height_ratios=(1, 3), hspace=0.1))
        cplot = sns.countplot(data = comb_sc[comb_sc[var] > lim_val], x = 'date', 
                              hue = 'scenario', ax = axes[0], palette = sc14colors if scens == '14' else sc23colors)
        for p in cplot.patches:
            cplot.annotate(format(p.get_height() / (500 * 500) * 100, '.2f') + '%', (p.get_x() + p.get_width() / 2., p.get_height() / 2),
                            ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points',
                            fontsize = 17, rotation = 90, bbox=dict(facecolor='white', 
                                                                                          edgecolor='none',
                                                                                          boxstyle='round',
                                                                                          alpha=0.5))
        axes[0].legend([],[], frameon=False)
        if var == 'PM25':
            axes[0].set_yticks(np.arange(0, 24000, 4000))
            axes[0].set_yticklabels(np.arange(0, 24000, 4000) / (500 * 500) * 100)
        elif var == 'NO2':
            axes[0].set_yticks(np.arange(0, 1200, 200))
            axes[0].set_yticklabels(np.arange(0, 1200, 200) / (500 * 500) * 100)
        myFmt = mdates.DateFormatter('%d-%b')
        axes[0].xaxis.set_major_formatter(plt.FixedFormatter([(pd.to_datetime(str(item)) - dt.timedelta(hours = 24)).strftime("%d-%m") for item in dates]))
        axes[0].set_ylabel('above WHO\nguideline (%)')
        sns.violinplot('date', var, data = comb_sc, hue = 'scenario', split=True, ax=axes[1],
                        palette = sc14colors if scens == '14' else sc23colors)
        axes[1].xaxis.set_major_formatter(plt.FixedFormatter([(pd.to_datetime(str(item)) - dt.timedelta(hours = 24)).strftime("%d-%m") for item in dates]))
        axes[1].set_ylabel('{} ($\\mu$g m$^{{-3}}$)'.format('PM$_{2.5}$' if var == 'PM25' else 'NO$_2$'))
        axes[1].axhline(y = lim_val, color = 'k', linestyle = 'dashed', label = 'WHO guideline')
        legend = axes[1].legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')


class _8hr_meandata:
    
    '''
    8-hour mean analysis. Restricted to ozone (O3) as the WHO has 8-hour guidelines for only this var (not for NO2 or PM2.5)
    
    Methods
    ----------
    gen_inoutput        : Generates the aggregated dataframe of main variable (O3) and linearly interpolates variable onto resolution of d04 (highest resolution)
    limit_exc_8hr       : Evaluates exceedances of daily maximum 8-hour mean WHO guideline concentrations and the locations and freqs at which this happens
    timeseries_24hr (v) : Violinplots of 8-hour mean concentrations + WHO guideline exceedances, Figure 5.11 of report. Scenarios 1 & 4 are paired, as well as scenarios 2 & 3
    '''
    
    def __init__(self):
        self.W = 8
        self.lim_val = lim_8hr_o3
        self.xi = np.linspace(d01.min_lon, d01.max_lon, new_res_lon)
        self.yi = np.linspace(d01.min_lat, d01.max_lat, new_res_lat)
        self.Xi, self.Yi = np.meshgrid(self.xi, self.yi)
        
        self.gen_inoutput()

    def gen_inoutput(self):
        
        o3_list = [[aux_func.rolling_mean_along_axis(d01.scen1_wrfout['o3'][idx_nospinup:,0,:,:], self.W) , aux_func.rolling_mean_along_axis(d02.scen1_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d03.scen1_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d04.scen1_wrfout['o3'][idx_nospinup:,0,:,:], self.W)],
            [aux_func.rolling_mean_along_axis(d01.scen2_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d02.scen2_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d03.scen2_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d04.scen2_wrfout['o3'][idx_nospinup:,0,:,:], self.W)],
            [aux_func.rolling_mean_along_axis(d01.scen3_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d02.scen3_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d03.scen3_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d04.scen3_wrfout['o3'][idx_nospinup:,0,:,:], self.W)],
            [aux_func.rolling_mean_along_axis(d01.scen4_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d02.scen4_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d03.scen4_wrfout['o3'][idx_nospinup:,0,:,:], self.W), aux_func.rolling_mean_along_axis(d04.scen4_wrfout['o3'][idx_nospinup:,0,:,:], self.W)]]

        self.emis_df_8h = create_df_merged_dom(timeshift = 8, periods = int(14*24 - 8 + 1), O3 = o3_list, burntfuel = burntfuel)
        
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp_sc1 = tri.Triangulation(self.emis_df_8h[(self.emis_df_8h['scenario'] == 1)]['lon'], self.emis_df_8h[(self.emis_df_8h['scenario'] == 1)]['lat'])
        interpolator_o3 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_8h[(self.emis_df_8h['scenario'] == 1)]['O3'])
        self.o3_sc1 = interpolator_o3(self.Xi, self.Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_8h[(self.emis_df_8h['scenario'] == 2)]['O3'])
        self.o3_sc2 = interpolator_o3(self.Xi, self.Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_8h[(self.emis_df_8h['scenario'] == 3)]['O3'])
        self.o3_sc3 = interpolator_o3(self.Xi, self.Yi)
        interpolator_o3 = tri.LinearTriInterpolator(interp_sc1, self.emis_df_8h[(self.emis_df_8h['scenario'] == 4)]['O3'])
        self.o3_sc4 = interpolator_o3(self.Xi, self.Yi)
        
        self.emis_df_8h['day'] = self.emis_df_8h['date'].apply(lambda x: x.date())
        self.emis_df_8h['time'] = self.emis_df_8h['date'].apply(lambda x: x.time())
        self.emis_df_8h['date'] = pd.to_datetime((self.emis_df_8h['date']))
        self.emis_df_8h = self.emis_df_8h[self.emis_df_8h.date.dt.strftime('%H:%M:%S').between('08:00:00','23:00:00')]
        
        self.tm8_o3 = self.emis_df_8h.groupby(['lon', 'lat', 'scenario', 'day'])['O3'].max()
        self.tm8_o3 = self.tm8_o3.to_frame().reset_index()
        
        self.tm8_o3 = self.tm8_o3.join(self.emis_df_8h, lsuffix="_left", rsuffix="_right")
        self.tm8_o3.drop(['lon_right', 'lat_right','scenario_right', 'date',
               'O3_right', 'burntfuel', 'day_right'], axis = 1, inplace = True)
        self.tm8_o3.columns = ['lon', 'lat', 'scenario', 'day', 'O3', 'domain', 'time']
        
    def limit_exc_8hr(self, scen):
        
        domains = [1,2,3,4]

        interp_sc1 = tri.Triangulation(self.tm8_o3[(self.tm8_o3['scenario'] == scen) & (self.tm8_o3['domain'].isin(domains)) & (self.tm8_o3['day'] == self.tm8_o3['day'].unique()[0])]['lon'],
                                       self.tm8_o3[(self.tm8_o3['scenario'] == scen) & (self.tm8_o3['domain'].isin(domains)) & (self.tm8_o3['day'] == self.tm8_o3['day'].unique()[0])]['lat'])
        
        o3 = []
        
        for date in self.tm8_o3['day'].unique():
            data = self.tm8_o3[self.tm8_o3['day'] == date]
            interpolator_o3 = tri.LinearTriInterpolator(interp_sc1, data[(data['scenario'] == scen) & (data['domain'].isin(domains))]['O3'])
            o3.append(interpolator_o3(self.Xi, self.Yi))
        
        o3 = np.reshape(np.stack(o3), (14, -1))
        
        self.df_o3 = pd.DataFrame(zip(np.repeat(self.tm8_o3['day'].unique(), np.shape(o3)[1]), 
                                o3.flatten(), np.tile(self.Xi.flatten(), np.shape(o3)[0]),
                                np.tile(self.Yi.flatten(), np.shape(o3)[0])), columns = ['date', 'O3', 'lon', 'lat'])
    
        self.groupby = self.df_o3.groupby('date')[['date', 'O3']]
        self.grouped = self.groupby.mean()
    
        self.exc_ind = self.df_o3[self.df_o3['O3'] > self.lim_val].groupby('date')['O3'].count() / self.df_o3.groupby('date')[['date', 'O3']].count()['date'].iloc[0] * 100
    
        exceed = self.df_o3[self.df_o3['O3'] > self.lim_val]
        num_timelocs = len(self.df_o3)
        num_loc = len(self.df_o3[self.df_o3['date'] == self.df_o3['date'].iloc[0]])
        
        ind_loc_exc = exceed.groupby(['lon', 'lat'])[['lon', 'lat']].count()
        print('Scenario {}:\nMean exceedances per location where lim is exceeded: {:.2f}'.format(
            scen, ind_loc_exc['lon'].sum() / len(ind_loc_exc)))
        self.perc_exc = ind_loc_exc['lon'].sum() / num_timelocs * 100
        perc_loc_exc = len(ind_loc_exc) / num_loc * 100
        print('% exceedances of total: {:.2f}%\n%Locations exceeding limit once: {:.2f}%'.format(self.perc_exc, perc_loc_exc))
        perc_loc_exc = len(ind_loc_exc[ind_loc_exc['lon'] > 3]) / num_loc * 100
        print('% locations exceeding limit four times or more: {:.2f}%'.format(perc_loc_exc))
        print(num_loc, num_timelocs)
        
        return self.df_o3
        
    def timeseries_8hr(self, scens): # scens could be '14' or '23'
        
        if scens == '14':
            self.limit_exc_8hr(1)
            sc1 = self.df_o3
            self.limit_exc_8hr(4)
            sc4 = self.df_o3
            comb_sc = sc1.append(sc4, ignore_index=True)
            comb_sc['scenario'] = np.repeat(['Sc$_{I}$', 'Sc$_{IV}$'], int(len(comb_sc) / 2))
        elif scens == '23':
            self.limit_exc_8hr(2)
            sc2 = self.df_o3
            self.limit_exc_8hr(3)
            sc3 = self.df_o3
            comb_sc = sc2.append(sc3, ignore_index=True)
            comb_sc['scenario'] = np.repeat(['Sc$_{II}$', 'Sc$_{III}$'], int(len(comb_sc) / 2))
        
        dates = self.df_o3['date'].unique()
        
        fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(8, 10), 
                         sharex=True, gridspec_kw=dict(height_ratios=(1, 3), hspace=0.1))
        cplot = sns.countplot(data = comb_sc[comb_sc['O3'] > self.lim_val], x = 'date', 
                              hue = 'scenario', ax = axes[0], palette = sc14colors if scens == '14' else sc23colors)
        
        for p in cplot.patches:
            cplot.annotate(format(p.get_height() / 2.5e5 * 100, '.2f') + '%', (p.get_x() + p.get_width() / 2., p.get_height() + 1.2e4),
                            ha = 'center', va = 'center', xytext = (0, 10), textcoords = 'offset points',
                            fontsize = 17, rotation = 90, bbox=dict(facecolor='white', 
                                                                                          edgecolor='none',
                                                                                          boxstyle='round',
                                                                                          alpha=0.5))
        axes[0].legend([],[], frameon=False)
        myFmt = mdates.DateFormatter('%d-%b')
        axes[0].xaxis.set_major_formatter(plt.FixedFormatter([(pd.to_datetime(str(item))).strftime("%d-%m") for item in dates]))
        axes[0].set_ylabel('above WHO\nguideline (%)')
        axes[0].set_yticks(np.arange(0, 1e5, 2e4))
        axes[0].set_yticklabels(np.arange(0, 1e5, 2e4) / 2.5e5 * 100)
        sns.violinplot('date','O3', data = comb_sc, hue = 'scenario', split=True, ax=axes[1],
                       palette = sc14colors if scens == '14' else sc23colors)
        axes[1].xaxis.set_major_formatter(plt.FixedFormatter([(pd.to_datetime(str(item))).strftime("%d-%m") for item in dates]))
        axes[1].set_ylabel('O$_{3}$ ($\\mu$g m$^{-3})$')
        axes[1].axhline(y = self.lim_val, color = 'k', linestyle = 'dashed', label = 'WHO guideline')
        legend = axes[1].legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')


class _1hr_meandata:
    
    '''
    1-hour mean analysis. Used mainly to generate time series.
    
    Methods
    ----------
    gen_timemeanlists       : Auxiliary method that generates 2D arrays aggregating data on 4D vars on different domains and scenarios
    gen_emis_1hr            : Generates the aggregated dataframe of main variables (NO2, O3, PM25, U10, V10)
    gen_time_series         : Generates time series for each scenarios
    time_series_per_loc (v) : Plots time series for a certain scenario, variable and location, versus the domain-mean concentration
    wind_dir (v)            : Plots wind rose with wind direction and wind speed (as color), Figure 5.14 of report (wind_dir(scen = 1, lon = lon0_man, lat = lat0_man))
    '''
    
    def __init__(self):
        
        self.gen_emis_1hr()

    def gen_timemeanlists(self, var):
        
        try:
            arr =  [[d01.scen1_wrfout[var][idx_nospinup:,ground_lvl,:,:] , d02.scen1_wrfout[var][idx_nospinup:,ground_lvl,:,:], d03.scen1_wrfout[var][idx_nospinup:,ground_lvl,:,:], d04.scen1_wrfout[var][idx_nospinup:,ground_lvl,:,:]],
                    [d01.scen2_wrfout[var][idx_nospinup:,ground_lvl,:,:], d02.scen2_wrfout[var][idx_nospinup:,ground_lvl,:,:], d03.scen2_wrfout[var][idx_nospinup:,ground_lvl,:,:], d04.scen2_wrfout[var][idx_nospinup:,ground_lvl,:,:]],
                    [d01.scen3_wrfout[var][idx_nospinup:,ground_lvl,:,:], d02.scen3_wrfout[var][idx_nospinup:,ground_lvl,:,:], d03.scen3_wrfout[var][idx_nospinup:,ground_lvl,:,:], d04.scen3_wrfout[var][idx_nospinup:,ground_lvl,:,:]],
                    [d01.scen4_wrfout[var][idx_nospinup:,ground_lvl,:,:], d02.scen4_wrfout[var][idx_nospinup:,ground_lvl,:,:], d03.scen4_wrfout[var][idx_nospinup:,ground_lvl,:,:], d04.scen4_wrfout[var][idx_nospinup:,ground_lvl,:,:]]]
        except:
            arr =  [[d01.scen1_wrfout[var][idx_nospinup:,:,:] , d02.scen1_wrfout[var][idx_nospinup:,:,:], d03.scen1_wrfout[var][idx_nospinup:,:,:], d04.scen1_wrfout[var][idx_nospinup:,:,:]],
                    [d01.scen2_wrfout[var][idx_nospinup:,:,:], d02.scen2_wrfout[var][idx_nospinup:,:,:], d03.scen2_wrfout[var][idx_nospinup:,:,:], d04.scen2_wrfout[var][idx_nospinup:,:,:]],
                    [d01.scen3_wrfout[var][idx_nospinup:,:,:], d02.scen3_wrfout[var][idx_nospinup:,:,:], d03.scen3_wrfout[var][idx_nospinup:,:,:], d04.scen3_wrfout[var][idx_nospinup:,:,:]],
                    [d01.scen4_wrfout[var][idx_nospinup:,:,:], d02.scen4_wrfout[var][idx_nospinup:,:,:], d03.scen4_wrfout[var][idx_nospinup:,:,:], d04.scen4_wrfout[var][idx_nospinup:,:,:]]]
    
        return arr
        
    def gen_emis_1hr(self):

        self.emis_df_1h = create_df_merged_dom(timeshift = 0, periods = int(14*24 + 1), NO2 = self.gen_timemeanlists('no2'),
                                               O3 = self.gen_timemeanlists('o3'), PM25 = self.gen_timemeanlists('PM2_5_DRY'),
                                               U10 = self.gen_timemeanlists('U10'), V10 = self.gen_timemeanlists('V10'), burntfuel = burntfuel)

        df = self.emis_df_1h[self.emis_df_1h['burntfuel'] >= fuel_lim][['lon','lat']].drop_duplicates().reset_index().drop('index', axis = 1)
        df['label'] = ['MXP', 'MUC', 'VIE', 'CDG', 'LHR', 'LHR',
                                'DUB', 'MAN', 'CDG', 'FRA', 'BRU', 'AMS']
        self.emis_df_1h = self.emis_df_1h.merge(df, how="left", on = ['lat', 'lon'])
        
    def gen_time_series(self):
        
        self.grouped_sc1 = self.emis_df_1h[self.emis_df_1h['scenario'] == 1].groupby('date')[['date', 'NO2', 'O3', 'PM25']].mean()
        self.grouped_sc2 = self.emis_df_1h[self.emis_df_1h['scenario'] == 2].groupby('date')[['date', 'NO2', 'O3', 'PM25']].mean()
        self.grouped_sc3 = self.emis_df_1h[self.emis_df_1h['scenario'] == 3].groupby('date')[['date', 'NO2', 'O3', 'PM25']].mean()
        self.grouped_sc4 = self.emis_df_1h[self.emis_df_1h['scenario'] == 4].groupby('date')[['date', 'NO2', 'O3', 'PM25']].mean()
        
    def time_series_per_loc(self, loclist, var, scen):
        
        dict_sc = {1: 'Sc$_{I}$', 2: 'Sc$_{II}$', 3: 'Sc$_{III}$', 4: 'Sc$_{IV}$'}

        start_date = dt.datetime.strptime('01-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        end_date = dt.datetime.strptime('15-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        daterange = pd.date_range(start_date, end_date, periods = 337)
        
        plt.figure()
        
        for loc in loclist:
            plt.plot(daterange, self.emis_df_1h[(self.emis_df_1h['label'] == loc) & (self.emis_df_1h['scenario'] == scen)].groupby('date')[var].mean(), label = '{} ({})'.format(loc, dict_sc[scen]))
        
        mean_sc = self.emis_df_1h[(self.emis_df_1h['scenario'] == scen)].groupby('date')[var].mean()
        plt.plot(daterange, mean_sc, label = 'mean')
        plt.ylabel('Concentration ($\mu$g m$^{-3}$')
        plt.legend()
        
    def wind_dir(self, scen, lon, lat):
        
        def local_wind(U, V, lon, lat):
        
            y, x = np.where((d01.lons == lon) & (d01.lats == lat))
            
            uvel_sc2 = U[:, y[0], x[0]]
            vvel_sc2 = V[:, y[0], x[0]]
            bins = [item - 22.5 for item in [0,45,90,135,180,225,270,315,360,405]]
            
            wind_speed = np.sqrt(uvel_sc2**2 + vvel_sc2**2)
            wind_dir = np.arctan2(vvel_sc2, uvel_sc2) * 180 / m.pi
            wind_dir = np.where(wind_dir < 0, abs(wind_dir) + 90, np.where(wind_dir < 90, 90 - wind_dir, 360 - (wind_dir - 90)))
            wind_dir = np.where(wind_dir > 180, wind_dir - 180, 360 - (180 - wind_dir))
            
            wind_dir_freq = stats.binned_statistic(wind_dir, wind_dir, statistic='count', 
                                                   bins=bins).statistic
            wind_dir_df = pd.DataFrame(wind_dir_freq, columns = ['dir'])
            wind_dir_df.index = np.convolve(stats.binned_statistic(wind_dir, wind_dir, statistic='count', 
                                                                   bins=bins).bin_edges,np.ones(2) / 2, mode='valid')
            
            wind_dir_df['dir'] = wind_dir_df['dir'] / wind_dir_df['dir'].sum() * 100
            
            wind_dir_speed = stats.binned_statistic(wind_dir, wind_speed, statistic='mean', 
                                                    bins=bins).statistic
            wind_dir_speed = pd.DataFrame(wind_dir_speed, columns = ['speed'])
            wind_dir_speed.iloc[0] = np.mean([wind_dir_speed.iloc[0], wind_dir_speed.iloc[-1]])
            wind_dir_speed.drop(labels = [8], axis = 0, inplace = True)
            
            wind_dir_df.iloc[0] = wind_dir_df.iloc[0] + wind_dir_df.iloc[-1]
            wind_dir_df.drop(labels = [360.0], axis = 0, inplace = True)
            
            return wind_dir_speed, wind_dir_df

        scen_to_data = {1: d01.scen1_wrfout, 2: d01.scen2_wrfout, 3: d01.scen3_wrfout, 4: d01.scen4_wrfout}
        scen_to_label = {1: 'Sc$_{I}$', 2: 'Sc$_{II}$', 3: 'Sc$_{III}$', 4: 'Sc$_{IV}$'}
        
        wind_dir_speed, wind_dir_df = local_wind(scen_to_data[scen]['U10'], scen_to_data[scen]['V10'], lon, lat)
        
        fig, axes = plt.subplots(nrows=1, ncols=1)
        ax = plt.subplot(111, polar = True)
        data_color = wind_dir_speed['speed'] / wind_dir_speed['speed'].max()
        my_cmap = plt.cm.get_cmap('ocean_r')
        colors = my_cmap(data_color)
        barp = ax.bar(wind_dir_df.index * m.pi / 180, wind_dir_df['dir'].values,
                     width = 0.5, alpha = 0.7, color = colors)
        sm = ScalarMappable(cmap=my_cmap, norm=plt.Normalize(0, max(wind_dir_speed['speed'])))
        sm.set_array([])
        
        ax.set_theta_zero_location("N")
        ax.set_theta_direction(-1)
        ax.set_ylim([0,35])
        ax.set_yticks(np.arange(5,40,5))
        ax.set_yticklabels([''] + ['{}%'.format(i) for i in np.arange(10,40,5)])
        ax.yaxis.set_tick_params(labelsize=15)
        ax.set_title(scen_to_label[scen])
        ax.set_rlabel_position(330)
        ax.set_xticklabels(['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'])
        cbar = fig.colorbar(sm)
        cbar.set_label('mean wind speed (m/s)', rotation=270,labelpad=25)


class local_airqual:
    
    '''
    Local air quality analysis, used for plotting (changes in) concentrations around airports.
    
    Methods
    ----------
    km_to_lon             : Converts kilometers to degrees in longitude
    km_to_lat             : Converts kilometers to degrees in latitude
    lon_deg_to_km         : Converts degrees in longitude to kilometers
    lat_deg_to_km         : Converts degrees in latitude to kilometers
    polar_grid            : Generates a new grid in polar coordinates based on a center coordinate, radius, radial node distance and number of angular directions
    gen_local_data        : interpolates data onto new polar grid
    demons_plot (v)       : Produces example of local air quality output at Eindhoven Airport for NO2, Figure 4.13 in report
    local_qual_plot_I (v) : Plots local changes in concentrations between scenarios and changes as a function of distance from the airport
    local_qual_plot_I (v) : Same as local_qual_plot_I, but for Eindhoven and Rotterdam-The Hague Airport combined, Figure 5.17 of report
    '''
    
    def __init__(self, dom, agg_array):
        # e.g. la = local_airqual(d03, gt.emis_df)
        
        self.norm = MidpointNormalize(midpoint = 0)
        self.dom = dom
        self.agg_array = agg_array

        xi = np.linspace(self.dom.min_lon, self.dom.max_lon, int((self.dom.max_lon - self.dom.min_lon) / (self.dom.res_x / 3)))
        yi = np.linspace(self.dom.min_lat, self.dom.max_lat, int((self.dom.max_lat - self.dom.min_lat) / (self.dom.res_y / 3)))
        self.Xi, self.Yi = np.meshgrid(xi, yi)
        
    def km_to_lon(self, lat, dist):
        circ = 6.378e3 * np.sin(np.radians(90 - lat))
        conv = 2 * m.pi * circ / 360
        return dist / conv
        
    def km_to_lat(self, dist):
        conv = 2 * m.pi * 6.378e3 / 360
        return dist / conv
    
    def lon_deg_to_km(self, lat, deg):
        circ = 6.378e3 * np.sin(np.radians(90 - lat))
        conv = 2 * m.pi * circ / 360
        return deg * conv
    
    def lat_deg_to_km(self, deg):
        conv = 2 * m.pi * 6.378e3 / 360
        return deg * conv
        
    def polar_grid(self, lon0, lat0, radius, nodes, nr_dirs):
        
        dist = radius / nodes
        grid_pt_nr = int(nodes * nr_dirs)
        self.lon = np.zeros((grid_pt_nr,))
        self.lat = np.zeros((grid_pt_nr,))
        self.d_km = np.zeros((grid_pt_nr,))
        self.nr_dirs = nr_dirs
        self.nodes = nodes
        self.radius = radius 
        
        x = 0
        i = 0
        
        while x < radius:
            
            x += dist
            print(x, self.km_to_lon(lat0, x))
            
            for point in range(nr_dirs):
                ang = 2 * m.pi / nr_dirs * point
                self.lon[i], self.lat[i], self.d_km[i] = lon0 + self.km_to_lon(lat0, x) * np.sin(ang), lat0 + self.km_to_lat(x) * np.cos(ang), x
                i += 1
        
        self.lon_new = np.insert(self.lon, 0, lon0)
        self.lat_new = np.insert(self.lat, 0, lat0)
        
        self.point_coords = np.vstack((self.dom.lons.flatten(), self.dom.lats.flatten())).T
        self.new_coords = np.vstack((self.lon_new, self.lat_new)).T
    
    def gen_local_data(self, var, lon0, lat0, radius, nodes, nr_dirs):
        
        self.polar_grid(lon0, lat0, radius, nodes, nr_dirs)
                
        # Perform linear interpolation of the data (x,y) on a grid defined by (xi,yi)
        interp = tri.Triangulation(self.agg_array[(self.agg_array['scenario'] == 1)]['lon'], self.agg_array[(self.agg_array['scenario'] == 1)]['lat'])
        
        interpolator = tri.LinearTriInterpolator(interp, self.agg_array[(self.agg_array['scenario'] == 1)][var])
        self.sc1 = interpolator(self.Xi, self.Yi)
        interpolator = tri.LinearTriInterpolator(interp, self.agg_array[(self.agg_array['scenario'] == 2)][var])
        self.sc2 = interpolator(self.Xi, self.Yi)
        interpolator = tri.LinearTriInterpolator(interp, self.agg_array[(self.agg_array['scenario'] == 3)][var])
        self.sc3 = interpolator(self.Xi, self.Yi)
        interpolator = tri.LinearTriInterpolator(interp, self.agg_array[(self.agg_array['scenario'] == 4)][var])
        self.sc4 = interpolator(self.Xi, self.Yi)
        
        self.point_coords = np.vstack((self.Xi.flatten(), self.Yi.flatten())).T
        # try:
            
        self.abs_sc1 = griddata(self.point_coords, self.sc1.flatten(), self.new_coords, method='linear')
        self.abs_sc2 = griddata(self.point_coords, self.sc2.flatten(), self.new_coords, method='linear')
        self.abs_sc3 = griddata(self.point_coords, self.sc3.flatten(), self.new_coords, method='linear')
        self.abs_sc4 = griddata(self.point_coords, self.sc4.flatten(), self.new_coords, method='linear')
        
        self.newgrid_scen1 = griddata(self.point_coords, self.sc2.flatten() - self.sc1.flatten(), self.new_coords, method='linear')
        self.newgrid_scen3 = griddata(self.point_coords, self.sc3.flatten() - self.sc1.flatten(), self.new_coords, method='linear')
        self.newgrid_scen4 = griddata(self.point_coords, self.sc4.flatten() - self.sc1.flatten(), self.new_coords, method='linear')
        
        self.newgrid_scen1_rel = griddata(self.point_coords, (self.sc2.flatten() - self.sc1.flatten()) / self.sc1.flatten() * 100, self.new_coords, method='linear')
        self.newgrid_scen3_rel = griddata(self.point_coords, (self.sc3.flatten() - self.sc1.flatten()) / self.sc1.flatten() * 100, self.new_coords, method='linear')
        self.newgrid_scen4_rel = griddata(self.point_coords, (self.sc4.flatten() - self.sc1.flatten()) / self.sc1.flatten() * 100, self.new_coords, method='linear')

        self.rad_func_scen1 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen1)).T
        self.rad_func_scen1 = pd.DataFrame(self.rad_func_scen1, columns = ['dist', 'val'])
        
        self.rad_func_scen3 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen3)).T
        self.rad_func_scen3 = pd.DataFrame(self.rad_func_scen3, columns = ['dist', 'val'])
        
        self.rad_func_scen4 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen4)).T
        self.rad_func_scen4 = pd.DataFrame(self.rad_func_scen4, columns = ['dist', 'val'])
        
        self.rel_rad_func_scen1 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen1_rel)).T
        self.rel_rad_func_scen1 = pd.DataFrame(self.rel_rad_func_scen1, columns = ['dist', 'val'])
        
        self.rel_rad_func_scen3 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen3_rel)).T
        self.rel_rad_func_scen3 = pd.DataFrame(self.rel_rad_func_scen3, columns = ['dist', 'val'])
        
        self.rel_rad_func_scen4 = np.vstack((np.insert(self.d_km, 0, 0), self.newgrid_scen4_rel)).T
        self.rel_rad_func_scen4 = pd.DataFrame(self.rel_rad_func_scen4, columns = ['dist', 'val'])

        # dom_mean_sc1 = (self.dom.time_mean(self.dom.scen1_wrfout, self.var)[ground_lvl,:,:].flatten() - self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten()) / self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten() * 100
        # dom_mean_sc3 = (self.dom.time_mean(self.dom.scen3_wrfout, self.var)[ground_lvl,:,:].flatten() - self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten()) / self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten() * 100
        # dom_mean_sc4 = (self.dom.time_mean(self.dom.scen4_wrfout, self.var)[ground_lvl,:,:].flatten() - self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten()) / self.dom.time_mean(self.dom.scen2_wrfout, self.var)[ground_lvl,:,:].flatten() * 100
    
        # self.Z_score_sc1 = (self.rel_rad_func_scen1.groupby('dist').mean()['val'] - dom_mean_sc1.mean()) / dom_mean_sc1.std()
        # self.Z_score_sc3 = (self.rel_rad_func_scen3.groupby('dist').mean()['val'] - dom_mean_sc3.mean()) / dom_mean_sc3.std()
        # self.Z_score_sc4 = (self.rel_rad_func_scen4.groupby('dist').mean()['val'] - dom_mean_sc4.mean()) / dom_mean_sc4.std()

        
    def demons_plot(self):
        
        self.polar_grid(lon0 = lon0_ein, lat0 = lat0_ein, radius = 15, nodes = 5, nr_dirs = 5)
        
        fig = plt.figure(figsize=(9, 6))
        plt.subplots_adjust(hspace = 0.4)
        
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        map = Basemap(width=200000 / 1.5,height=200000 / 1.5,
                    rsphere=(6378137.00,6356752.3142),\
                    resolution='h',area_thresh=1000.,projection='lcc',\
                    lat_0 = np.round(lat0_ein, 1), lon_0 = np.round(lon0_ein, 1), ax = ax)
        
        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        map.drawparallels(np.arange(-90., 120.,0.5),labels=[1,0,0,0],fontsize=14)
        map.drawmeridians(np.arange(-180.,180.,0.5),labels=[0,0,0,1],fontsize=14)
                
        lon, lat = map(self.Xi, self.Yi)
        scat = ax.scatter(lon, lat, 0.1, marker = 'o', color = 'k', label = 'original grid 2', zorder = 2)
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 20, marker = 'o', color = 'k', label = 'new polar grid', zorder = 2)
        cbar = plt.colorbar(scat, extend = 'both')
        cbar.set_label('NO$_2$ difference (%)')
        
        for i in range(self.nodes):
            x,y = map(lon0_ein, lat0_ein)
            x2,y2 = map(lon0_ein, self.lat_new[self.nr_dirs * i + 1])
            circle1 = plt.Circle((x, y), abs(y2 - y), fill = False)
            ax.add_patch(circle1)
            
        ax.annotate('Eindhoven\nAirport', xy=(map(lon0_ein + 0.01, lat0_ein)), xytext=(-60, 60), fontsize=15,
                    textcoords='offset points', ha='center', va='bottom', color = 'white',
                    bbox=dict(boxstyle='round', fc='black', edgecolor='none', alpha=0.8),
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.1', 
                                    color='black', lw = 3))

        fig = plt.figure(figsize=(9, 6))
        ax = fig.add_subplot(1,1,1) # two rows, two columns, fist cell
        map = Basemap(width=200000 / 1.5,height=200000 / 1.5,
                    rsphere=(6378137.00,6356752.3142),\
                    resolution='h',area_thresh=1000.,projection='lcc',\
                    lat_0 = np.round(lat0_ein, 1),lon_0 = np.round(lon0_ein, 1), ax = ax)
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        map.drawparallels(np.arange(-90., 120.,0.5),labels=[1,0,0,0],fontsize=14)
        map.drawmeridians(np.arange(-180.,180.,0.5),labels=[0,0,0,1],fontsize=14)
        
        self.gen_local_data(var = 'NO2', lon0 = lon0_ein, lat0 = lat0_ein, 
                            radius = 15, nodes = 15, nr_dirs = 100)
        
        lon, lat = map(self.Xi, self.Yi)
        scat = ax.scatter(lon, lat, 2, c = (self.sc1 - self.sc2) / self.sc2 * 100, marker = 'o', cmap = 'seismic',
                          vmin=-5, vmax=1, norm = self.norm, label = 'original grid 2', zorder = 2)
        cbar = plt.colorbar(scat, extend = 'both')
        cbar.set_label('NO$_2$ difference (%)')
        
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 20, marker = 'o', c = self.newgrid_scen1_rel, cmap = 'seismic',
                          vmin=-5, vmax=5, norm = self.norm, zorder = 2)
        lon, lat = map(lon0_ein, lat0_ein)
        scat = ax.scatter(lon, lat, 20, marker = 'o', color = 'w', zorder = 2)
        
        ax.annotate('Eindhoven\nAirport', xy=(map(lon0_ein, lat0_ein)), xytext=(-60, 60), fontsize=15,
                    textcoords='offset points', ha='center', va='bottom', color = 'white',
                    bbox=dict(boxstyle='round', fc='black', edgecolor='none', alpha=0.8),
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.1', 
                                    color = 'black', lw = 3))

    def local_qual_plot_I(self, var, lon0, lat0):

        label_var = {'NO2': 'NO$_2$', 'O3': 'O$_3$', 'PM25': 'PM$_{2.5}$', 'SWDOWN': 'SWDOWN', 'surf_balance': 'Net ground radiative forcing'}
        
        if lon0 == lon0_ein:
            label_airport = 'Eindhoven'
        elif lon0 == lon0_rot:
            label_airport = 'Rotterdam'
        elif lon0 == lon0_ams:
            label_airport = 'Amsterdam'
        elif lon0 == lon0_lon:
            label_airport = 'London'
        elif lon0 == lon0_man:
            label_airport = 'Manchester'
        elif lon0 == lon0_cdg:
            label_airport = 'Charles de Gaulle'
        else:
            label_airport = '?'
            
        color_range = {'NO2': [-0.3, 0.3], 'O3': [-0.8, 0.3], 'PM25': [-0.4, 0.1], 'SWDOWN': [-4, 4], 'surf_balance': [-2,2]}

        self.gen_local_data( var = var, lon0 = lon0, lat0 = lat0, 
                            radius = 15, nodes = 15, nr_dirs = 100)
        
        fig = plt.figure(figsize=(12, 7))
        plt.subplots_adjust(hspace = 0.4)
        ax = fig.add_subplot(2,1,1) # two rows, two columns, fist cell
        
        sns.lineplot(data = self.rad_func_scen1, x='dist', y = 'val', label = 'Sc$_{II}$ - Sc$_{I}$',
                      color = color_sc2, ax = ax)
        sns.lineplot(data = self.rad_func_scen3, x='dist', y = 'val', label = 'Sc$_{III}$ - Sc$_{I}$',
                      color = color_sc3, ax = ax)
        sns.lineplot(data = self.rad_func_scen4, x='dist', y = 'val', label = 'Sc$_{IV}$- Sc$_{II}$',
                      color = color_sc4, ax = ax)
        ax.axvline(0, color = 'black')
        ax.axhline(0, color = 'black')
        ax.set_xticks(np.arange(0, self.radius + 1, int(self.radius / self.nodes + 1)))
        
        ax.set_xlabel('distance from {} airport (km)'.format(label_airport))
        ax.set_ylabel('$\\Delta${} ($\\mu$g m$^{{-3}}$)'.format(label_var[var]))
        legend = plt.legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')
        
        width = 200000 / 1.5
        height = 200000 / 1.5
        
        ax = fig.add_subplot(2,3,4) # two rows, two columns, fist cell
        map = Basemap(width=width, height=height,
                    resolution='h', projection='lcc',
                    lat_0 = np.round(lat0, 1), lon_0 = np.round(lon0, 1), ax = ax)
        
        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 80, c = self.newgrid_scen1, marker = 'o', cmap = 'seismic',
                          vmin = color_range[var][0], vmax = color_range[var][1], norm = self.norm, zorder = 2)
        
        axins = inset_axes(ax,
                            width="5%",
                            height="100%",
                            loc='center left',
                            borderpad=-1
                           )
        
        cbar = fig.colorbar(scat, fraction=0.046, cax = axins, ax = ax, orientation = "vertical")
        axins.yaxis.set_ticks_position("left")
        cbar.set_label('$\\Delta${} ($\\mu$g m$^{{-3}}$)'.format(label_var[var]))
        axins.yaxis.set_label_position('left')
        ax.set_title('Sc$_{II}$ - Sc$_{I}$')
        
        ax = fig.add_subplot(2,3,5) # two rows, two columns, fist cell
        map = Basemap(width=width ,height=height,
                    resolution='h', projection='lcc',
                    lat_0 = np.round(lat0, 1), lon_0 = np.round(lon0, 1), ax = ax)

        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 80, c = self.newgrid_scen3, marker = 'o', cmap = 'seismic', 
                          vmin = color_range[var][0], vmax = color_range[var][1], norm = self.norm, zorder = 2)
        ax.set_title('Sc$_{III}$ - Sc$_{I}$')
        
        ax = fig.add_subplot(2,3,6) # two rows, two columns, fist cell
        map = Basemap(width=width, height=height, #70x35 / 69x38
                    resolution='h', projection='lcc',
                    lat_0 = np.round(lat0, 1), lon_0 = np.round(lon0, 1), ax = ax)

        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 80, c = self.newgrid_scen4, marker = 'o', cmap = 'seismic', 
                          vmin = color_range[var][0], vmax = color_range[var][1], norm = self.norm, zorder = 2)
        
        ax.set_title('Sc$_{IV}$ - Sc$_{I}$')
        fig.tight_layout()
        
    def local_qual_plot_II(self, var):
        
        label_var = {'NO2': 'NO$_2$', 'O3': 'O$_3$', 'PM25': 'PM$_{2.5}$'}
        
        fig = plt.figure(figsize=(9, 6))
        ax = fig.add_subplot(1,1,1)
        map = Basemap(width = 200000 / 1.2,height = 200000 / 1.2,
                    rsphere = (6378137.00, 6356752.3142),\
                    resolution = 'h', area_thresh = 1000., projection = 'lcc',\
                    lat_0 = 52, lon_0 = 5.15, ax = ax)
        map.shadedrelief()
        map.drawcoastlines()
        map.drawcountries()
        map.drawrivers(color='#0000ff')
        
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        self.gen_local_data(var = var, lon0 = lon0_ein, lat0 = lat0_ein, 
                            radius = 15, nodes = 15, nr_dirs = 100)
        
        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 20, marker = 'o', c = self.newgrid_scen3, cmap = 'seismic',
                          vmin=-0.3, vmax=0.05, norm = self.norm, zorder = 2)
        
        lon, lat = map(lon0_ein, lat0_ein)
        scat = ax.scatter(lon, lat, 20, marker = 'o', color = 'w', zorder = 2)
        
        ax.annotate('EIN', xy=(map(lon0_ein, lat0_ein)), xytext=(-60, 60), fontsize=15,
                    textcoords='offset points', ha='center', va='bottom', color = 'white',
                    bbox=dict(boxstyle='round', fc='black', edgecolor='none', alpha=0.8),
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.1', 
                                    color = 'black', lw = 3))
        
        self.gen_local_data(var = var, lon0 = lon0_rot, lat0 = lat0_rot, 
                            radius = 15, nodes = 15, nr_dirs = 100)

        lon, lat = map(self.lon_new, self.lat_new)
        scat = ax.scatter(lon, lat, 20, marker = 'o', c = self.newgrid_scen3, cmap = 'seismic',
                          vmin=-0.3, vmax=0.05, norm = self.norm, zorder = 2)
        cbar = plt.colorbar(scat, extend = 'both')
        cbar.set_label('{} change ($\\mu$g m$^{{-3}}$)'.format(label_var[var]))
        
        lon, lat = map(lon0_rot, lat0_rot)
        scat = ax.scatter(lon, lat, 20, marker = 'o', color = 'w', zorder = 2)
        
        ax.annotate('RTM', xy=(map(lon0_rot, lat0_rot)), xytext=(-60, 60), fontsize=15,
                    textcoords='offset points', ha='center', va='bottom', color = 'white',
                    bbox=dict(boxstyle='round', fc='black', edgecolor='none', alpha=0.8),
                    arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.1', 
                                    color = 'black', lw = 3))
        
        plt.title('{} (Sc$_{{II}}$ - Sc$_{{I}}$)'.format(label_var[var]), fontsize = 40)
        axins = inset_axes(ax, width = "80%", height = 2.5, loc = 'upper right')
        
        sns.lineplot(data = self.rad_func_scen3, x='dist', y = 'val', label = 'RTM',
                      color = color_sc3, linestyle='--', ax = axins, zorder = 2)
        self.gen_local_data(var = var, lon0 = lon0_ein, lat0 = lat0_ein, 
                            radius = 15, nodes = 15, nr_dirs = 100)
        sns.lineplot(data = self.rad_func_scen3, x='dist', y = 'val', label = 'EIN',
                      color = color_sc3, ax = axins, zorder = 2)
        axins.axvline(0, color = 'black')
        axins.axhline(0, color = 'black')
        legend = plt.legend(frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')
        axins.set_xlabel('$r$ (km)', fontsize = 18)
        axins.set_ylabel('$\\Delta${} ($\\mu$g m$^{{-3}}$)'.format(label_var[var]), fontsize = 18)

    
#%%
class validation_metrics:
    
    '''
    Validation metrics used to validate WRF-Chem output with observations.
    
    Methods
    ----------
    corr_coef       : Computes Pearson's correlation coefficients
    MBE             : Computes Mean Bias Error
    NMBE            : Computes Normalized Mean Bias Error
    NMGE            : Computes Normalized Mean Gross Error
    IOA             : Computes Index Of Agreement
    NRMSE           : Computes Normalized Root Mean Squared Error
    NCRMSD          : Computes Normalized Centered Root Mean Squared Difference
    RMSE            : Computes Root Mean Squared Error
    std             : Computes standard deviation
    background_mean : Computes mean of the observed data
    compute_metrics : Executes above methods for a certain variable
    '''
    
    def __init__(self, var, compare_C):
        
        self.var = var
        self.compare_C_val = compare_C.dropna(axis = 0, how = 'any', subset = ['{}_wrf'.format(self.var), '{}_valid'.format(self.var)])
        self.compare_C_val['{}_valid'.format(self.var)] = self.compare_C_val['{}_valid'.format(self.var)].astype(float)
        self.compute_metrics()
        self.get_df()
        
    def corr_coef(self, df):
        
        try:
            return np.corrcoef(df['{}_wrf'.format(self.var)].values, df['{}_valid'.format(self.var)].values)[0, 1]
        except:
            return np.corrcoef(df['{}_wrf'.format(self.var)].values, df['{}_valid'.format(self.var)].values.astype(float))[0, 1]
    
    def MBE(self, df):
        
        return np.mean(df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values)
    
    def NMBE(self, df):

        return np.mean(df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values) / np.mean(abs(df['{}_valid'.format(self.var)].values)) * 100
    
    def NMGE(self, df):
        
        return np.sum(abs(df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values)) / np.sum(abs(df['{}_valid'.format(self.var)].values))
    
    def IOA(self, df):
        
        return 1 - np.sum((df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values)**2) / np.sum((abs(df['{}_valid'.format(self.var)].values - np.mean(df['{}_valid'.format(self.var)].values)) +
                                                                                                             abs(df['{}_wrf'.format(self.var)].values - np.mean(df['{}_valid'.format(self.var)].values)))**2)
    def centered_rmsd(self, df, corr):
        std_mod = df['{}_wrf'.format(self.var)].std()
        std_obs = df['{}_valid'.format(self.var)].std()
        return np.sqrt(std_obs**2 + std_mod**2 - 2 * corr * std_obs * std_mod)
    
    def NRMSE(self, df):
        return np.sqrt(np.mean((df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values)**2)) / np.mean(abs(df['{}_valid'.format(self.var)].values)) * 100
    
    def NCRMSD(self, df, corr):
        std_mod = df['{}_wrf'.format(self.var)].std()
        std_obs = df['{}_valid'.format(self.var)].std()
        
        try:
            return np.sqrt(1 + (std_mod / std_obs)**2 - 2 * corr * (std_mod / std_obs))
        except:
            return np.nan
        
    def RMSE(self, df):
        
        return np.sqrt(np.mean((df['{}_wrf'.format(self.var)].values - df['{}_valid'.format(self.var)].values)**2))
    
    def std(self, df):
        std_mod = df['{}_wrf'.format(self.var)].std()
        std_obs = df['{}_valid'.format(self.var)].std()
        try:
            std_ratio = std_mod / std_obs
        except:
            std_ratio = np.nan
        return std_mod, std_obs, std_ratio
    
    def background_mean(self, df):
        
        return np.mean(abs(df['{}_valid'.format(self.var)].values))

    def compute_metrics(self):
        
        self.corrcoefs = []
        self.mbes = []
        self.nmbes = []
        self.nmges = []
        self.ioa = []
        self.crmsd = []
        self.std_obs = []
        self.std_mod = []
        self.std_ratio = []
        self.nrmse = []
        self.rmse = []
        self.ncrmsd = []
        self.mean_back = []
        
        for loc in self.compare_C_val['name'].unique():

            #print(self.var, loc)
            r = self.corr_coef(self.compare_C_val[self.compare_C_val['name'] == loc])
            self.corrcoefs.append(r)
            self.mbes.append(self.MBE(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.nmbes.append(self.NMBE(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.nmges.append(self.NMGE(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.ioa.append(self.IOA(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.nrmse.append(self.NRMSE(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.rmse.append(self.RMSE(self.compare_C_val[self.compare_C_val['name'] == loc]))
            self.crmsd.append(self.centered_rmsd(self.compare_C_val[self.compare_C_val['name'] == loc], r))
            self.ncrmsd.append(self.NCRMSD(self.compare_C_val[self.compare_C_val['name'] == loc], r))
            self.std_mod.append(self.std(self.compare_C_val[self.compare_C_val['name'] == loc])[0])
            self.std_obs.append(self.std(self.compare_C_val[self.compare_C_val['name'] == loc])[1])
            self.std_ratio.append(self.std(self.compare_C_val[self.compare_C_val['name'] == loc])[2])
            self.mean_back.append(self.background_mean(self.compare_C_val[self.compare_C_val['name'] == loc]))

    def get_df(self):
        
        self.eval_df = pd.DataFrame({'name': self.compare_C_val['name'].unique(),
                        'r': self.corrcoefs,
                        'MBE': self.mbes,
                        'NMBE': self.nmbes,
                        'NMGE': self.nmges,
                        'IOA': self.ioa})

        merge_prod = self.compare_C_val.groupby(['name', 'lat', 'lon']).mean().index.tolist()
        
        def Convert(tup, di):
            for a, b, c in tup:
                di.setdefault(a, []).append([b, c])
            return di
              
        dictionary = {}
        Convert(merge_prod, dictionary)
        
        self.eval_df['lat'] = self.eval_df.apply(lambda x: dictionary[x['name']][0][0], axis=1)
        self.eval_df['lon'] = self.eval_df.apply(lambda x: dictionary[x['name']][0][1], axis=1)
        


class validation_WRFchem_RIVM:
    
    '''
    Validation WRF-Chem output with observations in the Netherlands.
    
    Methods
    ----------
    __format__                       : Filters and formats the observational data from luchtmeetnet
    __timeseries_PM25__ (v)          : Plots monthly domain-mean PM2.5 concentrations from measurements & nr. of exceedances daily WHO guideline per month, Figure 6.10 of report
    __measurelocsmap__  (v)          : Rural background measurement sites shown on map, Figure 6.3 of report
    __WRF_data_exc__                 : Imports WRF-Chem output and matches data with observations at respective locations
    __WRF_output_exc_WHO__           : Computes exceedance of daily WHO guideline concentration
    __compare_timeseries_plots__ (v) : Produces one subplot time series per location, Appendix I of report
    __validation_vars__ (v)          : Plots Taylor diagram (NO2, O3, PM2.5, wind speed and wind direction) and NMBE versus NRMSE for all measurement sites, Figure 6.5 of report
    __barplot_map__ (v)              : R and IOA plotted for NO2 on a map as bar charts, Figure 6.4 of report
    '''
    
    def __init__(self):
        
        self.valid_data = pd.read_csv(path + '2019-AQ-METEO.csv')
        self.__format__()
        
    def __format__(self):
        
        # format and filter data
        self.valid_data['date'] = pd.to_datetime(self.valid_data['date'], format='%m/%d/%Y %H:%M')
        self.valid_data['date_end'] = pd.to_datetime(self.valid_data['date_end'], format='%m/%d/%Y %H:%M')
        
        start_date = dt.datetime.strptime('01-01-2019 01:00:00', '%d-%m-%Y %H:%M:%S')
        end_date = dt.datetime.strptime('31-12-2019 23:00:00', '%d-%m-%Y %H:%M:%S')
        
        self.valid_data = self.valid_data[self.valid_data['date'].between(start_date, end_date)]
        self.valid_data['date'] = self.valid_data['date'] - dt.timedelta(hours = 1) # correct to UTC time
         
        loc_cats = ['Rural Background']
        self.valid_data = self.valid_data[self.valid_data['type_airbase'].isin(loc_cats)]
        
        self.valid_data = self.valid_data.drop_duplicates()

        # get single 1 hr measurement per location (336 in total)
        self.valid_data = self.valid_data[~((self.valid_data['name'] == 'Westmaas-Groeneweg') & (self.valid_data['organisation'] == 'DCMR'))]
        
        # get locations
        self.locs = self.valid_data.groupby(['lon', 'lat', 'name']).size().reset_index().rename(columns={0:'count'})

    def __timeseries_PM25__(self):
        
        valid_data = self.valid_data.dropna(subset = ['pm25'], axis = 0)
                               
        rivm_pm25 = valid_data[['date', 'name', 'lon', 'lat', 'pm25']]
        rivm_pm25['pm25'] = pd.to_numeric(rivm_pm25['pm25'], downcast = 'float')
        pm25_year = rivm_pm25.groupby(['lon', 'lat', 'name', 'date'])['pm25'].mean().reset_index()
        
        pm25_year.columns = ['lon', 'lat', 'name', 'date', 'pm25']
        pm25_year['month'] = pm25_year['date'].apply(lambda x: x.month)
        pm25_month = pm25_year.groupby(['lon', 'lat', 'name', 'month'])['pm25'].mean().reset_index()
        pm25_year['day'] = pm25_year['date'].apply(lambda x: x.date())
        pm25_day = pm25_year.groupby(['lon', 'lat', 'name', 'day'])['pm25'].mean().reset_index()
        
        pm25_day_exc = pm25_day[pm25_day['pm25'] > lim_24hr_pm25_who].groupby(['day'])['pm25'].count().reset_index()
        ind_days = pm25_day.groupby(['day'])['pm25'].count().reset_index()
        
        pm25_day_exc = ind_days.merge(pm25_day_exc, how = 'outer', on = ['day'])
        pm25_day_exc['pm25_y'] = pm25_day_exc['pm25_y'].replace({np.nan: 0})
        pm25_day_exc['month'] = pm25_day_exc['day'].apply(lambda x: x.month)
        
        pm25_day_exc_month = pm25_day_exc.groupby('month')[['pm25_x', 'pm25_y']].sum().reset_index()
        pm25_day_exc_month['pm25_exc'] = pm25_day_exc_month['pm25_y'] / pm25_day_exc_month['pm25_x'] * 100
        
        start_date = dt.datetime.strptime('01-01-2019', '%d-%m-%Y')
        end_date = dt.datetime.strptime('31-12-2019', '%d-%m-%Y')
        pm25_day['day'] = pd.to_datetime(pm25_day['day'])
        selec_twowks = pm25_day[pm25_day['day'].between(start_date, end_date)]
        
        fig, ax1 = plt.subplots(figsize=(12,6))
        sns.lineplot(x = 'month', y = 'pm25', data = pm25_month, estimator=np.mean, ci = 95, markers=True,
                     err_style="band", color = 'orange', ax = ax1)
        ax2 = ax1.twinx()
        ax2.bar(pm25_day_exc_month['month'], pm25_day_exc_month['pm25_exc'], color = 'orange', alpha = 0.5)
        ax2.grid(False)
        ax1.set_xlabel('Month of the year (2019)')
        ax1.set_ylabel('PM$_{2.5}$ ($\\mu$ g$^{-3}$)')
        ax2.set_ylabel('daily PM$_{2.5}$ exceedance (%)')
        ax1.set_xticks(np.arange(1,13))
        ax1.set_ylim([0,19])
        ax1.set_xticklabels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

    def __measurelocsmap__(self):
        
        fig, ax = plt.subplots(figsize=(10, 9))  # bigger is better
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d03.min_lat - 1, urcrnrlat = d03.max_lat + 1,\
                    llcrnrlon = d03.min_lon - 1, urcrnrlon = d03.max_lon + 1, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
           
        lons, lats = map(self.locs['lon'], self.locs['lat'])
        map.scatter(lons, lats, color = 'k', s = 50, zorder = 2)
        
        ind_excfrmain = ['Hoofddorp-Hoofdweg', 'Amsterdam-Spaarnwoude', 'Oude Meer-Aalsmeerderdijk',
                         'Badhoevedorp-Sloterweg']

        xshift = [0, -0.58, 0, -0.48, -0.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        yshift = [0, 0, 0, 0, -0.22, 0, 0, 0, 0, 0, 0.025, 0, -0.22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        
        i = 0
        for row in self.locs.iterrows():
            if row[1]['name'] not in ind_excfrmain:
                x, y = map(row[1]['lon'] + 0.05 + xshift[i], row[1]['lat'] + 0.05 + yshift[i])
                plt.text(x, y, row[1]['name'].replace('-', '-\n'), color = 'white',
                              fontsize=10, ha = 'left', bbox=dict(facecolor='black',
                                                                  edgecolor='none',
                                                                  boxstyle='round',
                                                                  alpha=0.7))
            i += 1
        
        axins = zoomed_inset_axes(ax, 7, loc = 'upper left', bbox_to_anchor=(0,1-0.3,.3,.3), bbox_transform=ax.transAxes)
        
        map = Basemap(projection='cyl',llcrnrlat = d03.min_lat - 1, urcrnrlat = d03.max_lat + 1,\
                    llcrnrlon = d03.min_lon - 1, urcrnrlon = d03.max_lon + 1, resolution='h')
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
           
        lons, lats = map(self.locs['lon'], self.locs['lat'])
        map.scatter(lons, lats, color = 'k', s = 50, zorder = 2)
        
        xshift = [0, 0, 0, 0, 0, 0, -0.05, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        
        i = 0
        for row in self.locs.iterrows():
            if row[1]['name'] in ind_excfrmain:
                x, y = map(row[1]['lon'] + 0.01 + xshift[i], row[1]['lat'] + 0.01)
                plt.text(x, y, row[1]['name'].replace('-', '-\n'), color = 'white',
                              fontsize=10, ha = 'left', bbox=dict(facecolor='black',
                                                                  edgecolor='none',
                                                                  boxstyle='round',
                                                                  alpha=0.7))
        
            i += 1
            
        axins.set_xlim(4.68, 4.82)
        axins.set_ylim(52.25, 52.45)
        
        for axis in ['top','bottom','left','right']:
            axins.spines[axis].set_linewidth(5)
            axins.spines[axis].set_color('k')
            
        box, c1, c2 = mark_inset(ax, axins, loc1=1, loc2=3, fc="none", ec="k", linewidth = 2)

    def __WRF_data_exc__(self, df_1hr): # e.g. tm1.emis_df_1h
            
        self.emis_df_1h = df_1hr[df_1hr['scenario'] == 1]
                
        wrf_data_no2 = []
        wrf_data_o3 = []
        wrf_data_pm25 = []
        wrf_data_u10 = []
        wrf_data_v10 = []
        
        for date in self.emis_df_1h['date'].unique():
            print(date)
            wrf_df = self.emis_df_1h[self.emis_df_1h['date'] == date]
            
            point_coords = np.vstack((wrf_df['lon'], wrf_df['lat'])).T
            new_coords = np.vstack((self.locs['lon'], self.locs['lat'])).T
            
            wrf_data_no2.append(griddata(point_coords, wrf_df['NO2'], new_coords, method='linear'))
            wrf_data_o3.append(griddata(point_coords, wrf_df['O3'], new_coords, method='linear'))
            wrf_data_pm25.append(griddata(point_coords, wrf_df['PM25'], new_coords, method='linear'))
            wrf_data_u10.append(griddata(point_coords, wrf_df['U10'], new_coords, method='linear'))
            wrf_data_v10.append(griddata(point_coords, wrf_df['V10'], new_coords, method='linear'))
        
        self.df_wrf = pd.DataFrame({'date': np.repeat(self.emis_df_1h['date'].unique()[:-1], len(self.locs)),
                               'name': np.tile(self.locs['name'], len(wrf_data_no2) - 1),
                               'lon': np.tile(self.locs['lon'], len(wrf_data_no2) - 1),
                               'lat': np.tile(self.locs['lat'], len(wrf_data_no2) - 1),
                               'NO2': np.hstack(wrf_data_no2[:-1]),
                               'O3': np.hstack(wrf_data_o3[:-1]),
                               'PM25': np.hstack(wrf_data_pm25[:-1]),
                               'U10': np.hstack(wrf_data_u10[:-1]),
                               'V10': np.hstack(wrf_data_v10[:-1])})
        
        self.df_wrf['wdir'] = np.arctan2(self.df_wrf['V10'], self.df_wrf['U10']) * 180 / m.pi
        self.df_wrf['wdir'] = np.where(self.df_wrf['wdir'] <= 0, abs(self.df_wrf['wdir']) + 90, 
                                  np.where(self.df_wrf['wdir'] < 90, 90 - self.df_wrf['wdir'], 360 - (self.df_wrf['wdir'] - 90)))
        self.df_wrf['wdir'] = np.where(self.df_wrf['wdir'] > 180, self.df_wrf['wdir'] - 180, 360 - (180 - self.df_wrf['wdir']))
        
        self.df_wrf['wspd'] = np.sqrt(self.df_wrf['U10']**2 + self.df_wrf['V10']**2)
        
        self.compare_C = self.df_wrf.merge(self.valid_data[['date', 'name', 'lat', 'lon', 'no2', 'o3', 'pm25', 'wd', 'ws']],
                                 how = 'inner', on = ['date', 'name', 'lat', 'lon'])
        self.compare_C.drop(['U10', 'V10'], axis = 1, inplace = True)
        self.compare_C.columns = ['date', 'name', 'lon', 'lat', 'no2_wrf', 'o3_wrf', 'pm25_wrf', 'wd_wrf',
                             'wspd_wrf', 'no2_valid', 'o3_valid', 'pm25_valid', 'wd_valid', 'wspd_valid']
        
        
    def __WRF_output_exc_WHO__(self, var): # var could be 'NO2' or 'PM25'
        
        try:
            self.df_wrf
        except:
            self.__WRF_data_exc__(tm1.emis_df_1h)
        
        lim_val_dict = {'NO2': lim_24hr_no2_who, 'PM25': lim_24hr_pm25_who}
        
        rivm = self.df_wrf[['date', 'name', 'lon', 'lat', var]]
        rivm[var] = pd.to_numeric(rivm[var], downcast = 'float')
        var_year = rivm.groupby(['lon', 'lat', 'name', 'date'])[var].mean().reset_index()
        
        var_year.columns = ['lon', 'lat', 'name', 'date', var]
        var_year['month'] = var_year['date'].apply(lambda x: x.month)
        var_month = var_year.groupby(['lon', 'lat', 'name', 'month'])[var].mean().reset_index()
        var_year['day'] = var_year['date'].apply(lambda x: x.date())
        var_day = var_year.groupby(['lon', 'lat', 'name', 'day'])[var].mean().reset_index()
        
        var_day_exc = var_day[var_day[var] > lim_val_dict[var]].groupby(['day'])[var].count().reset_index()
        ind_days = var_day.groupby(['day'])[var].count().reset_index()
        
        self.var_day_exc = ind_days.merge(var_day_exc, how = 'outer', on = ['day'])
        self.var_day_exc['{}_y'.format(var)] = self.var_day_exc['{}_y'.format(var)].replace({np.nan: 0})
        self.var_day_exc['month'] = self.var_day_exc['day'].apply(lambda x: x.month)
        
        self.var_day_exc_month = self.var_day_exc.groupby('month')[['{}_x'.format(var), '{}_y'.format(var)]].sum().reset_index()
        self.var_day_exc_month['{}_exc'.format(var)] = self.var_day_exc_month['{}_y'.format(var)] / self.var_day_exc_month['{}_x'.format(var)] * 100
        
        start_date = dt.datetime.strptime('01-06-2019', '%d-%m-%Y')
        end_date = dt.datetime.strptime('14-06-2019', '%d-%m-%Y')
        var_day['day'] = pd.to_datetime(var_day['day'])
        selec_twowks = var_day[var_day['day'].between(start_date, end_date)]
        self.var_exc_count = selec_twowks[selec_twowks[var] > lim_val_dict[var]].groupby('name')[var].count()

    def __compare_timeseries_plots__(self, var): # var can be no2, o3, pm25, wd or wspd
    
        try:
            self.compare_C
        except:
            self.__WRF_data_exc__(tm1.emis_df_1h)
        
        fig, axs = plt.subplots(6, 4)
        fig.subplots_adjust(hspace = .2, wspace=.2)
        plt.rc('font', size=14) 
        plt.rc('xtick', labelsize=10) 
        
        axs = axs.ravel()
        
        for i in range(24):
            
            if i != 23:
                loc = self.compare_C['name'].unique()[i]
                axs[i].plot(self.compare_C[self.compare_C['name'] == loc]['date'],
                     self.compare_C[self.compare_C['name'] == loc]['{}_wrf'.format(var)], color = 'k', label = 'WRF-Chem simulation')
                axs[i].plot(self.compare_C[self.compare_C['name'] == loc]['date'],
                     self.compare_C[self.compare_C['name'] == loc]['{}_valid'.format(var)], color = 'darkorange', label = 'RIVM/DCMR data')
                axs[i].set_title(loc)
                axs[i].xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))

            else:
                axs[i].plot([0,0],
                            [0,0], color = 'k', label = 'WRF-Chem simulation')
                axs[i].plot([0,0],
                            [0,0], color = 'darkorange', label = 'RIVM/DCMR data')
                axs[i].set_xlim([0,0])
                axs[i].set_ylim([0,0])
                legend = axs[i].legend(loc = 10, frameon = True, framealpha = 1, shadow = True, 
                                        borderpad = 3, bbox_to_anchor=(0.5, 0.45))
                frame = legend.get_frame()
                frame.set_color('white')
        
    def __validation_vars__(self):
            
        try:
            self.compare_C
        except:
            self.__WRF_data_exc__(tm1.emis_df_1h)

        self.no2 = validation_metrics('no2', self.compare_C)
        self.o3 = validation_metrics('o3', self.compare_C)
        self.pm25 = validation_metrics('pm25', self.compare_C)
        self.wd = validation_metrics('wd', self.compare_C)
        self.wspd = validation_metrics('wspd', self.compare_C)

        label = {'NO$_2$': 'b', 'O$_3$': 'r', 'PM$_{25}$': 'green', 'Wind dir': 'orange', 'Wind speed': 'k'}
                
        conv_lab = {'Philippine-Stelleweg': 'PS', 'Zierikzee-Lange Slikweg': 'ZLS',
               'Huijbergen-Vennekenstraat': 'HV', 'Westmaas-Groeneweg': 'WG',
               'De Zilk-Vogelaarsdreef': 'DZV', 'Fijnaart-Zwingelspaansedijk': 'FZ',
               'Hoofddorp-Hoofdweg': 'HH', 'Amsterdam-Spaarnwoude': 'AS',
               'Oude Meer-Aalsmeerderdijk': 'OMA', 'Badhoevedorp-Sloterweg': 'BS',
               'Zegveld-Oude Meije': 'ZOM', 'Cabauw-Wielsekade': 'CW',
               'Wieringerwerf-Medemblikkerweg': 'WM', 'Biest Houtakker-Biestsestraat': 'BHB',
               'Balk-Trophornsterweg': 'BT', 'Wekerom-Riemterdijk': 'WR',
               'Vredepeel-Vredeweg': 'VV', 'Barsbeek-De Veenen': 'BDV',
               'Posterholt-Vlodropperweg': 'PV', 'Kollumerwaard-Hooge Zuidwal': 'KHZ',
               'Hellendoorn-Luttenbergerweg': 'HL', 'Eibergen-Lintveldseweg': 'EL',
               'Valthermond-Noorderdiep': 'VN'}
        
        labs_no2 = [conv_lab[lab] for lab in self.no2.compare_C_val['name'].unique()]
        labs_o3 = [conv_lab[lab] for lab in self.o3.compare_C_val['name'].unique()]
        labs_pm25 = [conv_lab[lab] for lab in self.pm25.compare_C_val['name'].unique()]
        labs_wd = [conv_lab[lab] for lab in self.wd.compare_C_val['name'].unique()]

        plt.figure()
        plt.rcParams['image.cmap'] = 'brg'
        sm.taylor_diagram(np.concatenate([[1], self.no2.std_ratio]), np.concatenate([[1], self.no2.ncrmsd]), 
                          np.concatenate([[1], self.no2.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.no2.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'b', alpha = 0.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 12)
        sm.taylor_diagram(np.concatenate([[1], self.o3.std_ratio]), np.concatenate([[1], self.o3.ncrmsd]), 
                          np.concatenate([[1], self.o3.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.o3.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 120.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              overlay = 'on', markerColor = 'r', alpha = 0.0, markerSize = 12)
        sm.taylor_diagram(np.concatenate([[1], self.pm25.std_ratio]), np.concatenate([[1], self.pm25.ncrmsd]), 
                          np.concatenate([[1], self.pm25.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.pm25.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 120.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              overlay = 'on', markerColor = 'green', alpha = 0.0, markerSize = 12)
        sm.taylor_diagram(np.concatenate([[1], self.wd.std_ratio]), np.concatenate([[1], self.wd.ncrmsd]), 
                          np.concatenate([[1], self.wd.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.wd.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 120.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              overlay = 'on', markerColor = 'orange', alpha = 0.0, markerSize = 12)
        sm.taylor_diagram(np.concatenate([[1], self.wspd.std_ratio]), np.concatenate([[1], self.wspd.ncrmsd]), 
                          np.concatenate([[1], self.wspd.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.wspd.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 120.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              overlay = 'on', markerColor = 'black', markerLabel = label, alpha = 0.0, markerSize = 12)
        
        sm.taylor_diagram(np.array([1, np.mean(self.no2.std_ratio)]), np.array([1, np.mean(self.no2.ncrmsd)]), 
                          np.array([1, np.mean(self.no2.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'b', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.o3.std_ratio)]), np.array([1, np.mean(self.o3.ncrmsd)]), 
                          np.array([1, np.mean(self.o3.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='r',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'r', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.pm25.std_ratio)]), np.array([1, np.mean(self.pm25.ncrmsd)]), 
                          np.array([1, np.mean(self.pm25.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'green', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.wd.std_ratio)]), np.array([1, np.mean(self.wd.ncrmsd)]), 
                          np.array([1, np.mean(self.wd.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'orange', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.wspd.std_ratio)]), np.array([1, np.mean(self.wspd.ncrmsd)]), 
                          np.array([1, np.mean(self.wspd.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'k', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        plt.ylabel('ratio standard deviation ($\\sigma_{mod} / \\sigma_{obs}$)')


        fig, ax = plt.subplots(figsize = (14,10))
        ax.scatter(self.no2.nmbes, self.no2.nrmse, s = 120, facecolors='none', edgecolors='b', alpha = 0.5, label = 'NO$_2$')
        ax.scatter(self.o3.nmbes, self.o3.nrmse, s = 120, facecolors='none', edgecolors='r', alpha = 0.5, label = 'O$_3$')
        ax.scatter(self.pm25.nmbes, self.pm25.nrmse, s = 120, facecolors='none', edgecolors='g', alpha = 0.5, label = 'PM$_{2.5}$')
        ax.scatter(self.wd.nmbes, self.wd.nrmse, s = 120, facecolors='none', edgecolors='orange', alpha = 0.5, label = 'Wind dir')
        ax.scatter(self.wspd.nmbes, self.wspd.nrmse, s = 80, facecolors='none', edgecolors='k', alpha = 0.5, label = 'Wind speed')
        
        ax.scatter([np.mean(self.no2.nmbes)], [np.mean(self.no2.nrmse)], s = 120, marker = 'd', c='b')
        ax.scatter([np.mean(self.o3.nmbes)], [np.mean(self.o3.nrmse)], s = 120, marker = 'd', c='r')
        ax.scatter([np.mean(self.pm25.nmbes)], [np.mean(self.pm25.nrmse)], s = 120, marker = 'd', c='g')
        ax.scatter([np.mean(self.wd.nmbes)], [np.mean(self.wd.nrmse)], s = 120, marker = 'd', c='orange')
        ax.scatter([np.mean(self.wspd.nmbes)], [np.mean(self.wspd.nrmse)], s = 120, marker = 'd', c='k')
        
        ax.set_xlabel('NMBE (%)')
        ax.set_ylabel('NRMSE (%)')
        legend = ax.legend(loc = 0, frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')

    def __barplot_map__(self):
        
        try:
            self.compare_C
        except:
            self.__WRF_data_exc__(tm1.emis_df_1h)
        
        self.no2 = validation_metrics('no2', self.compare_C)
        
        try:
            self.compare_C
        except:
            self.__WRF_data_exc__(tm1.emis_df_1h)

        def build_bar(mapx, mapy, ax, width, xvals, yvals, fcolors):
            ax_h = inset_axes(ax, width=width, height=width, loc=3,
                            bbox_to_anchor=(mapx, mapy), bbox_transform=ax.transData,
                            borderpad=0, axes_kwargs={'alpha': 0.35, 'visible': True})
        
            for x,y,c in zip(xvals, yvals, fcolors):
        
                ax_h.bar(x, y / 1.7, label=str(x), fc=c, edgecolor = 'k')
                for index, value in enumerate(yvals):
                    ax_h.text(0.1 if index == 0 else 1.1, value / 1.7 * 0.5 if value > 0.16 else value * 1.1,
                              '{:.2f}'.format(value),
                              fontsize = 10, color = 'k', ha = 'center', va = 'center', rotation = 90)
        
            ax_h.axis('off')
            return ax_h
        
        fig, ax = plt.subplots(figsize=(10, 9))
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d03.min_lat - 1, urcrnrlat = d03.max_lat + 1,\
                    llcrnrlon = d03.min_lon - 1, urcrnrlon = d03.max_lon + 1, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.locs['lon'], self.locs['lat'])
        map.scatter(lons, lats, color = 'k', s = 50, zorder=2, ax = ax)
        
        ind_excfrmain = ['Hoofddorp-Hoofdweg', 'Amsterdam-Spaarnwoude', 'Oude Meer-Aalsmeerderdijk',
                         'Badhoevedorp-Sloterweg']
        bar_width = 0.5  # inch
        xshift = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        
        colors = ['lightcoral', 'wheat']
        for ix, lon1, lat1, name in zip(list(range(len(self.no2.eval_df))), 
                                        self.no2.eval_df['lon'], self.no2.eval_df['lat'],
                                        self.no2.eval_df['name']):
            if name not in ind_excfrmain:
                x1, y1 = map(lon1 + xshift[ix], lat1)   # get data coordinates for plotting
                build_bar(x1, y1, ax, bar_width, xvals=['a', 'b'], \
                          yvals=[self.no2.eval_df['r'].iloc[ix], self.no2.eval_df['IOA'].iloc[ix]], \
                          fcolors=colors)
                    
        axins = zoomed_inset_axes(ax, 7, loc = 'upper left', bbox_to_anchor=(0,1-0.3,.3,.3), bbox_transform=ax.transAxes)
        
        map = Basemap(projection='cyl',llcrnrlat = d03.min_lat - 1, urcrnrlat = d03.max_lat + 1,\
                    llcrnrlon = d03.min_lon - 1, urcrnrlon = d03.max_lon + 1, resolution='h', ax = axins)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(self.locs['lon'], self.locs['lat'])
        map.scatter(lons, lats, color = 'k', s = 50, zorder=2, ax = axins)
        
        for ix, lon1, lat1, name in zip(list(range(len(self.no2.eval_df))), self.no2.eval_df['lon'],
                                        self.no2.eval_df['lat'], self.no2.eval_df['name']):
            if name in ind_excfrmain:
                x1, y1 = map(lon1 + xshift[ix], lat1)   # get data coordinates for plotting
                build_bar(x1, y1, axins, bar_width, xvals=['a', 'b'], \
                          yvals=[self.no2.eval_df['r'].iloc[ix], self.no2.eval_df['IOA'].iloc[ix]], \
                          fcolors=colors)
        
        axins.set_xlim(4.68, 4.82)
        axins.set_ylim(52.25, 52.45)
        for axis in ['top','bottom','left','right']:
            axins.spines[axis].set_linewidth(5)
            axins.spines[axis].set_color('k')
        box, c1, c2 = mark_inset(ax, axins, loc1=1, loc2=3, fc="none", ec="k", linewidth = 2)
        
        patch0 = mpatches.Patch(color=colors[0], label='$R$')
        patch1 = mpatches.Patch(color=colors[1], label='IOA')
        legend = ax.legend(handles = [patch0, patch1], loc=1, frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')


class validation_WRF_EEA:
    
    '''
    Validation WRF-Chem output with observations in entire study domain.
    
    Methods
    ----------
    __import_data__              : Imports observed data on NO2 and O3 concentrations in study domain
    __no2_yearplot__ (v)         : Plots monthly domain-mean NO2 concentrations from measurements & nr. of exceedances daily WHO guideline per month, Figure 6.8 of report
    __o3_yearplot__ (v)          : Plots monthly domain-mean O3 concentrations from measurements & nr. of exceedances max. 8-hour WHO guideline per month, Figure 6.8 of report
    __measurementslocs_map__ (v) : Measurement sites in Europe (NO2 & O3), Figure 6.6 of report
    __compare_exc_EEA_WRF__      : _1hr_meandata has to be run before. Compares WRF-Chem output with observations at measurement sites
    __validation_vars__ (v)      : Plots Taylor diagram (NO2 & O3) and NMBE versus NRMSE for all measurement sites within study domain outside NL, Figure 6.7 of report
    '''
    
    def __init__(self):
        
        self.__import_data__('NO2')
        self.eea_no2 = self.eea
        self.eea_no2_noNL = self.eea_noNL
        
        self.__import_data__('O3')
        self.eea_o3 = self.eea
        self.eea_o3_noNL = self.eea_noNL

    def __import_data__(self, var):
        
        meta = pd.read_csv(path + 'PanEuropean_metadata.csv')
        start_date_yr = dt.datetime.strptime('01-01-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        end_date_yr = dt.datetime.strptime('31-12-2019 23:00:00', '%d-%m-%Y %H:%M:%S')
        start_date_sim = dt.datetime.strptime('01-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        end_date_sim = dt.datetime.strptime('15-06-2019 00:00:00', '%d-%m-%Y %H:%M:%S')
        
        dict_list_sim = []
        dict_list_yr = []
        
        length = 3812 if var == 'NO2' else 2415
        
        for i in range(length):
            
            ff = pd.read_csv(path + 'EEA_data\\' + 'EEA_{}_{}.csv'.format(var, i + 1))
            ff = ff.merge(meta, how = 'inner', on = ['Countrycode', 'SamplingPoint'])
            
            ff = ff[(ff['AirQualityStationType'] == 'background') & (ff['AirQualityStationArea'] == 'rural') & 
                      (ff['Longitude'] > d01.min_lon) & (ff['Longitude'] < d01.max_lon) & (ff['Latitude'] > d01.min_lat) &
                      (ff['Latitude'] < d01.max_lat)]
            
            if len(ff) == 0:
                dict_list_sim.append(ff)
                dict_list_yr.append(ff)
                print('i: {}, length: {}'.format(i, len(ff)))
                continue
            else:
                ff['DatetimeBegin'] = ff['DatetimeBegin'].apply(lambda x: dt.datetime.strptime(x[:19], "%Y-%m-%d %H:%M:%S") +
                                                                (dt.datetime.strptime(x, "%Y-%m-%d %H:%M:%S %z") - 
                                                                 dt.datetime.strptime(x[:19] + ' +00:00', "%Y-%m-%d %H:%M:%S %z")))
                ff['DatetimeEnd'] = ff['DatetimeEnd'].apply(lambda x: dt.datetime.strptime(x[:19], "%Y-%m-%d %H:%M:%S") +
                                                                (dt.datetime.strptime(x, "%Y-%m-%d %H:%M:%S %z") - 
                                                                 dt.datetime.strptime(x[:19] + ' +00:00', "%Y-%m-%d %H:%M:%S %z")))
                
                ff_sim = ff[ff['DatetimeBegin'].between(start_date_sim, end_date_sim)]
                ff_yr = ff[ff['DatetimeBegin'].between(start_date_yr, end_date_yr)]
                
                dict_list_sim.append(ff_sim)
                dict_list_yr.append(ff_yr)
                print('i: {}, length: {}'.format(i, len(ff_sim)))
        
        dict_list_nw_sim = []
        dict_list_nw_yr = []
        
        for item_sim, item_yr in zip(dict_list_sim, dict_list_yr):
            dict_data = item_sim.to_dict('list')
            dict_list_nw_sim.append(dict_data)
            dict_data = item_yr.to_dict('list')
            dict_list_nw_yr.append(dict_data)
        
        self.eea = pd.DataFrame.from_dict(dict_list_nw_yr)
        self.eea = self.eea.set_index(self.eea.index).apply(pd.Series.explode).reset_index()
        self.eea.drop('index', axis = 1, inplace=True)
        self.eea.dropna(how = 'all', axis = 0, inplace=True)
        
        eea_sim = pd.DataFrame.from_dict(dict_list_nw_sim)
        eea_sim = eea_sim.set_index(eea_sim.index).apply(pd.Series.explode).reset_index()
        eea_sim.drop('index', axis = 1, inplace=True)
        eea_sim.dropna(how = 'all', axis = 0, inplace=True)
        self.eea_noNL = eea_sim[(eea_sim['Countrycode'] != 'NL')]

    def __no2_yearplot__(self):

        self.eea_no2['Concentration'] = pd.to_numeric(self.eea_no2['Concentration'], downcast = 'float')
        no2_year = self.eea_no2.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x', 'DatetimeBegin'])['Concentration'].mean().reset_index()
        no2_year.columns = ['lon', 'lat', 'name', 'date', 'no2']
        no2_year['month'] = no2_year['date'].apply(lambda x: x.month)
        no2_year['day'] = no2_year['date'].apply(lambda x: x.date())
        no2_month = no2_year.groupby(['lon', 'lat', 'name', 'month'])['no2'].mean().reset_index()
        no2_day = no2_year.groupby(['lon', 'lat', 'name', 'day'])['no2'].mean().reset_index()
        
        no2_day_exc = no2_day[no2_day['no2'] > lim_24hr_no2_who].groupby(['day'])['no2'].count().reset_index()
        ind_days = no2_day.groupby(['day'])['no2'].count().reset_index()
        
        no2_day_exc = ind_days.merge(no2_day_exc, how = 'outer', on = ['day'])
        no2_day_exc['no2_y'] = no2_day_exc['no2_y'].replace({np.nan: 0})
        no2_day_exc['month'] = no2_day_exc['day'].apply(lambda x: x.month)
        
        no2_day_exc_month = no2_day_exc.groupby('month')[['no2_x', 'no2_y']].sum().reset_index()
        no2_day_exc_month['no2_exc'] = no2_day_exc_month['no2_y'] / no2_day_exc_month['no2_x'] * 100
        
        fig, ax1 = plt.subplots(figsize=(12,6))
        sns.lineplot(x = 'month', y = 'no2', data = no2_month, estimator=np.mean, ci = 95, markers=True,
                     err_style="band", ax = ax1)
        ax2 = ax1.twinx()
        ax2.bar(no2_day_exc_month['month'], no2_day_exc_month['no2_exc'], alpha = 0.5)
        ax2.grid(False)
        ax1.set_xlabel('Month of the year (2019)')
        ax1.set_ylabel('NO$_2$ ($\\mu$ g$^{-3}$)')
        ax2.set_ylabel('daily NO$_2$ exceedance (%)')
        ax1.set_xticks(np.arange(1,13))
        ax1.set_ylim([0,19])
        ax1.set_xticklabels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

    def __o3_yearplot__(self):

        self.eea_o3['Concentration'] = pd.to_numeric(self.eea_o3['Concentration'], downcast = 'float')
        o3_year = self.eea_o3.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x', 'DatetimeBegin'])['Concentration'].mean().reset_index()
        o3_year.columns = ['lon', 'lat', 'name', 'date', 'o3']
        o3_year['month'] = o3_year['date'].apply(lambda x: x.month)
        o3_month = o3_year.groupby(['lon', 'lat', 'name', 'month'])['o3'].mean().reset_index()
        o3_hour = o3_year.groupby(['lon', 'lat', 'name', 'date'])['o3'].mean().reset_index()
        
        rolling_o3 = o3_hour.groupby(['name'])['o3'].rolling(8).mean().reset_index()
        rolling_o3.columns = ['name', 'id', 'o3']
        o3_hour = o3_hour.reset_index()
        o3_hour.columns = ['id', 'lon', 'lat', 'name', 'date', 'o3_old']
        
        rolling_o3 = rolling_o3.merge(o3_hour, how = 'inner', on = 'id')
        rolling_o3.drop(['name_y','o3_old'], axis = 1, inplace = True)
        rolling_o3.columns = ['name', 'id', 'o3', 'lat', 'lon', 'date']
        
        rolling_o3 = rolling_o3[rolling_o3.date.dt.strftime('%H:%M:%S').between('08:00:00','23:00:00')]
        rolling_o3['day'] = rolling_o3['date'].apply(lambda x: x.date())

        rolling_o3 = rolling_o3.groupby(['name', 'day'])['o3'].max().reset_index()
        rolling_o3_exc = rolling_o3[rolling_o3['o3'] > lim_8hr_o3].groupby(['name','day'])['o3'].count().reset_index()
        rolling_o3_days = rolling_o3.groupby(['name', 'day'])['o3'].count().reset_index()
        
        self.o3_day_exc = rolling_o3_days.merge(rolling_o3_exc, how = 'outer', on = ['name', 'day'])
        self.o3_day_exc['o3_y'] = self.o3_day_exc['o3_y'].replace({np.nan: 0})
        self.o3_day_exc['month'] = self.o3_day_exc['day'].apply(lambda x: x.month)
        
        o3_day_exc_month = self.o3_day_exc.groupby('month')[['o3_x', 'o3_y']].sum().reset_index()
        o3_day_exc_month['o3_exc'] = o3_day_exc_month['o3_y'] / o3_day_exc_month['o3_x'] * 100

        fig, ax1 = plt.subplots(figsize=(12,6))
        sns.lineplot(x = 'month', y = 'o3', data = o3_month, estimator=np.mean, ci = 95, markers=True,
                     err_style="band", color = 'red', ax = ax1)
        ax2 = ax1.twinx()
        ax2.bar(o3_day_exc_month['month'], o3_day_exc_month['o3_exc'], color = 'red', alpha = 0.5)
        ax2.grid(False)
        ax1.set_xlabel('Month of the year (2019)')
        ax1.set_ylabel('O$_3$ ($\\mu$ g$^{-3}$)')
        ax2.set_ylabel('daily O$_3$ exceedance (%)')
        ax1.set_xticks(np.arange(1,13))
        ax1.set_ylim([0,85])
        ax1.set_xticklabels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])        

    def __measurementslocs_map__(self):

        locs_no2 = self.eea_no2.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x']).count().reset_index()[['Longitude', 'Latitude', 'AirQualityStationEoICode_x']]
        locs_o3 = self.eea_o3.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x']).count().reset_index()[['Longitude', 'Latitude', 'AirQualityStationEoICode_x']]
        locs = pd.concat([locs_no2, locs_o3]).drop_duplicates().reset_index(drop=True)
        
        df_all = locs.merge(locs_o3.drop_duplicates(), 
                           how='left', indicator='o3?')
        df_all = df_all.merge(locs_no2.drop_duplicates(), on = ['Longitude', 'Latitude', 'AirQualityStationEoICode_x'],
                           how='left', indicator='no2?')
        
        fig, ax = plt.subplots(figsize=(10, 9))
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat - 1, urcrnrlat = d01.max_lat + 1,\
                    llcrnrlon = d01.min_lon - 1, urcrnrlon = d01.max_lon + 1, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        
        lons, lats = map(df_all[(df_all['o3?'] == 'both') & (df_all['no2?'] == 'both')]['Longitude'], df_all[(df_all['o3?'] == 'both') & (df_all['no2?'] == 'both')]['Latitude'])
        map.scatter(lons, lats, color = 'black', s = 40, zorder = 2, label = 'Both NO$_2$ and O$_3$')
          
        lons, lats = map(df_all[df_all['o3?'] == 'left_only']['Longitude'], df_all[df_all['o3?'] == 'left_only']['Latitude'])
        map.scatter(lons, lats, color = 'blue', s = 40, zorder = 2, label = 'Only NO$_2$')
        
        lons, lats = map(df_all[df_all['no2?'] == 'left_only']['Longitude'], df_all[df_all['no2?'] == 'left_only']['Latitude'])
        map.scatter(lons, lats, color = 'red', s = 40, zorder = 2, label = 'Only O$_3$')
        
        legend = plt.legend(frameon = True, title = 'Measured:', bbox_to_anchor=(0.45, 0.4))
        frame = legend.get_frame()
        frame.set_facecolor('white')
        frame.set_edgecolor('black')

    def __compare_exc_EEA_WRF__(self, df_1hr): # e.g. self.__compare_exc_EEA_WRF__(tm1.emis_df_1h)
        
        def __WRF_data_exc__(self, df_1hr):
            
            self.emis_df_1h = df_1hr[df_1hr['scenario'] == 1]
            
            locs_no2 = self.eea_no2.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x']).count().reset_index()[['Longitude', 'Latitude', 'AirQualityStationEoICode_x']]
            locs_o3 = self.eea_o3.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x']).count().reset_index()[['Longitude', 'Latitude', 'AirQualityStationEoICode_x']]
            locs = pd.concat([locs_no2, locs_o3]).drop_duplicates().reset_index(drop=True)
    
            wrf_data_no2 = []
            wrf_data_o3 = []
            
            for date in self.emis_df_1h['date'].unique():
                print(date)
                wrf_df = self.emis_df_1h[self.emis_df_1h['date'] == date]
                
                point_coords = np.vstack((wrf_df['lon'], wrf_df['lat'])).T
                new_coords = np.vstack((locs['Longitude'], locs['Latitude'])).T
                
                wrf_data_no2.append(griddata(point_coords, wrf_df['NO2'], new_coords, method='linear'))
                wrf_data_o3.append(griddata(point_coords, wrf_df['O3'], new_coords, method='linear'))
            
            self.df_wrf_eu = pd.DataFrame({'date': np.repeat(self.emis_df_1h['date'].unique()[:-1], len(locs)),
                                           'name': np.tile(locs['AirQualityStationEoICode_x'], len(wrf_data_no2) - 1),
                                           'lon': np.tile(locs['Longitude'], len(wrf_data_no2) - 1),
                                           'lat': np.tile(locs['Latitude'], len(wrf_data_no2) - 1),
                                           'NO2': np.hstack(wrf_data_no2[:-1]),
                                           'O3': np.hstack(wrf_data_o3[:-1])})
        
        self.__WRF_data_exc__(self, df_1hr)
        self.__o3_yearplot__()
        
        start_date = dt.datetime.strptime('01-06-2019', '%d-%m-%Y')
        end_date = dt.datetime.strptime('14-06-2019', '%d-%m-%Y')
        self.o3_day_exc['day'] = pd.to_datetime(self.o3_day_exc['day'])
        selec_twowks = self.o3_day_exc[self.o3_day_exc['day'].between(start_date, end_date)]
        self.o3_exc_per_loc = selec_twowks.groupby('name')['o3_y'].sum()
        
        self.o3_exc_per_loc = self.o3_day_exc.groupby(['name'])['o3_y'].transform(pd.Series.cumsum).reset_index()
        self.o3_exc_per_loc = self.o3_exc_per_loc.merge(self.o3_day_exc.reset_index(), how = 'inner', on = 'index')
        self.o3_exc_per_loc = self.o3_exc_per_loc[self.o3_exc_per_loc['o3_y_x'] == 4]
        self.o3_exc_per_loc.drop(['index', 'o3_x', 'o3_y_y', 'month'], axis = 1, inplace = True)
        self.o3_exc_per_loc = self.o3_exc_per_loc.drop_duplicates(['o3_y_x', 'name'])
        self.o3_exc_per_loc['day'] = pd.to_datetime(self.o3_exc_per_loc['day'])
        self.o3_exc_per_loc['month'] = self.o3_exc_per_loc['day'].apply(lambda x: x.month)
        print(self.o3_exc_per_loc.groupby('month').count())
        
        eea_no2_noNL = self.eea_no2_noNL[['AirQualityStationEoICode_x', 'Concentration', 'DatetimeBegin', 'Longitude', 'Latitude']]
        eea_no2_noNL.columns = ['name', 'no2', 'date', 'lon', 'lat']
        eea_no2_noNL['no2'] = eea_no2_noNL['no2'].astype(float)
        eea_no2_noNL = eea_no2_noNL.groupby(['name', 'date', 'lon', 'lat'])[['no2']].mean().reset_index()
        eea_no2_noNL.columns = ['name', 'date', 'lon', 'lat', 'no2']
        filter_locs = (eea_no2_noNL.groupby(['name', 'lon', 'lat'])['date'].count() > 50).reset_index()
        filter_locs = filter_locs[filter_locs['date'] == False]['name'].tolist()
        eea_no2_noNL = eea_no2_noNL[~eea_no2_noNL['name'].isin(filter_locs)]
        
        eea_o3_noNL = self.eea_o3_noNL[['AirQualityStationEoICode_x', 'Concentration', 'DatetimeBegin', 'Longitude', 'Latitude']]
        eea_o3_noNL.columns = ['name', 'o3', 'date', 'lon', 'lat']
        eea_o3_noNL['o3'] = eea_o3_noNL['o3'].astype(float)
        eea_o3_noNL = eea_o3_noNL.groupby(['name', 'date', 'lon', 'lat'])[['o3']].mean().reset_index()
        eea_o3_noNL.columns = ['name', 'date', 'lon', 'lat', 'o3']
        filter_locs = (eea_o3_noNL.groupby(['name', 'lon', 'lat'])['date'].count() > 50).reset_index()
        filter_locs = filter_locs[filter_locs['date'] == False]['name'].tolist()
        eea_o3_noNL = eea_o3_noNL[~eea_o3_noNL['name'].isin(filter_locs)]
        
        self.compare_C_eu = self.df_wrf_eu.merge(eea_o3_noNL, how = 'outer', on = ['date', 'name', 'lat', 'lon'])
        self.compare_C_eu = self.compare_C_eu.merge(eea_no2_noNL, how = 'outer', on = ['date', 'name', 'lat', 'lon'])
        self.compare_C_eu.columns = ['date', 'name', 'lon', 'lat', 'no2_wrf', 'o3_wrf', 'o3_valid', 'no2_valid']
        
        self.df_wrf_eu['O3'] = pd.to_numeric(self.df_wrf_eu['O3'], downcast = 'float')
        o3_year = self.df_wrf_eu.groupby(['lon', 'lat', 'name', 'date'])['O3'].mean().reset_index()
        o3_year.columns = ['lon', 'lat', 'name', 'date', 'o3']
        
        o3_hour = o3_year.groupby(['lon', 'lat', 'name', 'date'])['o3'].mean().reset_index()
        
        rolling_o3 = o3_hour.groupby(['name'])['o3'].rolling(8).mean().reset_index()
        rolling_o3.columns = ['name', 'id', 'o3']
        o3_hour = o3_hour.reset_index()
        o3_hour.columns = ['id', 'lon', 'lat', 'name', 'date', 'o3_old']
        
        rolling_o3 = rolling_o3.merge(o3_hour, how = 'inner', on = 'id')
        rolling_o3.drop(['name_y','o3_old'], axis = 1, inplace = True)
        rolling_o3.columns = ['name', 'id', 'o3', 'lat', 'lon', 'date']
        
        rolling_o3 = rolling_o3[rolling_o3.date.dt.strftime('%H:%M:%S').between('08:00:00','23:00:00')]
        rolling_o3['day'] = rolling_o3['date'].apply(lambda x: x.date())
        
        df = rolling_o3.groupby(['name', 'day'])['o3'].max().reset_index()
        df_exc = df[df['o3'] > lim_8hr_o3].groupby(['name','day'])['o3'].count().reset_index()
        df_days = df.groupby(['name', 'day'])['o3'].count().reset_index()
        
        o3_day_exc = df_days.merge(df_exc, how = 'outer', on = ['name', 'day'])
        o3_day_exc['o3_y'] = o3_day_exc['o3_y'].replace({np.nan: 0})
        o3_day_exc['month'] = o3_day_exc['day'].apply(lambda x: x.month)
        
        o3_day_exc_month = o3_day_exc.groupby('month')[['o3_x', 'o3_y']].sum().reset_index()
        o3_day_exc_month['o3_exc'] = o3_day_exc_month['o3_y'] / o3_day_exc_month['o3_x'] * 100
        
        o3_exc_per_loc_wrf = o3_day_exc.groupby('name')['o3_y'].sum()
        
        o3_exc_per_loc_wrf.dropna(inplace = True)
        
        self.o3_exc_per_loc_wrf = o3_exc_per_loc_wrf.reset_index().merge(self.o3_exc_per_loc.reset_index(),
                                                                         how = 'outer', on = 'name')


    def __validation_vars__(self):
            
        try:
            self.compare_C_eu
        except:
            self.__compare_exc_EEA_WRF__(tm1.emis_df_1h)

        self.no2 = validation_metrics('no2', self.compare_C_eu)
        self.o3 = validation_metrics('o3', self.compare_C_eu)
        
        label = {'NO$_2$': 'b', 'O$_3$': 'r'}
        
        plt.figure()
        plt.rcParams['image.cmap'] = 'brg'
        sm.taylor_diagram(np.concatenate([[1], self.no2.std_ratio]), np.concatenate([[1], self.no2.crmsd]), 
                          np.concatenate([[1], self.no2.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.no2.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 160.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='g',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'g', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'b', alpha = 0.0, rincSTD = 1, tickSTD = [0, 1, 2, 3], axismax = 3,
                              rincRMS = 1, tickRMS = [0, 1, 2, 3], markerSize = 12)#,
                              #markerLabel = list(np.concatenate([[''], labs_no2])), markerLabelColor = 'b')
        sm.taylor_diagram(np.concatenate([[1], self.o3.std_ratio]), np.concatenate([[1], self.o3.crmsd]), 
                          np.concatenate([[1], self.o3.corrcoefs]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              cmapzdata = np.concatenate([[1], self.o3.corrcoefs]), titleRMS = 'off',
                              tickRMSangle = 120.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='g',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'g', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              overlay = 'on', markerColor = 'r', markerLabel = label, alpha = 0.0, markerSize = 12)#,
                              #markerLabel = list(np.concatenate([[''], labs_o3])), markerLabelColor = 'r')
        
        sm.taylor_diagram(np.array([1, np.nanmean(self.no2.std_ratio)]), np.array([1, np.nanmean(self.no2.ncrmsd)]), 
                          np.array([1, np.nanmean(self.no2.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'b', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.nanmean(self.no2.std_ratio)]), np.array([1, np.nanmean(self.no2.ncrmsd)]), 
                          np.array([1, np.nanmean(self.no2.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='b',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'k', alpha = 0.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.o3.std_ratio)]), np.array([1, np.mean(self.o3.ncrmsd)]), 
                          np.array([1, np.mean(self.o3.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='r',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'r', alpha = 1.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        sm.taylor_diagram(np.array([1, np.mean(self.o3.std_ratio)]), np.array([1, np.mean(self.o3.ncrmsd)]), 
                          np.array([1, np.mean(self.o3.corrcoefs)]),
                              markerDisplayed = 'marker', titleColorbar = 'Mean Bias Error (MBE, $\\mu$g m$^{-3}$)',
                              titleRMS = 'off',
                              tickRMSangle = 110.0,
                              locationColorBar = 'EastOutside',
                              styleOBS  = 'solid', widthOBS = 2, colOBS ='r',
                              colRMS = 'm', styleRMS = ':', widthRMS = 2.0,
                              colSTD = 'b', styleSTD = '-.', widthSTD = 1.0,
                              colCOR = 'k', styleCOR = '--', widthCOR = 1.0,
                              markerColor = 'k', alpha = 0.0, rincSTD = 0.5, tickSTD = [0, 0.5, 1, 1.5, 2], axismax = 2.1,
                              overlay = 'on', rincRMS = 0.5, tickRMS = [0, 0.5, 1, 1.5, 2], markerSize = 14,
                              markerSymbol = 'd')
        plt.ylabel('ratio standard deviation ($\\sigma_{mod} / \\sigma_{obs}$)')
        
        fig, ax = plt.subplots(figsize = (13,9))
        ax.scatter(self.no2.nmbes, self.no2.nrmse, s = 80, facecolors='none', edgecolors='b', alpha = 0.5, label = 'NO$_2$')
        ax.scatter(self.o3.nmbes, self.o3.nrmse, s = 80, facecolors='none', edgecolors='r', alpha = 0.5, label = 'O$_3$')
        ax.scatter([np.nanmean(np.ma.masked_invalid(self.no2.nmbes))], [np.nanmean(np.ma.masked_invalid(self.no2.nrmse))], s = 200,
                   edgecolor = 'k', marker = 'd', c='b')
        ax.scatter([np.mean(self.o3.nmbes)], [np.mean(self.o3.nrmse)], s = 200, marker = 'd', c='orangered', edgecolor = 'k')
        ax.set_xlabel('NMBE (%)')
        ax.set_ylabel('NRMSE (%)')
        legend = ax.legend(loc = 2, frameon = True)
        frame = legend.get_frame()
        frame.set_color('white')

        cmap = (mpl.colors.ListedColormap(['yellow', 'orange', 'red', 'maroon', 'black']))
        
        bounds = [0, 1, 2, 3, 4, 5]
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        
        fig, ax = plt.subplots(figsize=(4, 4))
        fig.patch.set_facecolor('#EAEAF2')
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection='cyl',llcrnrlat = d01.min_lat, urcrnrlat = d01.max_lat,\
                    llcrnrlon = d01.min_lon, urcrnrlon = d01.max_lon, resolution='h', ax = ax)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0)
        map.fillcontinents(color='white',zorder=1)
        
        locs_no2 = self.eea_no2.groupby(['Longitude', 'Latitude', 'AirQualityStationEoICode_x']).count().reset_index()[['Longitude', 'Latitude', 'AirQualityStationEoICode_x']]
        locs_no2.columns = ['lon', 'lat', 'name']
        lons = []
        lats = []
        names = []
        for i in self.no2.compare_C['name'].unique():
            lons.append(locs_no2[locs_no2['name'] == i]['lon'].values[0])
            lats.append(locs_no2[locs_no2['name'] == i]['lat'].values[0])
            names.append(i)
        df = pd.DataFrame({'name': names, 'lon': lons, 'lat': lats, 'nmbe': self.no2.nmbes, 'C': self.no2.mean_back})
        
        df = df[df['nmbe'] > 100]
        
        lons, lats = map(df['lon'], df['lat'])
        scat = map.scatter(lons, lats, s = 50, c = df['C'], cmap = cmap, norm = norm, zorder=2)
        plt.title('NMBE > 100%')
        cbar = plt.colorbar(scat, fraction=0.038)
        cbar.set_label('NO$_2$ ($\\mu$g m$^{-3}$)')

    