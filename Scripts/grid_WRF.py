# -*- coding: utf-8 -*-
"""
Created on Sun May 16 15:35:08 2021

@author: T Y van der Duim
"""

'''
REQUIRED FILES:
    
    - namelist_wps.test (included)
    - namelist_input.test (included)
    - Grid_WRF.xlsx (included)
'''

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Import packages-----------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

import matplotlib.pyplot as plt
import numpy as np 
import pandas as pd
from matplotlib.patches import Polygon
import re
import datetime as dt
import os
os.environ["PROJ_LIB"] = "C:\\Users\\fafri\\anaconda3\\pkgs\\basemap-1.2.2-py38hcd0dc2c_2\\Library\\share\\"
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

'''
-------------------------------------------------------------------------------------------------------------
-----------------------------------------------Plot settings-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

plt.style.use('seaborn-darkgrid')
plt.rc('text', usetex=False)
plt.rc('font', family='century')
plt.rc('xtick', labelsize=18) 
plt.rc('ytick', labelsize=18) 
plt.rc('font', size=20) 

'''
-------------------------------------------------------------------------------------------------------------
---------------------------------------------------Variables-------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

# directories
home_dir = # home directory
path = home_dir + 'Files\\'

'''
-------------------------------------------------------------------------------------------------------------
---------------------------------------------------MAIN------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
'''

class WRF_grid(): # run by calling 'WRF_grid([tab])' in Console where tab is the excel tab (either 'input_res' or 'input_leftcorn')
    
    '''
    Defines WRF-Chem simulation grid and automates generation of namelist files for WRF-Chem runs
    
    Methods
    ----------
    __import_xlsx__       : Imports Grid_WRF.xslx where domain properties are defined
    __gen_grid_points__   : Generates grid points based on domain properties
    __draw_poly__         : Generates drawing objects (polygons) for map
    __draw_gridpoints__   : Generates drawing objects (grid points) for map
    __plot_map__ (v)      : Plots grids onto a map, Figure 3.2 of report
    __write_to_namelist__ : Automatically generates namelists (.wps and .input) using other methods
    '''
    
    def __init__(self, tab):
        with open(path + 'namelist_wps.test') as file:
            self.namelist_wps = file.readlines()
        with open(path + 'namelist_input.test') as file:
            self.namelist_input = file.readlines()
        self.tab = tab
        self.__import_xlsx__()
        self.__gen_grid_points__()
        self.__plot_map__()
        
    def __import_xlsx__(self):
        
        '''
        Parameters
        ----------
        grid_wrf : imported grid WRF excel file
        ncols    : number of columns in file (used to determine nr of grids)
        names    : column names, consisting of 'name', 'unit' and x nr of grids

        Main output
        -------
        df containing info on grids (cleaned and named)
        '''
    
        self.grid_wrf = pd.read_excel(path + 'Grid_WRF.xlsx', self.tab, usecols = [0,1,2,3,4,5])
        self.ncols = np.shape(self.grid_wrf)[1]
            
        self.names_init = ['name', 'unit']
        self.nr_grids = int(self.ncols - 2)
        self.names = ['grid {0}'.format(gridnr + 1) for gridnr in range(self.nr_grids)]
        self.names = self.names_init + self.names
        
        self.grid_wrf.columns = self.names
        self.grid_wrf = self.grid_wrf.dropna(subset = ['name']).reset_index(drop = True)
        self.grid_wrf = self.grid_wrf.drop(self.grid_wrf[self.grid_wrf.name == 
                               'Grid dimensions (the area that is covered by the grid)'].index).reset_index(drop = True)
        self.grid_wrf.columns = self.grid_wrf.columns.str.rstrip()
    
    def __gen_grid_points__(self):
        
        '''
        Parameters
        ----------
        min_lat_idx : row index in grid_wrf df of bottom latitude
        max_lat_idx : row index in grid_wrf df of top longitude
        min_lon_idx : row index in grid_wrf df of left-most latitude
        max_lon_idx : row index in grid_wrf df of right-most longitude

        Main output
        -------
        lons       : for each grid a list of 4 longitudinal coordinates (corresponding to polygon)
        lats       : for each grid a list of 4 latitudinal coordinates (corresponding to polygon)
        lon_points : for each grid a meshgrid of grid cell centers within polygon (longitudinal coordinates)
        lat_points : for each grid a meshgrid of grid cell centers within polygon (latitudinal coordinates)
        '''
        
        self.lons = []
        self.lats = []
        self.lon_points = []
        self.lat_points = []
        self.dx_grids = []
        self.dy_grids = []
        
        min_lat_idx = self.grid_wrf[self.grid_wrf.name == 'latitude bottom (bottom cell)'].index[0]
        max_lat_idx = self.grid_wrf[self.grid_wrf.name == 'latitude top (top cell)'].index[0]
        min_lon_idx = self.grid_wrf[self.grid_wrf.name == 'longitude left (left cell)'].index[0]
        max_lon_idx = self.grid_wrf[self.grid_wrf.name == 'longitude right (right cell) '].index[0]

        for col in self.grid_wrf.columns[2:]:
        
            lat = [self.grid_wrf[col].iloc[min_lat_idx], self.grid_wrf[col].iloc[max_lat_idx], 
                    self.grid_wrf[col].iloc[max_lat_idx], self.grid_wrf[col].iloc[min_lat_idx]]
            lon = [self.grid_wrf[col].iloc[min_lon_idx], self.grid_wrf[col].iloc[min_lon_idx], 
                    self.grid_wrf[col].iloc[max_lon_idx], self.grid_wrf[col].iloc[max_lon_idx]]
            
            self.lats.append(lat)
            self.lons.append(lon)
            
            self.dx = self.grid_wrf[(self.grid_wrf.name == 'dx =') & (self.grid_wrf.unit == 'degrees')].iloc[0][col]
            self.dy = self.grid_wrf[(self.grid_wrf.name == 'dy =') & (self.grid_wrf.unit == 'degrees')].iloc[0][col]
            X, Y = np.meshgrid(np.arange(np.min(lon) + self.dx / 2, np.max(lon), self.dx),
                               np.arange(np.min(lat) + self.dy / 2, np.max(lat), self.dy))
            
            self.lon_points.append(X)
            self.lat_points.append(Y)
            self.dx_grids.append(self.dx)
            self.dy_grids.append(self.dy)
                    
    def __draw_poly__(self, lons, lats, m, color, label):
        
        '''
        Parameters
        ----------
        lons  : for each grid a list of 4 longitudinal coordinates (corresponding to polygon)
        lats  : for each grid a list of 4 latitudinal coordinates (corresponding to polygon)
        m     : map to project on
        color : color of polygon
        label : label to give the polygon

        Main output
        -------
        polygon on map
        '''

        x, y = m(lons, lats)
        poly = Polygon(list(zip(x, y)), facecolor = color, alpha=0.4, label = label)
        plt.gca().add_patch(poly)
        
    def __draw_gridpoints__(self, lons_grid, lats_grid, m, color, size):
        
        '''
        Parameters
        ----------
        lons_grid  : for each grid a meshgrid of grid cell centers within polygon (longitudinal coordinates)
        lats_grid  : for each grid a meshgrid of grid cell centers within polygon (latitudinal coordinates)
        m          : map to project on
        color      : color of polygon
        size       : size of scatter points

        Main output
        -------
        scatter of each grid cell center in same color as corresponding polygon
        '''
        
        x, y = m(lons_grid, lats_grid)
        m.scatter(x, y, facecolor = 'black', s = size, zorder = 2)
    
    def __plot_map__(self):
        
        fig = plt.figure(figsize=(16,12))
        ax = plt.subplot(111)
        plt.rcParams.update({'font.size': 20})
        map = Basemap(projection = 'cyl',llcrnrlat = np.min(self.lats) - 2, urcrnrlat = np.max(self.lats) + 2,
                      resolution = 'h', llcrnrlon = np.min(self.lons) - 2, urcrnrlon = np.max(self.lons) + 2)
        map.drawcoastlines()
        map.drawcountries()
        map.drawmapboundary(fill_color='steelblue',zorder=0) 
        map.fillcontinents(color='white',zorder=1)
        map.drawparallels(np.arange(-90., 120.,5.),labels=[1,0,0,0],fontsize=14)
        map.drawmeridians(np.arange(-180.,180.,5.),labels=[0,0,0,1],fontsize=14)
        
        colors = ['red', 'cyan', 'green', 'brown', 'blue', 'grey', 'black'] # add colors if using more than 7 grids
        
        for grid in range(len(self.lats)):
            self.__draw_poly__(self.lons[grid], self.lats[grid], map, colors[grid], self.names[grid + 2])
            
            grid_point_size = {1: 1, 2: 2, 3: 0.1, 4: 0.001}
            self.__draw_gridpoints__(self.lon_points[grid], self.lat_points[grid], map, colors[grid], grid_point_size[grid + 1])
            
            if grid == 3:
                x, y = map(self.lons[grid][1] + 4 * (grid + 1) * self.dx_grids[grid], self.lats[grid][-2] - 4 * (grid + 1) * self.dy_grids[grid])
            else:
                x, y = map(self.lons[grid][1] + (grid + 1) * self.dx_grids[grid], self.lats[grid][-2] - (grid + 1) * self.dy_grids[grid])
            
            map.plot(x, y, "o", color = 'white', markersize = 30 - 4 * (grid + 1), zorder = 2)

            plt.annotate(str(grid + 1), xy = (x, y), color = 'black', size = 18 - 2 * grid, ha='center', va='center', zorder = 2)
        
        axins = zoomed_inset_axes(ax, zoom=6, loc=1)

        map2 = Basemap(projection = 'cyl',llcrnrlat = np.min(self.lats) - 2, urcrnrlat = np.max(self.lats) + 2,
                      resolution = 'h', llcrnrlon = np.min(self.lons) - 2, urcrnrlon = np.max(self.lons) + 2)
            
        map2.drawcoastlines()
        map2.drawcountries()
        map2.drawrivers(color='#0000ff')

        map2.drawmapboundary(fill_color='steelblue',zorder=0) 
        map2.fillcontinents(color='white',zorder=1)

        map2.drawparallels(np.arange(-90., 120.,5.),labels=[1,0,0,0],fontsize=14)
        map2.drawmeridians(np.arange(-180.,180.,5.),labels=[0,0,0,1],fontsize=14)
        
        colors = ['red', 'cyan', 'green', 'brown', 'blue', 'grey', 'black'] # add colors if using more than 7 grids
        
        for grid in range(2,len(self.lats)):
            self.__draw_poly__(self.lons[grid], self.lats[grid], map2, colors[grid], self.names[grid + 2])
            self.__draw_gridpoints__(self.lon_points[grid], self.lat_points[grid], map2, colors[grid], 1)
            
            if grid == 3:
                x, y = map2(self.lons[grid][1] + 2 * (grid + 1) * self.dx_grids[grid], self.lats[grid][-2] - 2 * (grid + 1) * self.dy_grids[grid])
            else:
                x, y = map2(self.lons[grid][1] + (grid + 1) * self.dx_grids[grid], self.lats[grid][-2] - (grid + 1) * self.dy_grids[grid])

            map2.plot(x, y, "o", color = 'white', markersize = 30 - 2 * (grid + 1), zorder = 2)
            plt.annotate(str(grid + 1), xy = (x, y), color = colors[grid], size = 18 - grid, ha='center', va='center', zorder = 2)
        
        ams_x, ams_y = map2(4.76417, 52.3081)
        map2.plot(ams_x, ams_y, "o", color = 'black', markersize = 8, zorder = 2)
        plt.annotate('Schiphol\n  Airport', xy = (ams_x + 0.05, ams_y + 0.05), color = 'k', size = 13,
                     zorder = 2, bbox=dict(facecolor='white', 
                                           edgecolor='none',
                                           boxstyle='round',
                                           alpha=0.7))
        
        ein_x, ein_y = map2(5.63888889, 51.44444444)
        map2.plot(ein_x, ein_y, "o", color = 'black', markersize = 8, zorder = 2)
        plt.annotate('Eindhoven\n  Airport', xy = (ein_x - 0.05, ein_y + 0.05), color = 'k', size = 13,
                     zorder = 2, ha = 'right', bbox=dict(facecolor='white', 
                                           edgecolor='none',
                                           boxstyle='round',
                                           alpha=0.7))
        
        rot_x, rot_y = map2(4.52777778, 51.88888889)
        map2.plot(rot_x, rot_y, "o", color = 'black', markersize = 8, zorder = 2)
        plt.annotate('Rotterdam-\n The Hague Airport', xy = (rot_x + 0.05, rot_y + 0.05), color = 'k', size = 13, 
                     zorder = 2, bbox=dict(facecolor='white', 
                                           edgecolor='none',
                                           boxstyle='round',
                                           alpha=0.7))
        
        x1, x2, y1, y2 = 4.2, 5.8, 51.3, 52.6
        axins.set_xlim(map2(x1, x2))
        axins.set_ylim(map2(y1, y2))
        pp,p1,p2 = mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="red", linewidth = 1, zorder = 3)
        for axis in ['top','bottom','left','right']:
            axins.spines[axis].set_linewidth(2)
            axins.spines[axis].set_color('red')

    def __gen_namelist_wps__(self, var):
        
        '''
        Parameters
        ----------
        idx_txt   : row index of variable sought for in namelist text file
        idx_xslx  : row index of variable sought for in excel file
        vals      : list of values for each grid

        Main output
        -------
        new line for text file containing set params
        '''
    
        idx_txt = [self.namelist_wps.index(l) for l in self.namelist_wps if l.startswith(' {0}'.format(var))][0]
        idx_xslx = self.grid_wrf[self.grid_wrf['name'].apply(lambda x: x.startswith(var))].index[0]
        vals = self.grid_wrf.iloc[idx_xslx].tolist()[2:]
        self.namelist_wps[idx_txt] = '{0}\t=   {1},\n'.format(' {0}'.format(var), ',   '.join(map(str, vals)))
    
    def __gen_namelist_wrf__(self, var):
        
        '''
        Parameters
        ----------
        idx_txt   : row index of variable sought for in namelist text file
        idx_xslx  : row index of variable sought for in excel file
        vals      : list of values for each grid

        Main output
        -------
        new line for text file containing set params
        '''
        if var != 'dx_eq' and var != 'dy_eq':
            idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format(var))][0]
            idx_xslx = self.grid_wrf[self.grid_wrf['name'].apply(lambda x: x.startswith(var))].index[0]
            vals = self.grid_wrf.iloc[idx_xslx].tolist()[2:]
            self.namelist_input[idx_txt] = '{0}\t=   {1},\n'.format(' {0}'.format(var), ',   '.join(map(str, vals)))
            
            # set 'parent_time_step_ratio' equal to 'parent_grid_ratio' in namelist input and gen exceptional lines on the fly
            if var == 'parent_grid_ratio':
                idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format('parent_time_step_ratio'))][0]
                self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format('parent_time_step_ratio'), ',   '.join(map(str, vals)))
                
                idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format('grid_id'))][0]
                vals = list(np.arange(1, 1 + self.nr_grids))
                self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format('grid_id'), ',   '.join(map(str, vals)))
                
                idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format('specified'))][0]
                specified = ['.true.']
                specified.extend(['.false.'] * (self.nr_grids - 1))
                self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format('specified'), ',   '.join(map(str, specified)))
                
                idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format('nested'))][0]
                nested = ['.false.']
                nested.extend(['.true.'] * (self.nr_grids - 1))
                self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format('nested'), ',   '.join(map(str, nested)))
                
        else:

            idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format(var[0:2]))][0]
            
            idx_xslx = np.where((self.grid_wrf['name'].str.startswith(var)) & (self.grid_wrf['unit'] == 
                                                                           'm'))[0][0]
            vals = self.grid_wrf.iloc[idx_xslx].tolist()[2:]
            vals = [val for val in vals] # from km to m
            self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format(var[0:2]), ',   '.join(map(str, vals)))
            
    def __gen_namelist_time__(self, var, val):
        
        '''
        Parameters
        ----------
        idx_txt   : row index of variable sought for in namelist text file
        idx_xslx  : row index of variable sought for in excel file
        vals      : list of values for each grid

        Main output
        -------
        new line for text file containing set params
        '''
        try:
            idx_txt = [self.namelist_wps.index(l) for l in self.namelist_wps if l.startswith(' {0}'.format(var))][0]
            try:
                vals = [eval(val)] * self.nr_grids
            except:
                vals = [val] * self.nr_grids
            self.namelist_wps[idx_txt] = "{0}\t=   '{1}',\n".format(' {0}'.format(var), "',   '".join(map(str, vals)))
        except:
            idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format(var))][0]
            vals = [val] * self.nr_grids
            self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format(var), ',   '.join(map(str, vals)))
            
    def __gen_namelist_re_wps__(self, var):
        
        '''
        Parameters
        ----------
        idx_txt   : row index of variable sought for in namelist text file
        idx_xslx  : row index of variable sought for in excel file (max_dom is not contained in excel file but determined earlier in script)
        val       : value as retrieved from excel

        Main output
        -------
        new line for text file containing set params
        '''
        
        idx_txt = [self.namelist_wps.index(l) for l in self.namelist_wps if l.startswith(' {0}'.format(var))][0]
        idx_xslx = np.where((self.grid_wrf['name'].str.startswith(var)) & (self.grid_wrf['unit'] == 
                                                                           'degrees'))[0][0] if (var != 'max_dom') else 0
        val = self.grid_wrf.iloc[idx_xslx]['grid 1']
        self.namelist_wps[idx_txt] = re.sub('[.\d]+', str(val), 
                                        self.namelist_wps[idx_txt]) if (var != 'max_dom') else re.sub('[.\d]+', 
                                                                                                  str(self.nr_grids), 
                                                                                                  self.namelist_wps[idx_txt])
        
    def __gen_namelist_re_wrf__(self, var):
    
        '''
        Parameters
        ----------
        idx_txt   : row index of variable sought for in namelist text file
        idx_xslx  : row index of variable sought for in excel file (max_dom is not contained in excel file but determined earlier in script)
        val       : value as retrieved from excel
    
        Main output
        -------
        new line for text file containing set params
        '''                                                                                              
                                                                                                  
        idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format(var))][0]
        idx_xslx = np.where((self.grid_wrf['name'].str.startswith(var)) & (self.grid_wrf['unit'] == 
                                                                           'degrees'))[0][0] if (var != 'max_dom') else 0
        val = self.grid_wrf.iloc[idx_xslx]['grid 1']
        self.namelist_input[idx_txt] = re.sub('[.\d]+', str(val), 
                                        self.namelist_wps[idx_txt]) if (var != 'max_dom') else re.sub('[.\d]+', 
                                                                                                  str(self.nr_grids), 
                                                                                                  self.namelist_input[idx_txt])
        
    def __gen_x_dups__(self, var):

        idx_txt = [self.namelist_input.index(l) for l in self.namelist_input if l.startswith(' {0}'.format(var))][0]
        val = self.namelist_input[idx_txt].split(',')[0].split('=')[-1].lstrip()
        try:
            vals = [eval(val)] * self.nr_grids if isinstance(eval(val), (int, float)) == True else [val] * self.nr_grids
        except:
            vals = [val] * self.nr_grids
        self.namelist_input[idx_txt] = '{0}\t\t\t=   {1},\n'.format(' {0}'.format(var), ',   '.join(map(str, vals)))
                                                                                                      
    def __write_to_namelist__(self, start_date, end_date):
        
        '''
        Parameters
        ----------
        start_date : start date of simulation in format yyyy-mm-dd hh:mm:ss, e.g. '2013-01-01 00:00:00'
        end_date   : end date of simulation in format yyyy-mm-dd hh:mm:ss, e.g. '2013-01-02 00:00:00'
        varlist    : list of params that are defined for each grid
        varlist_re : list of params that are defined for just one grid or define the number of grids (single values)

        Main output
        -------
        runs the above methods and overwrites the namelist txt file with the newly retrieved params
        ONLY RUN WHEN EXCEL VALUES ARE SET TO CELL (NOT DEGREES!)
        '''
        varlist = ['parent_id', 'parent_grid_ratio', 'i_parent_start', 'j_parent_start', 'e_we', 'e_sn']
        varlist_re = ['max_dom', 'dx', 'dy', 'ref_lat', 'ref_lon']
        
        list(map(self.__gen_namelist_wps__, varlist))
        list(map(self.__gen_namelist_re_wps__, varlist_re))
        
        
        self.__gen_namelist_time__('start_date', start_date.replace(' ', '_'))
        self.__gen_namelist_time__('end_date', end_date.replace(' ', '_'))
        
        with open(path + 'namelist_wps.test', 'w') as file:
            file.writelines(self.namelist_wps)
        
        self.start_date = dt.datetime.strptime(start_date, '%Y-%m-%d %H:%M:%S')
        self.end_date = dt.datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S')
        
        start = {'start_year': self.start_date.year, 'start_month': self.start_date.month, 'start_day': self.start_date.day,
                 'start_hour': self.start_date.hour, 'start_minute': self.start_date.minute, 'start_second': self.start_date.second}
        
        end = {'end_year': self.end_date.year, 'end_month': self.end_date.month, 'end_day': self.end_date.day,
                 'end_hour': self.end_date.hour, 'end_minute': self.end_date.minute, 'end_second': self.end_date.second}
        
        list(map(self.__gen_namelist_time__, start.keys(), start.values()))
        list(map(self.__gen_namelist_time__, end.keys(), end.values()))
        
        dup_inputs = ['input_from_file', 'history_interval', 'frames_per_outfile', 'auxinput5_interval',
                      'iofields_filename', 'auxhist3_interval', 'frames_per_auxhist3', 'e_vert', 'mp_physics', 'ra_lw_physics',
                      'ra_sw_physics', 'radt', 'sf_sfclay_physics', 'sf_surface_physics', 'bl_pbl_physics',
                      'bldt', 'cu_physics', 'cudt', 'progn', 'diff_6th_opt', 'diff_6th_factor', 'zdamp', 'dampcoef', 'khdif', 'kvdif', 'non_hydrostatic',
                      'moist_adv_opt', 'scalar_adv_opt', 'chem_adv_opt', 'chem_opt', 'bioemdt', 'photdt', 'phot_opt', 'chemdt',
                      'emiss_inpt_opt', 'emiss_opt', 'chem_in_opt', 'gas_drydep_opt', 'aer_drydep_opt', 'bio_emiss_opt',
                      'gas_bc_opt', 'gas_ic_opt', 'aer_bc_opt', 'aer_ic_opt', 'gaschem_onoff', 'aerchem_onoff',
                      'wetscav_onoff', 'cldchem_onoff', 'vertmix_onoff', 'chem_conv_tr', 'conv_tr_wetscav', 'conv_tr_aqchem',
                      'biomass_burn_opt', 'plumerisefire_frq', 'aer_ra_feedback', 'have_bcs_chem', 'have_bcs_tracer']
        
        list(map(self.__gen_x_dups__, dup_inputs))
                        
        varlist = ['parent_id', 'parent_grid_ratio', 'i_parent_start', 'j_parent_start', 'e_we', 'e_sn', 'dx_eq', 'dy_eq']
        varlist_re = ['max_dom']
        
        list(map(self.__gen_namelist_wrf__, varlist))
        list(map(self.__gen_namelist_re_wrf__, varlist_re))
        
        with open(path + 'namelist_input.test', 'w') as file:
            file.writelines(self.namelist_input)
         
        # align columns in namelist files
        
        with open('test_wps.test', 'w') as file:
            namelist = [re.split('(=)',row) for row in self.namelist_wps]
            for row in namelist:
                if len(row) == 3:
                    file.write("{} {} {}".format(*row))
                else:
                    file.write("{}".format(*row))
        
        with open('test_input.test', 'w') as file:
            namelist = [re.split('(=)',row) for row in self.namelist_input]
            for row in namelist:
                if len(row) == 3:
                    file.write("{} {} {}".format(*row))
                else:
                    file.write("{}".format(*row))