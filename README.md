# Master Thesis Fly-on-Hydro

This repository contains three main folders:

	- Scripts: Includes the main (clean) scripts used during this research. Included scripts are:

		- grid_WRF.py: Processes modeled grid defined in grid_WRF.xslx, plots domains and outputs namelists.
		- ac_emissions: Main code on the preparation and processing of air traffic (emission) files.
		- output_analysis: Main WRF-Chem output processing code including validation.
		- emiss_valid: Validation script of air traffic emissions and other inventory emissions.

	  Complete line 'home_dir = ' in script with path where Github folder is located.

	- Files: Includes most files needed to run above scripts. Not included files due to size (>100 MB) are:

		- Flights_06-2019.csv -> Can be downloaded from the EUROCONTROL R&D data archive: https://www.eurocontrol.int/dashboard/rnd-data-archive
		- Flight_Points_Actual_06-2019.csv -> Can be downloaded from the EUROCONTROL R&D data archive: https://www.eurocontrol.int/dashboard/rnd-data-archive
		- ac_emiss_1min_jun19.so6 -> output by Python after running 'flight_analysis()' class in ac_emissions.py (need previous two files)
		- ac_emiss_1min_jun19_OUTPUT_SEGMENTS_20211121_184824.so6 -> output of AEM model. Can be obtained by inputting ac_emiss_1min_jun19.so6 into AEM Kernel.
		- scen{1-4}_wrfout_d0{1-4}.nc -> Available from Snellius server in personal archive folder.
		- agg_vocscen{1-4}_wrfout_d0{1-4}.nc -> Available from Snellius server in personal archive folder.
		- pm_scen{1-4}_wrfout_d0{1-4}.nc -> Available from Snellius server in personal archive folder.
		- EEA_{var}_{num}.csv -> Downloadable from https://discomap.eea.europa.eu/map/fme/AirQualityExport.htm
		- 2019-AQ-METEO.csv -> Downloadable from https://www.luchtmeetnet.nl/

	- Visuals: Contains figures included in report and other output figures from analysis.